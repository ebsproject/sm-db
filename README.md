
![Alt text](https://thumbnails-photos.amazon.com/v1/thumbnail/jlO5R-FlQi2jc7XIDi0WIw?viewBox=1153%2C328&ownerId=A3RL6H4CGV9EDF&groupShareToken=BMjypj3yTjKYQZeEzFAEUw.WzZF0j057nuvZB9AjXgh1l "EBS Project")

# Service Management Database

This is the database module of the Service Management Applications. You'll find all the database scripts and binaries in this repository.

This project has been fully containerized. The commands to help you use this container is outlined below. When you run the container, the following will be done for you:

1. Ubuntu base image with utility tools installed
2. Postgres 13 installed and configured
3. Database engine tuned to run in a modest server
4. Database user created based on the passed variable or created from the default
5. Database created based on the passed variable or created from the default
6. Liquibase migration against the created database - effectively giving you the latest SM schema. Note that you can override the default contexts if you need fixture data, etc.

## Contents of this repository

```bash
.
├── Dockerfile
├── LICENSE.txt
├── README.md
├── build
│   ├── liquibase
│   └── util
└── config.sh
```

#### Dockerfile and config.sh
Contains all the containerization steps. The config.sh is the entrypoint and you'll find all the database provisioning in there.

#### Build/liquibase directory
Contains all the liquibase scripts and binaries.

#### Build/util
Contains all utility scripts to make automation easier


## Using this container

Usage can be classified into two types: database development and general usage. The following environment variables/parameters are available (shown below with their respective default values) and can be set during `docker run` invocation:

```bash
postgres_local_auth_method=trust
postgres_host_auth_method=trust
postgres_listen_address=*
db_user=ebsuser
db_pass=3nt3rpr1SE!
db_name=smtemplatedb
pg_driver=postgresql.jar
lq_contexts=schema,template
lq_labels=clean
```

### Using Docker Compose

Note that the example command below will create the container off of the nightly build (tag=dev). Change the tag (or any parameter) from the `deploy/.env` file as needed - prior to running docker compose.
If you are okay with keeping all the defaults or have modified the env file according to what you need, then `cd` into the `deploy` directory, and:

* Start the sm-db container/service:
```bash
docker compose -f sm-db.yml up -d
```

A few useful commands:

* To stop the cs-db container/service
```bash
docker compose -f sm-db.yml down
```
* To check if the container is now running
```bash
docker compose -f sm-db.yml ps
```

>You can also use this method for database development, you just need to make sure that `deploy/.env` has the parameters set to what you need.

### Using Docker Run (more complex but more flexibility)

As mentioned above, the container will set up and configure everything you need. So you can focus on just writing SQL or database scripts. As long as they are in the build directory, the container will pick it up.

**Steps**


* Make sure your repository is up to date with remote (ie. `git pull --all`)
* Write your code, ex. for liquibase, make sure the SQL files are in build/liquibase/changesets directory and specified in a changelog XML (see [Database Management Guideline](https://ebsproject.atlassian.net/wiki/spaces/DB/pages/104235022/Database+Change+Management))
* Build the image. Make sure you are in the root directory of this repository, then run

```bash
docker build --force-rm=true -t sm-db .
```

* If the build succeeds, you should now have the docker image locally. You can then start and initialize the container. You have two options depending on wether or not you want the database data to persist. Change variable values as you see fit (-v).
	* Persist data across docker runs:
	```bash
	docker run --detach --name sm-db -h sm-db -p 5433:5432 --health-cmd="pg_isready -U postgres || exit 1" -e "db_name=sm_db" -e "db_user=kevin" -e "lq_contexts=schema,template,fixture" -e "lq_labels=clean,develop" -v postgres_etc:/etc/postgresql -v postgres_log:/var/log/postgresql -v postgres_lib:/var/lib/postgresql -it sm-db:latest
	```
	* Do not persist data (whenever container is removed via `docker rm`, the data goes away with it):
	```bash
	docker run --detach --name sm-db -h sm-db -p 5433:5432 --health-cmd="pg_isready -U postgres || exit 1" -e "db_name=sm_db" -e "db_user=kevin" -e "lq_contexts=schema,template,fixture" -e "lq_labels=clean,develop" -it sm-db:latest
	```

* Wait a minute or two. Feel free to check the status of the schema migration via `docker logs sm-db`.
* You now have a running Postgres 13 on port 5433 with all the latest changes. You can either connect to it to port 5433 from outside the container, or go inside the container and check via psql

```bash
docker exec -ti sm-db bash
su - postgres
psql
```

* Lastly, you have the option to either **keep the container running** as long as you're making your database changes, then invoking liquibase within the container to test. This way you save time by not having to rebuild the image everytime. Once you are happy with your work, push your liquibase changesets to this repository.

>Note that the example commands above creates a schema with fixture data. If you don't want that, modify the liquibase context being passed, ie. remove the `fixture` context
