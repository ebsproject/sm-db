--liquibase formatted sql

--changeset postgres:add_collection_layout_data context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: DB-162 Create_sm_baseline_database


--
-- Inserting data into table sample.collection_container_type
--
INSERT INTO sample.collection_container_type
(id, "name", description, code, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES
(1, 'plate 96', 'plate', 'P', 1, '2021-01-11 14:54:51.9369370', null, 1, null, false);


--
-- Inserting data into table sample.collection_layout
--
INSERT INTO sample.collection_layout
(id, num_columns, num_rows, num_well, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, collection_container_type_id)
VALUES
(1, 10, 50, 500, 1, '2021-01-11 14:54:51.9369370', null, 1, null, false, 1);


--
-- Inserting data into table analyticalsampling.loadtype
--
INSERT INTO sample.load_type
(id, "name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES
(1, 'Columns', 'load by column', 1, '2021-01-11 14:54:51.3826080', null, 1, null, false),
(2, 'Rows', 'load by row', 1, '2021-01-11 14:54:51.3826080', null, 1, null, false),
(3, 'CIMMYT format', 'load by CIMMYT format', 1, '2021-01-11 14:54:51.3826080', null, 1, null, false);



--
-- Inserting data into table analyticalsampling.mixturemethod
--
INSERT INTO sample.mixture_method
(id, "name", code, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES
(1, 'Bulk', 'B', 1, '2021-01-11 14:54:51.3826080', null, 1, null, false),
(2, 'Pool', 'P', 1, '2021-01-11 14:54:51.3826080', null, 1, null, false);


--Revert Changes
--rollback TRUNCATE TABLE sample.mixture_method CASCADE;
--rollback TRUNCATE TABLE sample.load_type CASCADE;
--rollback TRUNCATE TABLE sample.collection_layout CASCADE;
--rollback TRUNCATE TABLE sample.collection_container_type CASCADE;