--liquibase formatted sql

--changeset postgres:add_request_data context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: DB-162 Create_sm_baseline_database



INSERT INTO sample.request
(id, requester, submition_date, admin_contact_name, icc, description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, person_id, crop_id, program_id, purpose_id, service_provider_id, status_id, service_type_id, service_id, request_code, completed_by, requester_email, admin_contact_email, list_id, total_entities)
VALUES
(1, 'Ruth Singh','2021-01-11', 'Ruth Singh', 'A1040.01', 'Shipment Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 3, 1, 6, 1, 1, 1,  'RLB-20200615-1002', null, null, null,  '1', 100),
(2, 'Sarah Hearne', '2021-01-11', 'Sarah Hearne', 'A1041.01', 'Shipment Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 1, 3, 1, 5, 1, 1, 1,  'RLB-20200718-1003', null, null, null,  '2', 100),
(3, 'Mary Reynolds', '2021-01-11', 'Mary Reynalds', 'A1042.01', 'Shipment Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 3, 1, 6, 1, 1, 1,  'RLB-20200801-1004', null, null, null,  '1', 100),
(4, 'Robbert Dittmer', '2021-01-11', 'Robbert Dittmer', 'A1095.01', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 3, 8, 6, 1, 3, 11,  'S9001DW20GS0310001',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '100'),
(5, 'Susanne Phillipie', '2021-01-11', 'Susanne Phillipie', 'A1092.01', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 3, 8, 6, 1, 3, 12,  'S7001DW20GS0312001',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '150'),
(6, 'Brie Thompson', '2021-01-11', 'Brie Thompson', 'A1098.01', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 3, 8, 6, 1, 3, 12,  'S8001DW20GS0311001',  '2021-01-11 14:43:04.1758120', null, null,  '3',  '342'),
(7, 'Gwenora Sperwell', '2021-01-11', 'Elaina Arro', 'J8460.88', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 1, 2, 8, 5, 1, 3, 12,  'S1707GM56YGS9611259',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '150'),
(8, 'Brittaney Leigh', '2021-01-11', 'Ira Rutherfoord', 'G2313.76', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 1, 8, 6, 1, 3, 12,  'S7708DW15IGS6697254',  '2021-01-11 14:43:04.1758120', null, null,  '3',  '342'),
(9, 'Cheslie Mapston', '2021-01-11', 'Cullen Dackombe', 'J0843.44', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 1, 2, 8, 5, 1, 3, 12,  'S5065GM93TGS8984068',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '150'),
(10, 'Heindrick Haversum', '2021-01-11', 'Aland Kiddell', 'Y4472.43', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 3, 8, 6, 1, 3, 12,  'S2019DW69TGS0552471',  '2021-01-11 14:43:04.1758120', null, null,  '3',  '342'),
(11, 'Tanney Gilloran', '2021-01-11', 'Kellia Drummond', 'X3745.28', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 1, 2, 8, 5, 1, 3, 12,  'S5423GM96NGS5484163',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '150'),
(12, 'Philippine Gonnin', '2021-01-11', 'Marv Aven', 'Q6383.60', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 1, 8, 6, 1, 3, 12,  'S8145DW34IGS2891773',  '2021-01-11 14:43:04.1758120', null, null,  '3',  '342'),
(13, 'Burg Emslie', '2021-01-11', 'Patti Branno', 'C1698.47', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 1, 2, 8, 5, 1, 3, 12,  'S2759GM44GGS3064395',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '150'),
(14, 'Jeanna Minihan', '2021-01-11', 'Jeffry Frosdick', 'Y7416.84', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 3, 8, 6, 1, 3, 12,  'S0554DW15IGS5242484',  '2021-01-11 14:43:04.1758120', null, null,  '3',  '342'),
(15, 'Myrtie Shory', '2021-01-11', 'Carmelle Gorstidge', 'U7851.09', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 1, 2, 8, 5, 1, 3, 12,  'S7320GM12GGS8929360',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '150'),
(16, 'Sorcha Neil', '2021-01-11', 'Adolphe Vallance', 'N7655.20', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 1, 8, 6, 1, 3, 12,  'S9287DW27TGS4504521',  '2021-01-11 14:43:04.1758120', null, null,  '3',  '342'),
(17, 'Hoebart Andreou', '2021-01-11', 'Gonzales Edlestone', 'Q3457.79', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 1, 2, 8, 5, 1, 3, 12,  'S5242GM16RGS9418667',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '150'),
(18, 'George Behr', '2021-01-11', 'Reine Avo', 'F6084.20', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 3, 8, 6, 1, 3, 12,  'S2198DW23OGS0428295',  '2021-01-11 14:43:04.1758120', null, null,  '3',  '342'),
(19, 'Andy Rene', '2021-01-11', 'Neilla Rollo', 'T8040.80', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 1, 2, 8, 5, 1, 3, 12,  'S3561GM22YGS5603155',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '150'),
(20, 'Vyky Smallpeice', '2021-01-11', 'Barthel Please', 'H5610.91', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 2, 1, 8, 6, 1, 3, 12,  'S7474DW17TGS5433299',  '2021-01-11 14:43:04.1758120', null, null,  '3',  '342'),
(21, 'Vinny Benard', '2021-01-11', 'Langston Kilmister', 'U5906.90', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120',  null, 1,  null,  false,  null, 1, 2, 8, 5, 1, 3, 12,  'S4946GM73MGS5599532',  '2021-01-11 14:43:04.1758120', null, null,  '1',  '150'),
(22, 'Reggi Dymocke', '2021-01-11', 'Ricky Leband', 'A8781.31', 'Genotyping Request',  1, '2021-01-11 14:43:04.1758120', null, 1,  null,  false, null, 2, 3, 8, 6, 1, 3, 12,  'S3406DW29JGS2159098',  '2021-01-11 14:43:04.1758120', null, null,  '3',  '342');

SELECT setval('sample.request_id_seq', (SELECT MAX(id) FROM sample.request));


--Revert Changes
--rollback TRUNCATE TABLE sample.request CASCADE;
--rollback SELECT setval('sample.request_id_seq', (SELECT MAX(id) FROM sample.request));