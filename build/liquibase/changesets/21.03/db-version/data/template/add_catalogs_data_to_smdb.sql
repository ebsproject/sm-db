--liquibase formatted sql

--changeset postgres:add_catalogs_data_to_smdb context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-162 Create_sm_baseline_database



--
-- Inserting data into table program.crop
--
INSERT INTO "program".crop
(id, code, "name", description, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, notes) VALUES
(1, 'M', 'Maize', 'Maize', '2021-02-11 18:14:12.4781440', null, 1, null, false, null),
(2, 'W', 'Wheat', 'Wheat', '2021-02-11 18:14:12.4781440', null, 1, null, false, null),
(3, 'R', 'Rice', 'Rice', '2021-02-11 18:14:12.4781440', null, 1, null, false, null);



--
-- Inserting data into table services.serviceprovider
--
INSERT INTO services.service_provider
(id, code, "name", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void) VALUES
(1, 'WMBL', 'CIMMYT Mexico Wheat Molecular Laboratory', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(2, 'ASM', 'CIMMYT India Maize Molecular Laboratory', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(3, 'AFM', 'CIMMYT Kenya Maize Molecular Laboratory', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(4, 'LAM', 'CIMMYT Mexico Maize Molecular Laboratory', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(5, 'SHL', 'CIMMYT Mexico Maize Seed Health Laboratory', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(6, 'SHL', 'CIMMYT Mexico Wheat Seed Health Laboratory', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(7, 'MZQ', 'CIMMYT Mexico Maize Quality Laboratory', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(8, 'WQL', 'CIMMYT Mexico Wheat Quality Laboratory', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(9, 'SDU', 'CIMMYT Mexico Seed Distribution Unit', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(10, 'GSL', 'IRRI Philippines Genotyping Service Lab', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(11, 'SHU', 'IRRI Philippines Seed Health Unit', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(12, 'GQNSL', 'IRRI Philippines Grain Quality Nutrition Service Lab', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false);


--
-- Inserting data into table services.servicetype
--
INSERT INTO services.service_type
(id, "name", code, description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void) VALUES
(1, 'Seed Health Analysis', 'H', 'Analysis of seed, other tissues, soil, or other materials to detect the presence or absence of microorganisms', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(2, 'Quality Analysis', 'Q', 'Analysis to measure chemical or physical properties of seed or other tissues or downstream products such as flour', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(3, 'Genotyping Analysis', 'G', 'Analysis to produce genotypic data from leaves, seeds, or other plant parts or DNAs isolated from plant parts', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(4, 'Material Transfer', 'S', 'Material transfer of seed, leaves, roots, tubers, or any plant parts, DNAs, RNAs isolated from plants, in any packets, envelopes, bags, plates, tubes', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false),
(5, 'Pathology Screening', 'P', 'Screening or serverity scoring of diseases in plants grown and infested under natural or artificial inoculation condition', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false);

--
-- Inserting data into table services.purpose
--
INSERT INTO services.purpose
(id, "name", code, description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, service_type_id) VALUES
(1, 'Seed Shipment', 'S', 'Seed health analysis to be carried out to meet compliance requirements in order to move seeds, or other material types across country borders. Materials to be shipped already exist in the database', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 1),
(2, 'Seed Introduction', 'I', 'Seed health analysis to be carried out to meet compliance requirements to import new materials into the country and these materials are non-exist in the EBS database system', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 1),
(3, 'Seed Certification', 'C', 'Seed health analysis to be carried out to meet compliance requirements to import new materials into the country and these materials are non-exist in the EBS database system', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 1),
(4, 'Quality Control: F1 Verfication, Ped Ver, Purity, Line Finishing_Fixation', 'Q', 'Quality Control: F1 Verfication, Ped Ver, Purity, Line Finishing_Fixation', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 3),
(5, 'Germplasm Characterization', 'G', 'Germplasm Characterization: Finger Printing, Diversity', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 3),
(6, 'Genome-wide Genotyping', 'W', 'Genome-wide Genotyping: GS, GWAS', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 3),
(7, 'Marker Assisted Selection', 'S', 'Marker Assisted Selection: MAS, MABC', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 3),
(8, 'Quality Control', 'QC', 'Quality Control', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 3),
(9, 'Custom Quality Analysis', 'CUQA', 'Custom Quality Analysis', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 2),
(10, 'Complete Quality Analysis', 'COQA', 'Complete Quality Analysis', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 2),
(11, 'Basic Quality Analysis', 'BAQA', 'Basic Quality Analysis', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 2),
(12, 'Nitrogen or Protein Analysis', 'NPA', 'Nitrogen or Protein Analysis', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 2),
(13, 'MLN Screening', 'MLN', 'MLN Screening', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 5),
(14, 'TLB Screening', 'TLB', 'TLB Screening', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 5),
(15, 'GLS Screening', 'GLS', 'GLS Screening', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 5),
(16, 'MSV Screening', 'MSV', 'MSV Screening', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 5),
(17, 'Regional Trial Shipments', 'RTS', 'Regional Trial Shipments', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 4),
(18, 'Genebank Materials GG', 'GMGG', 'Genebank materials (GRP) specific request from GRIN-Global', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 4),
(19, 'Breeders materials-ad hoc', 'BMA', 'Breeders’ materials-ad hoc', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 4),
(20, 'Non-propagation materials', 'NPM', 'Non-propagation materials', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 4),
(21, 'International nurseries', 'INU', 'International nurseries uniform trial (GWP) Shipment', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 4);


--
-- Inserting data into table services.service
--
INSERT INTO services.service
(id, "name", description, code, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, purpose_id) VALUES
(1, 'Seed Shipment', 'Seed health analysis to enable Seed Shipment for maize, wheat, barley, triticale', 'SS', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 1),
(2, 'Other Shipment', 'Seed health analysis to enable Shipment for flour, ground, leaves, plants, roots, soil, DNA, etc.', 'OS', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 1),
(3, 'Seed Introduction', 'Seed health analysis to enable the seed Introduction for maize, wheat, barley, triticale', 'SI', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 2),
(4, 'Other Introduction', 'Seed health analysis to enable the introduction for beans, fava beans, peas, lentils, forages, potatoes, cabbages, beets, spinach, flour, ground grains, leaves, plants, roots, soil, DNA, etc.', 'OS', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 2),
(5, 'Seed Certification', 'Seed health analysis to enable seed certification for maize, wheat, barley, triticale', 'SC', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 3),
(6, 'Other Certification', 'Seed health analysis to enable the Certification for beans, fava beans, peas, lentils, forages, potatoes, cabbages, beets, spinach, flour, ground grains, leaves, plants, roots, soil, DNA, etc.', 'OC', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 3),
(7, 'Maize Custom Quality Analyses', 'User-defined set of quality analyses from the maize quality lab', 'MQU', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 9),
(8, 'Wheat Complete Quality Analysis', 'Wheat complete analysis which includes 11 separate assays', 'WQC', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 10),
(9, 'Wheat Basic Quality Analysis', 'Wheat basic quality analysis which includes 5 separate assays', 'WQB', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 11),
(10, 'Wheat Nitrogen or Protein Analysis', 'Wheat nitrogen or protein analysis using one of three possible methods', 'WQN', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 12),
(11, '100 SNP_Panel 1 V1', '100 SNP_Panel 1 V1', '100SNP', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 8),
(12, '200 SNP_Panel 2 V1', '200 SNP_Panel 2 V1', '200SNP', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 8),
(13, '45 SNP_Diversity_Panel V1', '45 SNP_Diversity_Panel V1', '45SNP', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 5),
(14, '50 SNP_Diversity_Panel V1', '50 SNP_Diversity_Panel V1', '50SNP1', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 5),
(15, '50 SNP_Diversity_Panel V2', '50 SNP_Diversity_Panel V2', '50SNP2', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 8),
(16, '55 SNP_Panel 1 V1', '55 SNP_Panel 1 V1', '55SNP', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 4),
(17, 'DArT_Seq', 'DArT_Seq', 'DArT', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 6),
(18, 'Diversity_Panel V2', 'Diversity_Panel V2', 'Divers', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 5),
(19, 'rAmpSeq', 'rAmpSeq', 'rAmpSeq', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 6),
(20, 'rhAmpSeq', 'rhAmpSeq', 'rhAmpSeq', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 6),
(21, 'Sequencing', 'Sequencing', 'Sequencing', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 6),
(22, 'User_selected_markers', 'User_selected_markers', 'User', 1, '2021-02-11 18:14:12.4781440', null, 1, null, false, 7),
(23, 'MLN_Indexing', 'MLN_Indexing', 'MLNI', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 13),
(24, 'MLN_Screening', 'MLN_Screening', 'MLNS', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 13),
(25, 'MCMV', 'MCMV', 'MCMV', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 13),
(26, 'SCMV', 'SCMV', 'SCMV', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 13),
(27, 'TLB', 'TLB', 'TLB', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 14),
(28, 'GLS', 'GLS', 'GLS', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 15),
(29, 'MSV', 'MSV', 'MSV', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 16),
(30, 'Regional Trial Shipments', 'Regional Trial Shipments', 'RTS', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 17),
(31, 'Genebank Materials GG', 'Genebank Materials GG', 'GMGG', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 18),
(32, 'Breeders Materials Requests', 'Breeders’ Materials Requests', 'BMR', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 19),
(33, 'Non-propagation materials', 'Non-propagation materials', 'NPM', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 20),
(34, 'International nurseries', 'International nurseries', 'INU', 1, '2021-02-11 18:14:43.5120270', null, 1, null, false, 21);


--
-- Inserting data into table services.service_provider_service_type
--
INSERT INTO services.service_provider_service_type (service_type_id, service_provider_id) VALUES
(1, 5),
(1, 6),
(1, 11),
(2, 7),
(2, 8),
(2, 12),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 10),
(4, 9);


--
-- Inserting data into table services.assay_class
--
INSERT INTO services.assay_class
("name", description, tenant_id, creation_timestamp, creator_id, is_void, service_id) VALUES
('Fixed', 'Fixed', 1, now(), 1, false, 11),
('Fixed', 'Fixed', 1, now(), 1, false, 12),
('Fixed', 'Fixed', 1, now(), 1, false, 13),
('Fixed', 'Fixed', 1, now(), 1, false, 14),
('Fixed', 'Fixed', 1, now(), 1, false, 15),
('Fixed', 'Fixed', 1, now(), 1, false, 16),
('Fixed', 'Fixed', 1, now(), 1, false, 18),
('Non-target', 'Non-target', 1, now(), 1, false, 19),
('Non-target', 'Non-target', 1, now(), 1, false, 20),
('Non-target', 'Non-target', 1, now(), 1, false, 21),
('User_defined', 'Non-target', 1, now(), 1, false, 22)
;


--
-- Inserting data into table services.dataformat
--
INSERT INTO services.data_format
(id, "name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void) VALUES
(1, 'csv', 'CSV', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false),
(2, 'hdf5', 'HDF5', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false),
(3, 'xls', 'Excel file', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false);


--
-- Inserting data into table services.vendor
--
INSERT INTO services.vendor
(id, code, reference, status, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, data_format_id) VALUES
(1, 'Intertek', 'INtertek', 'Active', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 1),
(2, 'DArT', 'DArT', 'Active', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 1);


--
-- Inserting data into table services.technologyplatform
--
INSERT INTO services.technology_platform
(id, "name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, vendor_id) VALUES
(1, 'low density', 'low density', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 1),
(2, 'mid density', 'mid density', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 1),
(3, 'low density', 'low density', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 2),
(4, 'mid density', 'mid density', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 2),
(5, 'high density', 'mid density', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 2);

--
-- Inserting data into table services.vendorcontrol
--
INSERT INTO services.vendor_control
(id, "position", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, technology_platform_id) VALUES
(1, 'H11,H12', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 1),
(2, 'G12,H12', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 2),
(3, 'G12,H12', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 4),
(4, 'User defined', 1, '2021-02-11 18:15:49.5599410', null, 1, null, false, 1);

--
-- Inserting data into table program.program
--
INSERT INTO "program"."program"
(id, code, "name", "type", status, description, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, notes) VALUES
(1, 'GWP', 'Global Wheat Program', 'Breeding Program', 'Active', 'Global Wheat Program', '2021-02-11 18:14:29.0043540', null, 1, null, false, null),
(2, 'GMP', 'Global Maize Program', 'Breeding Program', 'Active', 'Global Maize Program', '2021-02-11 18:14:29.0043540', null, 1, null, false, null),
(3, 'DW', 'Durum Wheat', 'Breeding Program', 'Active', 'Durum Wheat', '2021-02-11 18:14:29.0043540', null, 1, null, false, null);


--
-- Inserting data into table program.crop_program
--
INSERT INTO "program".crop_program
(program_id, crop_id)
VALUES
(1, 2),
(2, 1),
(3, 2)
;

SELECT setval('services.service_id_seq', (SELECT MAX(id) FROM services.service));
SELECT setval('program.crop_id_seq', (SELECT MAX(id) FROM program.crop));
SELECT setval('services.service_provider_id_seq', (SELECT MAX(id) FROM services.service_provider));
SELECT setval('services.service_type_id_seq', (SELECT MAX(id) FROM services.service_type));
SELECT setval('services.purpose_id_seq', (SELECT MAX(id) FROM services.purpose));
SELECT setval('services.assay_class_id_seq', (SELECT MAX(id) FROM services.assay_class));
SELECT setval('services.data_format_id_seq', (SELECT MAX(id) FROM services.data_format));
SELECT setval('services.vendor_id_seq', (SELECT MAX(id) FROM services.vendor));
SELECT setval('services.technology_platform_id_seq', (SELECT MAX(id) FROM services.technology_platform));
SELECT setval('services.vendor_control_id_seq', (SELECT MAX(id) FROM services.vendor_control));
SELECT setval('program.program_id_seq', (SELECT MAX(id) FROM program.program));


--Revert Chages
--rollback TRUNCATE TABLE "program".crop_program CASCADE;
--rollback TRUNCATE TABLE "program"."program" CASCADE;
--rollback TRUNCATE TABLE services.vendor_control CASCADE;
--rollback TRUNCATE TABLE services.technology_platform CASCADE;
--rollback TRUNCATE TABLE services.vendor CASCADE;
--rollback TRUNCATE TABLE services.data_format CASCADE;
--rollback TRUNCATE TABLE services.assay_Class CASCADE;
--rollback TRUNCATE TABLE services.service_provider_service_type CASCADE;
--rollback TRUNCATE TABLE services.service CASCADE;
--rollback TRUNCATE TABLE services.purpose CASCADE;
--rollback TRUNCATE TABLE services.service_type CASCADE;
--rollback TRUNCATE TABLE services.service_provider CASCADE;
--rollback TRUNCATE TABLE program.crop CASCADE;

--rollback SELECT setval('services.service_id_seq', (SELECT MAX(id) FROM services.service));
--rollback SELECT setval('program.crop_id_seq', (SELECT MAX(id) FROM program.crop));
--rollback SELECT setval('services.service_provider_id_seq', (SELECT MAX(id) FROM services.service_provider));
--rollback SELECT setval('services.service_type_id_seq', (SELECT MAX(id) FROM services.service_type));
--rollback SELECT setval('services.purpose_id_seq', (SELECT MAX(id) FROM services.purpose));
--rollback SELECT setval('services.assay_class_id_seq', (SELECT MAX(id) FROM services.assay_class));
--rollback SELECT setval('services.data_format_id_seq', (SELECT MAX(id) FROM services.data_format));
--rollback SELECT setval('services.vendor_id_seq', (SELECT MAX(id) FROM services.vendor));
--rollback SELECT setval('services.technology_platform_id_seq', (SELECT MAX(id) FROM services.technology_platform));
--rollback SELECT setval('services.vendor_control_id_seq', (SELECT MAX(id) FROM services.vendor_control));
--rollback SELECT setval('program.program_id_seq', (SELECT MAX(id) FROM program.program));
