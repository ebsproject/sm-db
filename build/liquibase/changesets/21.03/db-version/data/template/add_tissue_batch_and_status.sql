--liquibase formatted sql

--changeset postgres:add_tissue_batch_and_status context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-162 Create_sm_baseline_database


--
-- Inserting data into table sample.tissue_type
--
INSERT INTO sample.tissue_type
(id, "name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES
(1, 'Seed', 'Seed', 1, '2021-01-11 14:54:51.3826080', null, 1, null, false),
(2, 'Leaf', 'Leaf', 1, '2021-01-11 14:54:51.3826080', null, 1, null, false);

--
-- Inserting data into table sample.batch_code
--
INSERT INTO sample.batch_code
(id, "name", code, last_sample_number, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, service_provider_id, last_batch_number, last_plate_number)
VALUES
(1, 'BS1004BW-DW-QC200313', 'BS1004BW-DW-QC200313', 0, 1, '2021-01-11 14:54:51.9369370', null, 1, null, false, 4, 0, 0);


--
-- Inserting data into table sample.status
--
INSERT INTO sample.status
(id, "name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES
(1, 'New', 'New', 1, '2020-11-05 17:08:53.2090000', null, 1, null, false),
(2, 'Approved', 'Approved', 1, '2020-11-05 17:15:28.7220000', null, 1, null, false),
(3, 'Rejected', 'Rejected', 1, '2020-11-05 17:18:06.1790000', null, 1, null, false),
(4, 'In Batch', 'Batch', 1, '2020-11-05 17:19:30.2810000', null, 1, null, false),
(5, 'Create Design', 'Create Design', 1, '2020-11-05 17:20:18.3890000', null, 1, null, false);


SELECT setval('sample.tissue_type_id_seq', (SELECT MAX(id) FROM sample.tissue_type));
SELECT setval('sample.batch_code_id_seq', (SELECT MAX(id) FROM sample.batch_code));
SELECT setval('sample.status_id_seq', (SELECT MAX(id) FROM sample.status));

--Revert Changes
--rollback TRUNCATE TABLE sample.tissue_type CASCADE;
--rollback TRUNCATE TABLE sample.batch_code CASCADE;
--rollback TRUNCATE TABLE sample.status CASCADE;

--rollback SELECT setval('sample.tissue_type_id_seq', (SELECT MAX(id) FROM sample.tissue_type));
--rollback SELECT setval('sample.batch_code_id_seq', (SELECT MAX(id) FROM sample.batch_code));
--rollback SELECT setval('sample.status_id_seq', (SELECT MAX(id) FROM sample.status));