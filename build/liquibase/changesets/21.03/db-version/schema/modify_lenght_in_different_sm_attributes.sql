--liquibase formatted sql

--changeset postgres:modify_lenght_in_different_sm_attributes context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-162 Create SM Baseline Database


ALTER TABLE services.service_provider ALTER COLUMN name TYPE varchar(200);

ALTER TABLE services.service_type ALTER COLUMN description TYPE varchar(500); 

ALTER TABLE services.purpose ALTER COLUMN name TYPE varchar(200);  

ALTER TABLE services.purpose ALTER COLUMN description TYPE varchar(500);  

ALTER TABLE services.service ALTER COLUMN name TYPE varchar(200);  

ALTER TABLE services.service ALTER COLUMN description TYPE varchar(500);  

ALTER TABLE sample.plant_entry_list ALTER COLUMN germplasm_code TYPE varchar(200);

ALTER TABLE sample.plant_entry_list ALTER COLUMN pedigree TYPE varchar(500); 


--Revert Changes
--rollback ALTER TABLE services.service_provider ALTER COLUMN name TYPE varchar(50);
--rollback ALTER TABLE services.service_type ALTER COLUMN description TYPE varchar(50); 
--rollback ALTER TABLE services.purpose ALTER COLUMN name TYPE varchar(50);  
--rollback ALTER TABLE services.purpose ALTER COLUMN description TYPE varchar(50);  
--rollback ALTER TABLE services.service ALTER COLUMN name TYPE varchar(50);  
--rollback ALTER TABLE services.service ALTER COLUMN description TYPE varchar(50);  
--rollback ALTER TABLE sample.plant_entry_list ALTER COLUMN germplasm_code TYPE varchar(50);
--rollback ALTER TABLE sample.plant_entry_list ALTER COLUMN pedigree TYPE varchar(50); 

