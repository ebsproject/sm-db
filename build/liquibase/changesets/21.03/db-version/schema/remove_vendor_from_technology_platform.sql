--liquibase formatted sql

--changeset postgres:remove_vendor_from_technology_platform context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-162 Create SM Baseline Database



ALTER TABLE services.vendor
  DROP  CONSTRAINT "FK_vendor_technology_platform";

ALTER TABLE services.vendor 
 DROP COLUMN IF EXISTS technology_platform_id;

ALTER TABLE services.technology_platform 
 ADD COLUMN vendor_id integer NULL;

ALTER TABLE services.technology_platform ADD CONSTRAINT "FK_technology_platform_vendor"
	FOREIGN KEY (vendor_id) REFERENCES services.vendor (id) ON DELETE No Action ON UPDATE No Action
;


--Revert Changes
--rollback ALTER TABLE services.technology_platform DROP CONSTRAINT "FK_technology_platform_vendor";
--rollback ALTER TABLE services.technology_platform DROP COLUMN vendor_id;
--rollback ALTER TABLE services.vendor ADD COLUMN technology_platform_id integer;
--rollback ALTER TABLE services.vendor ADD CONSTRAINT "FK_vendor_technology_platform" FOREIGN KEY (technology_platform_id) REFERENCES services.technology_platform(id);

