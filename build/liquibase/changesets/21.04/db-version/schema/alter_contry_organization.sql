--liquibase formatted sql
--changeset postgres:add_organization_and_country_into_request_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-162 Create SM Baseline Database

ALTER TABLE sample.request ADD COLUMN country_id integer NULL DEFAULT 0;
ALTER TABLE sample.request ADD COLUMN organization_id integer NULL DEFAULT 0;
--Revert Changes
--rollback ALTER TABLE sample.request DROP COLUMN country_id;
--rollback ALTER TABLE sample.request DROP COLUMN organization_id;
