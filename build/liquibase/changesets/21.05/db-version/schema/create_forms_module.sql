--liquibase formatted sql

--changeset kpalis:add_forms_module context:schema splitStatements:false rollbackSplitStatements:false runOnChange:false
ALTER TABLE services.service 
 ADD COLUMN form_id integer NULL;

CREATE TABLE sample.field
(
	name varchar(50) NULL,
	label varchar(50) NULL,
	type integer NULL, --type of the field in integer representation, example: 1=input, 2=dropdown
	data_type integer NULL, --type of data allowed in integer representation, example: 1=text, 2=number
	tooltip varchar(50) NULL,
	default_value varchar(50) NULL,
	minimum_length real NULL,
	maximum_length real NULL,
	validation_regex varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('sample."field_id_seq"'::text)::regclass),
	fields_scale_value_id integer NULL
)
;

CREATE TABLE sample.fields_scale_value
(
	field_name varchar(50) NULL,
	field_scale_value varchar(50) NULL,
	scale_value_name varchar(50) NULL,
	sort_order integer NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('sample."fields_scale_value_id_seq"'::text)::regclass)
)
;

CREATE TABLE sample.form
(
	name varchar(50) NULL,
	description varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('sample."form_id_seq"'::text)::regclass)
)
;

CREATE TABLE sample.form_fields
(
	is_required boolean NULL,
	field_order integer NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('sample."form_fields_id_seq"'::text)::regclass),
	field_id integer NOT NULL,
	form_id integer NOT NULL
)
;

CREATE SEQUENCE sample.field_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE sample.fields_scale_value_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE sample.form_fields_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE sample.form_id_seq INCREMENT 1 START 1;

ALTER TABLE sample.field ADD CONSTRAINT "PK_field"
	PRIMARY KEY (id)
;

ALTER TABLE sample.fields_scale_value ADD CONSTRAINT "PK_fields_scale_value"
	PRIMARY KEY (id)
;

ALTER TABLE sample.form ADD CONSTRAINT "PK_form"
	PRIMARY KEY (id)
;

ALTER TABLE sample.form_fields ADD CONSTRAINT "PK_form_fields"
	PRIMARY KEY (id)
;

ALTER TABLE sample.field ADD CONSTRAINT "FK_field_fields_scale_value"
	FOREIGN KEY (fields_scale_value_id) REFERENCES sample.fields_scale_value (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE sample.form_fields ADD CONSTRAINT "FK_form_fields_field"
	FOREIGN KEY (field_id) REFERENCES sample.field (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE sample.form_fields ADD CONSTRAINT "FK_form_fields_form"
	FOREIGN KEY (form_id) REFERENCES sample.form (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE services.service ADD CONSTRAINT "FK_service_form"
	FOREIGN KEY (form_id) REFERENCES sample.form (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN sample.field.type
	IS 'type of the field in integer representation, example: 1=input, 2=dropdown'
;

COMMENT ON COLUMN sample.field.data_type
	IS 'type of data allowed in integer representation, example: 1=text, 2=number'
;

COMMENT ON COLUMN sample.field.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN sample.field.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN sample.field.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN sample.field.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN sample.field.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN sample.field.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN sample.fields_scale_value.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN sample.fields_scale_value.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN sample.fields_scale_value.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN sample.fields_scale_value.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN sample.fields_scale_value.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN sample.fields_scale_value.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN sample.form.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN sample.form.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN sample.form.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN sample.form.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN sample.form.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN sample.form.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN sample.form_fields.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN sample.form_fields.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN sample.form_fields.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN sample.form_fields.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN sample.form_fields.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN sample.form_fields.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN services.service.description
	IS 'Description of the Service'
;

COMMENT ON COLUMN services.service.form_id
	IS ''
;

COMMENT ON COLUMN services.service.name
	IS 'Name of the Service'
;

