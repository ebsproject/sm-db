--liquibase formatted sql

--changeset postgres:add_ebs_forms_data context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM-225 Add data to ebs_forms tables


INSERT INTO ebsform.form_type (id, "name",tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void) VALUES
	 (1, 'Tab',1,'2021-08-03 15:25:41.041572',NULL,1,NULL,false),
	 (2, 'Group',1,'2021-08-03 15:21:45.513664',NULL,1,NULL,false),
	 (3, 'Component',1,'2021-08-03 15:26:07.140446',NULL,1,NULL,false);


INSERT INTO ebsform.ebs_form (id, "name",tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,form_type_id) VALUES
	 (1,'CIMMYT Request Genotyping Service',1,'2021-08-03 15:24:07.929918',NULL,1,NULL,false,2);

INSERT INTO ebsform.tab (id, "index","name",tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,ebs_form_id) VALUES
	 (1, 3,'REVIEW',1,'2021-08-03 15:29:54.908135',NULL,1,NULL,false,1),
	 (2, 0,'ENTRY LIST',1,'2021-08-03 15:28:15.477912',NULL,1,NULL,false,1),
	 (3, 1,'BASIC',1,'2021-08-03 15:28:42.454343',NULL,1,NULL,false,1),
	 (4, 2,'SERVICES',1,'2021-08-03 15:29:08.797944',NULL,1,NULL,false,1);


INSERT INTO customform.validation_regex (id, regex,"name",tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void) VALUES
	 (1, '^[a-zA-Z ]*$','Person Name',1,'2021-08-03 15:37:09.733529',NULL,1,NULL,false),
	 (2, '.+@cgiar.org.+@cimmyt.org.+@cornell.org.+@irri.org','Email',1,'2021-08-03 15:38:25.688734',NULL,1,NULL,false),
	 (3, 'MM/DD/YYYY','Date',1,'2021-08-03 15:39:05.705394',NULL,1,NULL,false),
	 (4, '[\\d]+[A-Za-z0-9\\s,\\.]+?[\\d\\-]+','Address',1,'2021-08-03 15:39:56.6846',NULL,1,NULL,false);

INSERT INTO customform.data_type (id, "name",description,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void) VALUES
	 (1, 'String','String',1,'2021-08-03 15:34:12.984645',NULL,1,NULL,false),
	 (2, 'Integer','Integer',1,'2021-08-03 15:34:40.15818',NULL,1,NULL,false),
	 (3, 'Date','Date',1,'2021-08-03 15:34:59.569532',NULL,1,NULL,false),
	 (4, 'API','API',1,'2021-08-03 15:35:24.540808',NULL,1,NULL,false);



INSERT INTO customform.form (id, "name",description,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,form_type_id) VALUES
	 (1, 'CIMMYT Request Genotyping Service','Genotyping Services',1,'2021-08-03 15:32:27.507132',NULL,1,NULL,false,2);


INSERT INTO customform.field (id, "name","label",tooltip,default_value,is_required,"order",is_base,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,component_id,data_type_id,fields_scale_value_id,validation_regex_id,form_id,group_id,tab_id) VALUES
	 (1, 'idProgram','Program','Program',NULL,true,1,true,1,'2021-08-03 15:43:59.286256',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,3),
	 (2, 'idCrop','Crop','Crop',NULL,true,2,true,1,'2021-08-03 15:45:11.500342',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,3),
	 (3, 'idRequester','Requester','Requester',NULL,true,3,true,1,'2021-08-03 15:46:20.313201',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,3),
	 (4, 'idRequesterEmail','Requester Email','Requester Email',NULL,true,4,true,1,'2021-08-03 15:48:15.353439',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,3),
	 (5, 'idTissueType','Tissue Type','Tissue Type',NULL,true,5,true,1,'2021-08-03 15:49:25.22128',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,3),
	 (6, 'idAdminContact','Admin Contact','Admin Contact',NULL,true,6,true,1,'2021-08-03 15:50:54.369219',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,3),
	 (7, 'idAdminEmail','Admin Email','Admin Email',NULL,true,7,true,1,'2021-08-03 15:52:36.950963',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,3),
	 (8, 'idICC','ICC','ICC',NULL,true,8,false,1,'2021-08-03 15:53:40.523821',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,3),
	 (9, 'idSubDate','Submission Date','Submission Date',NULL,true,9,true,1,'2021-08-03 15:55:22.650115',NULL,1,NULL,false,NULL,3,NULL,3,1,NULL,3),
	 (10, 'idCompleBy','Complete By','Complete By',NULL,true,10,true,1,'2021-08-03 15:56:33.978854',NULL,1,NULL,false,NULL,3,NULL,3,1,NULL,3);
INSERT INTO customform.field (id, "name","label",tooltip,default_value,is_required,"order",is_base,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,component_id,data_type_id,fields_scale_value_id,validation_regex_id,form_id,group_id,tab_id) VALUES
	 (11, 'idNotes','Notes','Notes',NULL,NULL,11,true,1,'2021-08-03 15:57:37.332117',NULL,1,NULL,false,NULL,1,NULL,4,1,NULL,3),
	 (12, 'idServiceProvoder','Service Provider','Service Provider',NULL,true,1,true,1,'2021-08-03 15:59:02.871021',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,4),
	 (13, 'idServiceType','Service Type','Service Type',NULL,true,2,true,1,'2021-08-03 16:00:16.058529',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,4),
	 (14, 'idPurpose','Purpose','Purpose',NULL,true,3,true,1,'2021-08-03 16:01:12.157342',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,4),
	 (15, 'idService','Service','Service',NULL,true,4,true,1,'2021-08-03 16:01:51.788328',NULL,1,NULL,false,NULL,4,NULL,NULL,1,NULL,4);


     

UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=1;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=2;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=3;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=4;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=5;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=6;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=7;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=8;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=9;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=10;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=11;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=12;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=13;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=14;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=15;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=16;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=17;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=18;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=19;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=20;
UPDATE sample.request
SET admin_contact_id = 33, form_id = 1, requester_owner_id = 33, tissue_type_id= 2
WHERE id=21;
UPDATE sample.request
SET admin_contact_id = 32, form_id = 1, requester_owner_id = 32, tissue_type_id= 2
WHERE id=22;

