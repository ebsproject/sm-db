--liquibase formatted sql

--changeset postgres:add_columns_to_request_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-225 Add columns to request table


ALTER TABLE sample.request 
 DROP COLUMN IF EXISTS admin_contact_email;
 
ALTER TABLE sample.request 
 DROP COLUMN IF EXISTS person_id;

ALTER TABLE sample.request 
 DROP COLUMN IF EXISTS admin_contact_name;

ALTER TABLE sample.request 
 DROP COLUMN IF EXISTS country_id;

ALTER TABLE sample.request 
 DROP COLUMN IF EXISTS description;

ALTER TABLE sample.request 
 DROP COLUMN IF EXISTS icc;

ALTER TABLE sample.request 
 DROP COLUMN IF EXISTS organization_id;

ALTER TABLE sample.request 
 DROP COLUMN IF EXISTS requester;

ALTER TABLE sample.request 
 DROP COLUMN IF EXISTS requester_email;

ALTER TABLE sample.request 
 ADD COLUMN admin_contact_id integer NULL;

ALTER TABLE sample.request 
 ADD COLUMN form_id integer NULL;

ALTER TABLE sample.request 
 ADD COLUMN note varchar(500) NULL;

ALTER TABLE sample.request 
 ADD COLUMN requester_owner_id integer NULL;

ALTER TABLE sample.request 
 ADD COLUMN tissue_type_id integer NULL;

ALTER TABLE sample.request ADD CONSTRAINT "FK_request_form"
	FOREIGN KEY (form_id) REFERENCES customform.form (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE sample.request ADD CONSTRAINT "FK_request_tissue_type"
	FOREIGN KEY (tissue_type_id) REFERENCES sample.tissue_type (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN sample.request.admin_contact_id
	IS 'Refers to the admin person to contact about the request.'
;


COMMENT ON COLUMN sample.request.note
	IS 'Additional information about the request'
;

--Revert Changes
--rollback ALTER TABLE sample.request ADD COLUMN admin_contact_email varchar(50) NULL;
--rollback ALTER TABLE sample.request ADD COLUMN person_id int4 NULL;
--rollback ALTER TABLE sample.request ADD COLUMN admin_contact_name varchar(50) NULL;
--rollback ALTER TABLE sample.request ADD COLUMN country_id int4;
--rollback ALTER TABLE sample.request ADD COLUMN description varchar(50) NULL;
--rollback ALTER TABLE sample.request ADD COLUMN icc varchar(50) NULL;
--rollback ALTER TABLE sample.request ADD COLUMN organization_id int4;
--rollback ALTER TABLE sample.request ADD COLUMN requester varchar(50) NULL;
--rollback ALTER TABLE sample.request ADD COLUMN requester_email varchar(50) NULL;
--rollback ALTER TABLE sample.request DROP COLUMN admin_contact_id;
--rollback ALTER TABLE sample.request DROP COLUMN form_id;
--rollback ALTER TABLE sample.request DROP COLUMN note;
--rollback ALTER TABLE sample.request DROP COLUMN requester_owner_id;
--rollback ALTER TABLE sample.request DROP COLUMN tissue_type_id;