--liquibase formatted sql

--changeset postgres:create_forms_ebsforms_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-225 Create tables for forms and ebs forms



DROP TABLE IF EXISTS sample.form_fields;

DROP TABLE IF EXISTS sample.field;

DROP TABLE IF EXISTS sample.fields_scale_value;

ALTER TABLE services.service DROP CONSTRAINT "FK_service_form";

DROP TABLE IF EXISTS sample.form;

CREATE SCHEMA customform;

CREATE SCHEMA ebsform;

CREATE TABLE customform.data_type
(
	name varchar(50) NULL,
	description varchar(250) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('customform."data_type_id_seq"'::text)::regclass)
)
;

CREATE TABLE customform.field
(
	name varchar(50) NULL,
	label varchar(250) NULL,
	tooltip varchar(150) NULL,
	default_value varchar(50) NULL,
	is_required boolean NULL,
	"order" integer NULL,
	is_base boolean NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('customform."field_id_seq"'::text)::regclass),
	component_id integer NULL,
	data_type_id integer NULL,
	fields_scale_value_id integer NULL,
	validation_regex_id integer NULL,
	form_id integer NULL,
	group_id integer NULL,
	tab_id integer NULL
)
;

CREATE TABLE customform.form
(
	name varchar(50) NULL,
	description varchar(250) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('customform."form_id_seq"'::text)::regclass),
	form_type_id integer NULL
)
;

CREATE TABLE customform.validation_regex
(
	regex varchar(50) NULL,
	name varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('customform."validation_regex_id_seq"'::text)::regclass)
)
;

CREATE TABLE customform.value
(
	value varchar(150) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('customform."value_id_seq"'::text)::regclass),
	request_id integer NULL,
	field_id integer NULL
)
;

CREATE TABLE ebsform.check_prop
(
	key varchar(50) NULL,
	value varchar(150) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."check_prop_id_seq"'::text)::regclass),
	component_id integer NULL
)
;

CREATE TABLE ebsform.component
(
	name varchar(50) NULL,
	title varchar(50) NULL,
	size varchar(50) NULL,
	label varchar(50) NULL,
	default_value varchar(50) NULL,
	on_change varchar(50) NULL,
	rule varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."component_id_seq"'::text)::regclass),
	group_id integer NULL,
	tab_id integer NULL,
	component_type_id integer NULL,
	ebs_form_id integer NULL
)
;

CREATE TABLE ebsform.component_type
(
	name varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."component_type_id_seq"'::text)::regclass)
)
;

CREATE TABLE ebsform.ebs_form
(
	name varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."ebs_form_id_seq"'::text)::regclass),
	form_type_id integer NULL
)
;

CREATE TABLE ebsform.form_type
(
	name varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."form_type_id_seq"'::text)::regclass)
)
;

CREATE TABLE ebsform."group"
(
	name varchar(50) NULL,
	title varchar(250) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."group_id_seq"'::text)::regclass),
	ebs_form_id integer NULL
)
;

CREATE TABLE ebsform.helper
(
	title varchar(250) NULL,
	placement varchar(50) NULL,
	arrow boolean NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."helper_id_seq"'::text)::regclass),
	component_id integer NULL
)
;

CREATE TABLE ebsform.input_prop
(
	key varchar(50) NULL,
	value varchar(250) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."input_prop_id_seq"'::text)::regclass),
	component_id integer NULL
)
;

CREATE TABLE ebsform.option
(
	label varchar(50) NULL,
	value integer NULL,
	color varchar(50) NULL,
	disabled boolean NULL,
	default_value varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."option_id_seq"'::text)::regclass),
	component_id integer NULL
)
;

CREATE TABLE ebsform.tab
(
	index integer NULL,
	name varchar(50) NULL,
	tenant_id integer NOT NULL,	-- Id of the selected Tenant
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false,	-- Indicator whether the record is deleted (true) or not (false)
	id integer NOT NULL   DEFAULT NEXTVAL(('ebsform."tab_id_seq"'::text)::regclass),
	ebs_form_id integer NULL
)
;

CREATE SEQUENCE customform.data_type_id_seq INCREMENT 1 START 5;

CREATE SEQUENCE customform.field_id_seq INCREMENT 1 START 16;

CREATE SEQUENCE customform.form_id_seq INCREMENT 1 START 2;

CREATE SEQUENCE customform.validation_regex_id_seq INCREMENT 1 START 5;

CREATE SEQUENCE customform.value_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE ebsform.check_prop_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE ebsform.component_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE ebsform.component_type_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE ebsform.ebs_form_id_seq INCREMENT 1 START 2;

CREATE SEQUENCE ebsform.form_type_id_seq INCREMENT 1 START 4;

CREATE SEQUENCE ebsform.group_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE ebsform.helper_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE ebsform.input_prop_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE ebsform.option_id_seq INCREMENT 1 START 1;

CREATE SEQUENCE ebsform.tab_id_seq INCREMENT 1 START 5;

ALTER TABLE customform.data_type ADD CONSTRAINT "PK_data_type"
	PRIMARY KEY (id)
;

ALTER TABLE customform.field ADD CONSTRAINT "PK_field"
	PRIMARY KEY (id)
;

ALTER TABLE customform.form ADD CONSTRAINT "PK_form"
	PRIMARY KEY (id)
;

ALTER TABLE customform.validation_regex ADD CONSTRAINT "PK_validation_regex"
	PRIMARY KEY (id)
;

ALTER TABLE customform.value ADD CONSTRAINT "PK_value"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform.check_prop ADD CONSTRAINT "PK_check_prop"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform.component ADD CONSTRAINT "PK_component"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform.component_type ADD CONSTRAINT "PK_component_type"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform.ebs_form ADD CONSTRAINT "PK_ebs_form"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform.form_type ADD CONSTRAINT "PK_form_type"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform."group" ADD CONSTRAINT "PK_group"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform.helper ADD CONSTRAINT "PK_helper"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform.input_prop ADD CONSTRAINT "PK_input_prop"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform.option ADD CONSTRAINT "PK_option"
	PRIMARY KEY (id)
;

ALTER TABLE ebsform.tab ADD CONSTRAINT "PK_tab"
	PRIMARY KEY (id)
;

ALTER TABLE customform.field ADD CONSTRAINT "FK_field_component"
	FOREIGN KEY (component_id) REFERENCES ebsform.component (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE customform.field ADD CONSTRAINT "FK_field_data_type"
	FOREIGN KEY (data_type_id) REFERENCES customform.data_type (id) ON DELETE No Action ON UPDATE No Action
;


ALTER TABLE customform.field ADD CONSTRAINT "FK_field_form"
	FOREIGN KEY (form_id) REFERENCES customform.form (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE customform.field ADD CONSTRAINT "FK_field_group"
	FOREIGN KEY (group_id) REFERENCES ebsform."group" (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE customform.field ADD CONSTRAINT "FK_field_tab"
	FOREIGN KEY (tab_id) REFERENCES ebsform.tab (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE customform.field ADD CONSTRAINT "FK_field_validation_regex"
	FOREIGN KEY (validation_regex_id) REFERENCES customform.validation_regex (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE customform.form ADD CONSTRAINT "FK_form_form_type"
	FOREIGN KEY (form_type_id) REFERENCES ebsform.form_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE customform.value ADD CONSTRAINT "FK_value_field"
	FOREIGN KEY (field_id) REFERENCES customform.field (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE customform.value ADD CONSTRAINT "FK_value_request"
	FOREIGN KEY (request_id) REFERENCES sample.request (id) ON DELETE No Action ON UPDATE No Action
;


ALTER TABLE ebsform.component ADD CONSTRAINT "FK_component_component_type"
	FOREIGN KEY (component_type_id) REFERENCES ebsform.component_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform.component ADD CONSTRAINT "FK_component_ebs_form"
	FOREIGN KEY (ebs_form_id) REFERENCES ebsform.ebs_form (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform.component ADD CONSTRAINT "FK_component_group"
	FOREIGN KEY (group_id) REFERENCES ebsform."group" (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform.component ADD CONSTRAINT "FK_component_tab"
	FOREIGN KEY (tab_id) REFERENCES ebsform.tab (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform.ebs_form ADD CONSTRAINT "FK_ebs_form_form_type"
	FOREIGN KEY (form_type_id) REFERENCES ebsform.form_type (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform."group" ADD CONSTRAINT "FK_group_ebs_form"
	FOREIGN KEY (ebs_form_id) REFERENCES ebsform.ebs_form (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform.helper ADD CONSTRAINT "FK_helper_component"
	FOREIGN KEY (component_id) REFERENCES ebsform.component (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform.input_prop ADD CONSTRAINT "FK_input_prop_component"
	FOREIGN KEY (component_id) REFERENCES ebsform.component (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform.check_prop ADD CONSTRAINT "FK_check_prop_component"
	FOREIGN KEY (component_id) REFERENCES ebsform.component (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform.option ADD CONSTRAINT "FK_option_component"
	FOREIGN KEY (component_id) REFERENCES ebsform.component (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE ebsform.tab ADD CONSTRAINT "FK_tab_ebs_form"
	FOREIGN KEY (ebs_form_id) REFERENCES ebsform.ebs_form (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN customform.data_type.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN customform.data_type.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN customform.data_type.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN customform.data_type.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN customform.data_type.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN customform.data_type.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN customform.field.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN customform.field.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN customform.field.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN customform.field.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN customform.field.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN customform.field.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN customform.form.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN customform.form.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN customform.form.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN customform.form.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN customform.form.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN customform.form.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN customform.validation_regex.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN customform.validation_regex.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN customform.validation_regex.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN customform.validation_regex.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN customform.validation_regex.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN customform.validation_regex.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN customform.value.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN customform.value.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN customform.value.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN customform.value.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN customform.value.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN customform.value.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform.check_prop.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform.check_prop.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform.check_prop.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform.check_prop.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform.check_prop.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform.check_prop.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform.component.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform.component.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform.component.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform.component.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform.component.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform.component.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform.component_type.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform.component_type.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform.component_type.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform.component_type.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform.component_type.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform.component_type.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform.ebs_form.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform.ebs_form.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform.ebs_form.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform.ebs_form.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform.ebs_form.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform.ebs_form.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform.form_type.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform.form_type.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform.form_type.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform.form_type.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform.form_type.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform.form_type.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform."group".creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform."group".creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform."group".is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform."group".modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform."group".modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform."group".tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform.helper.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform.helper.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform.helper.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform.helper.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform.helper.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform.helper.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform.input_prop.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform.input_prop.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform.input_prop.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform.input_prop.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform.input_prop.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform.input_prop.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform.option.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform.option.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform.option.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform.option.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform.option.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform.option.tenant_id
	IS 'Id of the selected Tenant'
;

COMMENT ON COLUMN ebsform.tab.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN ebsform.tab.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN ebsform.tab.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN ebsform.tab.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN ebsform.tab.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN ebsform.tab.tenant_id
	IS 'Id of the selected Tenant'
;

