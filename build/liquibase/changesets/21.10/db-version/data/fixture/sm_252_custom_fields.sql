--liquibase formatted sql

--changeset postgres:sm_252_custom_fields context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM-225 Add custom fields


INSERT INTO ebsform.component_type
(name, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Switch', 1, '2021-10-14 04:32:54.863', NULL, 556, NULL, false, 3);
INSERT INTO ebsform.component_type
(name, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('TextField', 1, '2021-09-30 05:37:40.889', NULL, 556, NULL, false, 1);
INSERT INTO ebsform.component_type
(name, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('DatePicker', 1, '2021-09-30 05:41:29.400', NULL, 556, NULL, false, 2);
INSERT INTO ebsform.component_type
(name, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Select', 1, '2021-10-14 04:43:27.311', NULL, 556, NULL, false, 4);



INSERT INTO ebsform."group"
("name", title, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, ebs_form_id)
VALUES('basicAutopupulated', 'DISABLED', 1, '2021-09-30 05:04:49.466', NULL, 556, NULL, false, 2, 1);
INSERT INTO ebsform."group"
("name", title, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, ebs_form_id)
VALUES('basicForm', 'ENABLED', 1, '2021-09-30 04:54:30.045', NULL, 556, NULL, false, 1, 1);


INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('adminContact', 'Admin Contact', '12, 12, 12, 12, 12', 'Admin Contact', NULL, NULL, 'Admin Contact is required. Please select one.', 1, '2021-09-30 05:37:57.731', NULL, 1, NULL, false, 1, 2, 2, 4, 1);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('completedBy', 'Completed By', '12, 12, 12, 12, 12', 'Completed By', NULL, NULL, 'Please enter target completion date', 2, '2021-10-14 05:09:13.161', NULL, 556, NULL, false, 5, NULL, NULL, 2, NULL);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('icc', 'ICC', '12, 12, 12, 12, 12', 'ICC', NULL, NULL, 'Please enter a value for ICC', 1, '2021-09-30 05:41:40.409', NULL, 1, NULL, false, 2, 2, 2, 1, 1);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('submitionDate', 'Submission Date', '12, 12, 12, 12, 12', 'Submission Date', NULL, NULL, NULL, 1, '2021-10-14 05:00:44.508', NULL, 556, NULL, false, 3, NULL, 2, 2, 1);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('description', 'Notes', '12, 12, 12, 12, 12', 'Notes', '', NULL, NULL, 1, '2021-10-14 05:23:39.609', NULL, 556, NULL, false, 4, NULL, NULL, 1, NULL);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('serviceProvider', 'Service Provider', '12, 12, 12, 12, 12', 'Service Provider', NULL, NULL, 'Service Provider is required. Please select one.', 1, '2021-10-14 08:41:13.361', NULL, 556, NULL, false, 8, NULL, NULL, 4, NULL);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('serviceType', 'Service Type', '12, 12, 12, 12, 12', 'Service Type', NULL, NULL, 'Service Type is required. Please select one.', 1, '2021-10-14 08:41:49.283', NULL, 556, NULL, false, 9, NULL, NULL, 4, NULL);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('purpose', 'Purpose', '12, 12, 12, 12, 12', 'Purpose', NULL, NULL, 'Purpose is required. Please select one.', 1, '2021-10-14 08:41:49.283', NULL, 556, NULL, false, 10, NULL, NULL, 4, NULL);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('Service', 'Service', '12, 12, 12, 12, 12', 'Service', NULL, NULL, 'Service is required. Please select one.', 1, '2021-10-14 08:41:49.283', NULL, 556, NULL, false, 11, NULL, NULL, 4, NULL);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('Program', 'Program', '12, 12, 12, 12, 12', 'Program', NULL, NULL, 'Program is required. Please select one.', 1, '2021-10-14 08:41:49.283', NULL, 556, NULL, false, 12, NULL, NULL, 4, NULL);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('Crop', 'Crop', '12, 12, 12, 12, 12', 'Crop', NULL, NULL, 'Crop is required. Please select one.', 1, '2021-10-14 08:41:49.283', NULL, 556, NULL, false, 13, NULL, NULL, 4, NULL);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('Tissue Type', 'Tissue Type', '12, 12, 12, 12, 12', 'Tissue Type', NULL, NULL, 'Tissue Type is required. Please select one.', 1, '2021-10-14 08:41:49.283', NULL, 556, NULL, false, 14, NULL, NULL, 4, NULL);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('ocsNumber', 'OCS Number', '12, 12, 12, 12, 12', 'OCS Number', NULL, NULL, '', 1, '2021-09-30 05:41:40.409', NULL, 1, NULL, false, 6, 2, 2, 1, 1);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('contactPerson', 'Contact Person', '12, 12, 12, 12, 12', 'Contact Person', NULL, NULL, '', 1, '2021-09-30 05:41:40.409', NULL, 1, NULL, false, 7, 2, 2, 1, 1);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('requester', 'Requester', '12, 12, 12, 12, 12', 'Requester', NULL, NULL, 'Please enter a value for Requester', 1, '2021-09-30 05:41:40.409', NULL, 1, NULL, false, 15, 2, 2, 1, 1);
INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('requesterEmail', 'Requester Email', '12, 12, 12, 12, 12', 'Requester Email', NULL, NULL, 'Please enter a value for Email', 1, '2021-09-30 05:41:40.409', NULL, 1, NULL, false, 16, 2, 2, 1, 1);



INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('styles', 'customStyles', 1, '2021-10-14 06:28:43.155', NULL, 556, NULL, false, 1, 1);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('size', 'small', 1, '2021-10-14 06:35:27.679', NULL, 556, NULL, false, 2, 2);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('color', 'primary', 1, '2021-10-14 06:35:27.683', NULL, 556, NULL, false, 3, 2);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('label', 'ICC', 1, '2021-10-14 06:35:27.685', NULL, 556, NULL, false, 4, 2);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('size', 'small', 1, '2021-10-14 06:35:27.688', NULL, 556, NULL, false, 5, 3);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('autoOk', 'true', 1, '2021-10-14 06:35:27.691', NULL, 556, NULL, false, 6, 3);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('variant', 'inline', 1, '2021-10-14 06:35:27.699', NULL, 556, NULL, false, 7, 3);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('label', 'Submission Date', 1, '2021-10-14 06:35:27.702', NULL, 556, NULL, false, 8, 3);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('rows', '4', 1, '2021-10-14 06:35:27.714', NULL, 556, NULL, false, 11, 4);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('autoOk', 'true', 1, '2021-10-14 06:35:27.716', NULL, 556, NULL, false, 12, 5);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('variant', 'inline', 1, '2021-10-14 06:35:27.719', NULL, 556, NULL, false, 13, 5);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('label', 'Completed By', 1, '2021-10-14 06:35:27.721', NULL, 556, NULL, false, 14, 5);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('size', 'small', 1, '2021-10-14 06:35:27.730', NULL, 556, NULL, false, 17, 5);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('size', 'small', 1, '2021-10-14 06:35:27.733', NULL, 556, NULL, false, 18, 4);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('variant', 'outlined', 1, '2021-10-14 06:35:27.735', NULL, 556, NULL, false, 19, 4);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('color', 'primary', 1, '2021-10-14 06:35:27.738', NULL, 556, NULL, false, 20, 4);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('placeholder', 'Notes', 1, '2021-10-14 06:35:27.740', NULL, 556, NULL, false, 21, 4);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('multiline', 'true', 1, '2021-10-14 06:35:27.742', NULL, 556, NULL, false, 22, 4);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('placeholder', 'Admin Contact', 1, '2021-10-14 07:09:26.579', NULL, 556, NULL, false, 23, 1);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('format', 'MM/dd/yyyy', 1, '2021-10-14 06:35:27.707', NULL, 556, NULL, false, 10, 2);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('format', 'MM/dd/yyyy', 1, '2021-10-14 06:35:27.724', NULL, 556, NULL, false, 15, 5);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('disabled', 'true', 1, '2021-10-14 06:35:27.705', NULL, 556, NULL, false, 9, 3);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('label', 'OCS Number', 2, '2021-10-14 08:14:01.261', NULL, 556, NULL, false, 24, 6);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('label', 'Contact Person', 2, '2021-10-14 08:14:01.265', NULL, 556, NULL, false, 25, 7);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('minDate', 'getValues(''submitiondate'') || new Date()', 1, '2021-10-14 06:35:27.727', NULL, 556, NULL, false, 16, 5);










INSERT INTO customform.form
("name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, form_type_id)
VALUES('IRRI Request Genotyping Service', 'Genotyping Services', 2, '2021-08-03 15:32:27.000', NULL, 1, NULL, false, 2, 2);


truncate table customform.field cascade ;

INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idAdminContact', NULL, 'Admin Contact', 'Admin Contact', true, 1, true, 1, '2021-08-03 15:52:36.950', NULL, 1, NULL, false, 7, 1, 4, NULL, NULL, 1, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idCompleBy', NULL, 'Complete By', 'Complete By', true, 4, true, 1, '2021-08-03 15:56:33.978', NULL, 1, NULL, false, 10, 5, 3, NULL, 1, 2, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idCompleBy', NULL, 'Complete By', 'Complete By', true, 4, false, 3, '2021-08-03 15:56:33.978', NULL, 1, NULL, false, 17, 5, 3, NULL, 1, 1, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idContactPerson', NULL, 'ContactPerson', 'ContactPerson', true, 3, false, 2, '2021-09-30 05:45:50.887', NULL, 1, NULL, false, 19, 7, 1, NULL, NULL, 2, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idCrop', NULL, 'Crop', 'Crop', true, 2, true, 1, '2021-08-03 15:45:11.500', NULL, 1, NULL, false, 2, 13, 4, NULL, NULL, 1, 1, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idICC', NULL, 'ICC', 'ICC', true, 2, false, 1, '2021-08-03 15:53:40.523', NULL, 1, NULL, false, 8, 2, 4, NULL, NULL, 1, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idNotes', NULL, 'Notes', 'Notes', false, 5, true, 1, '2021-08-03 15:57:37.332', NULL, 1, NULL, false, 20, 4, 1, NULL, 4, 1, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idNotes', NULL, 'Notes', 'Notes', false, 5, false, 3, '2021-08-03 15:57:37.332', NULL, 1, NULL, false, 11, 4, 1, NULL, 4, 1, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idOcsNumber', NULL, 'Gobii Number', 'OCS Number', true, 2, false, 2, '2021-09-30 05:45:50.887', NULL, 1, NULL, false, 16, 6, 1, NULL, NULL, 2, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idProgram', NULL, 'Program', 'Program', true, 1, true, 1, '2021-08-03 15:43:59.286', NULL, 1, NULL, false, 1, 12, 4, NULL, NULL, 1, 1, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idPurpose', NULL, 'Purpose', 'Purpose', true, 3, true, 1, '2021-08-03 16:01:12.157', NULL, 1, NULL, false, 14, 10, 4, NULL, NULL, 1, NULL, 4);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idRequester', NULL, 'Requester', 'Requester', true, 3, true, 1, '2021-08-03 15:46:20.313', NULL, 1, NULL, false, 3, 15, 4, NULL, NULL, 1, 1, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idRequesterEmail', NULL, 'Requester Email', 'Requester Email', true, 4, true, 1, '2021-08-03 15:48:15.353', NULL, 1, NULL, false, 4, 16, 4, NULL, NULL, 1, 1, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idResearcher', NULL, 'Reasearcher', 'Reasearcher', true, 1, false, 3, '2021-08-03 15:52:36.950', NULL, 1, NULL, false, 18, 1, 4, NULL, NULL, 1, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idService', NULL, 'Service', 'Service', true, 4, true, 1, '2021-08-03 16:01:51.788', NULL, 1, NULL, false, 15, 11, 4, NULL, NULL, 1, NULL, 4);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idServiceProvider', NULL, 'Service Provider', 'Service Provider', true, 1, true, 1, '2021-08-03 15:59:02.871', NULL, 1, NULL, false, 12, 8, 4, NULL, NULL, 1, NULL, 4);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idServiceType', NULL, 'Service Type', 'Service Type', true, 2, true, 1, '2021-08-03 16:00:16.058', NULL, 1, NULL, false, 13, 9, 4, NULL, NULL, 1, NULL, 4);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idSubDate', NULL, 'Submission Date', 'Submission Date', true, 3, true, 1, '2021-08-03 15:55:22.650', NULL, 1, NULL, false, 9, 3, 3, NULL, 3, 1, 2, 3);
INSERT INTO customform.field
("name", default_value, "label", tooltip, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idTissueType', NULL, 'Tissue Type', 'Tissue Type', true, 5, true, 1, '2021-08-03 15:49:25.221', NULL, 1, NULL, false, 5, 14, 4, NULL, NULL, 1, 1, 3);
