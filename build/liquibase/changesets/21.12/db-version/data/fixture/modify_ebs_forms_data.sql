--liquibase formatted sql

--changeset postgres:add_ebs_forms_data context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM-503 Update EBS_FORM data

truncate table ebsform.component_type cascade ;

INSERT INTO ebsform.component_type ("name",tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id) VALUES
	 ('Switch',1,'2021-10-14 04:32:54.863049',NULL,556,NULL,false,3),
	 ('TextField',1,'2021-09-30 05:37:40.889464',NULL,556,NULL,false,1),
	 ('DatePicker',1,'2021-09-30 05:41:29.400412',NULL,556,NULL,false,2),
	 ('Select',1,'2021-10-14 04:43:27.311544',NULL,556,NULL,false,4);

INSERT INTO ebsform.ebs_form (name,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,form_type_id) VALUES
	 ('IRRI Request Genotyping Service',2,'2021-12-09 03:20:50.448469',NULL,1,NULL,false,2,2),
	 ('CIMMYT Seed Health Analysis',1,'2021-12-09 03:31:45.528703',NULL,1,NULL,false,5,1),
	 ('CIMMYT Molecular Lab',1,'2021-12-09 03:31:45.536206',NULL,1,NULL,false,4,1),
	 ('IRRI Seed Health Analysis',2,'2021-12-09 03:31:45.53321',NULL,1,NULL,false,3,1);

truncate table ebsform."group" cascade ;

INSERT INTO ebsform."group"
("name", title, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, ebs_form_id)
VALUES('basicAutopupulated', 'DISABLED', 1, '2021-09-30 05:04:49.466', NULL, 556, NULL, false, 2, 1);
INSERT INTO ebsform."group"
("name", title, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, ebs_form_id)
VALUES('basicForm', 'ENABLED', 1, '2021-09-30 04:54:30.045', NULL, 556, NULL, false, 1, 1);

truncate table ebsform.component cascade ;

INSERT INTO ebsform.component ("name",title,"size","label",default_value,on_change,"rule",tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,group_id,tab_id,component_type_id,ebs_form_id) VALUES
	 ('completedBy','Completed By','12, 12, 12, 12, 12','Completed By',NULL,NULL,'Please enter target completion date',2,'2021-10-14 05:09:13.161126',NULL,556,NULL,false,5,NULL,NULL,2,1),
	 ('purpose','Purpose','12, 12, 12, 12, 12','Purpose',NULL,NULL,'Purpose is required. Please select one.',1,'2021-10-14 08:41:49.283418',NULL,556,NULL,false,10,NULL,NULL,4,3),
	 ('contactPerson','Contact Person','12, 12, 12, 12, 12','Contact Person',NULL,NULL,'Contact Person is required. Please select one.',1,'2021-09-30 05:41:40.409979',NULL,1,NULL,false,7,2,NULL,1,2),
	 ('Crop','Crop','12, 12, 12, 12, 12','Crop',NULL,NULL,'Crop is required. Please select one.',1,'2021-10-14 08:41:49.283418',NULL,556,NULL,false,13,NULL,NULL,4,5),
	 ('Tissue Type','Tissue Type','12, 12, 12, 12, 12','Tissue Type',NULL,NULL,'Tissue Type is required. Please select one.',1,'2021-10-14 08:41:49.283418',NULL,556,NULL,false,14,NULL,NULL,4,5),
	 ('requester','Requester','12, 12, 12, 12, 12','Requester',NULL,NULL,'Please enter a value for Requester',1,'2021-09-30 05:41:40.409979',NULL,1,NULL,false,15,2,NULL,1,5),
	 ('serviceProvider','Service Provider','12, 12, 12, 12, 12','Service Provider',NULL,NULL,'Service Provider is required. Please select one.',1,'2021-10-14 08:41:13.361002',NULL,556,NULL,false,8,NULL,NULL,4,3),
	 ('Service','Service','12, 12, 12, 12, 12','Service',NULL,NULL,'Service is required. Please select one.',1,'2021-10-14 08:41:49.283418',NULL,556,NULL,false,11,NULL,NULL,4,3),
	 ('requesterEmail','Requester Email','12, 12, 12, 12, 12','Requester Email',NULL,NULL,'Please enter a value for Email',1,'2021-09-30 05:41:40.409979',NULL,1,NULL,false,16,2,NULL,1,5),
	 ('Program','Program','12, 12, 12, 12, 12','Program',NULL,NULL,'Program is required. Please select one.',1,'2021-10-14 08:41:49.283418',NULL,556,NULL,false,12,NULL,NULL,4,3);
INSERT INTO ebsform.component ("name",title,"size","label",default_value,on_change,"rule",tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,group_id,tab_id,component_type_id,ebs_form_id) VALUES
	 ('submitionDate','Submition Date','12, 12, 12, 12, 12','Submition Date',NULL,NULL,'Please enter submition date',1,'2021-10-14 05:00:44.508459',NULL,556,NULL,false,3,1,NULL,2,1),
	 ('serviceType','Service Type','12, 12, 12, 12, 12','Service Type',NULL,NULL,'Service Type is required. Please select one.',1,'2021-10-14 08:41:49.283418',NULL,556,NULL,false,9,NULL,NULL,4,3),
	 ('ocsNumber','OCS Number','12, 12, 12, 12, 12','OCS Number',NULL,NULL,'',2,'2021-09-30 05:41:40.409979',NULL,1,NULL,false,6,2,NULL,1,2),
	 ('adminContact','Admin Contact','12, 12, 12, 12, 12','Admin Contact',NULL,NULL,'Admin Contact is required. Please select one.',1,'2021-09-30 05:37:57.731209',NULL,1,NULL,false,1,2,NULL,4,1),
	 ('completedBy','Completed By','12, 12, 12, 12, 12','Completed By',NULL,NULL,'Please enter target completion date',2,'2021-12-09 03:39:04.919384',NULL,556,NULL,false,21,1,NULL,2,2),
	 ('icc','ICC','12, 12, 12, 12, 12','ICC',NULL,NULL,'Please enter a value for ICC',1,'2021-09-30 05:41:40.409979',NULL,1,NULL,false,2,2,NULL,1,1),
	 ('serviceprovider','Service Provider','12, 12, 12, 12, 12','Service Provider',NULL,NULL,'Service Provider is required. Please select one.',1,'2021-11-23 05:17:09.467965',NULL,1,NULL,false,17,NULL,NULL,4,3),
	 ('serviceType','Service Type','12, 12, 12, 12, 12','Service Type',NULL,NULL,'Service Type is required. Please select one.',1,'2021-11-23 05:33:21.792821',NULL,1,NULL,false,18,NULL,NULL,4,3),
	 ('purpose','Purpose','12, 12, 12, 12, 12','Purpose',NULL,NULL,'Purpose is required. Please select one.',1,'2021-11-23 06:12:25.559585',NULL,1,NULL,false,19,NULL,NULL,4,3),
	 ('service','Service','12, 12, 12, 12, 12','Service',NULL,NULL,'Service is required. Please select one.',1,'2021-11-23 06:12:25.566293',NULL,1,NULL,false,20,NULL,NULL,4,3);
INSERT INTO ebsform.component ("name",title,"size","label",default_value,on_change,"rule",tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,group_id,tab_id,component_type_id,ebs_form_id) VALUES
	 ('adminContact','Researcher','12, 12, 12, 12, 12','Researcher',NULL,NULL,'Researcher is required. Please select one.',2,'2021-12-09 03:48:00.618589',NULL,1,NULL,false,22,NULL,NULL,4,2),
	 ('description','Notes','12, 12, 12, 12, 12','Notes','',NULL,NULL,1,'2021-10-14 05:23:39.609106',NULL,556,NULL,false,4,NULL,NULL,1,1);

truncate table ebsform.check_prop cascade ;

INSERT INTO ebsform.check_prop ("key",value,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,component_id) VALUES
	 ('styles','customStyles',1,'2021-10-14 06:28:43.155668',NULL,556,NULL,false,1,1),
	 ('size','small',1,'2021-10-14 06:35:27.679634',NULL,556,NULL,false,2,2),
	 ('color','primary',1,'2021-10-14 06:35:27.683724',NULL,556,NULL,false,3,2),
	 ('size','small',1,'2021-10-14 06:35:27.688964',NULL,556,NULL,false,5,3),
	 ('autoOk','true',1,'2021-10-14 06:35:27.691853',NULL,556,NULL,false,6,3),
	 ('variant','inline',1,'2021-10-14 06:35:27.699416',NULL,556,NULL,false,7,3),
	 ('label','Submission Date',1,'2021-10-14 06:35:27.702287',NULL,556,NULL,false,8,3),
	 ('rows','4',1,'2021-10-14 06:35:27.714389',NULL,556,NULL,false,11,4),
	 ('autoOk','true',1,'2021-10-14 06:35:27.716944',NULL,556,NULL,false,12,5),
	 ('variant','inline',1,'2021-10-14 06:35:27.719374',NULL,556,NULL,false,13,5);
INSERT INTO ebsform.check_prop ("key",value,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,component_id) VALUES
	 ('label','Completed By',1,'2021-10-14 06:35:27.721678',NULL,556,NULL,false,14,5),
	 ('size','small',1,'2021-10-14 06:35:27.730518',NULL,556,NULL,false,17,5),
	 ('size','small',1,'2021-10-14 06:35:27.733221',NULL,556,NULL,false,18,4),
	 ('variant','outlined',1,'2021-10-14 06:35:27.735733',NULL,556,NULL,false,19,4),
	 ('color','primary',1,'2021-10-14 06:35:27.738017',NULL,556,NULL,false,20,4),
	 ('placeholder','Notes',1,'2021-10-14 06:35:27.74016',NULL,556,NULL,false,21,4),
	 ('multiline','true',1,'2021-10-14 06:35:27.742793',NULL,556,NULL,false,22,4),
	 ('format','MM/dd/yyyy',1,'2021-10-14 06:35:27.707423',NULL,556,NULL,false,10,2),
	 ('format','MM/dd/yyyy',1,'2021-10-14 06:35:27.724077',NULL,556,NULL,false,15,5),
	 ('disabled','true',1,'2021-10-14 06:35:27.70501',NULL,556,NULL,false,9,3);
INSERT INTO ebsform.check_prop ("key",value,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,component_id) VALUES
	 ('label','OCS Number',2,'2021-10-14 08:14:01.261806',NULL,556,NULL,false,24,6),
	 ('label','Contact Person',2,'2021-10-14 08:14:01.265776',NULL,556,NULL,false,25,7),
	 ('minDate','getValues(''submitiondate'') || new Date()',1,'2021-10-14 06:35:27.727535',NULL,556,NULL,false,16,5),
	 ('label','ICC',1,'2021-10-14 06:35:27.685955',NULL,556,NULL,false,4,2),
	 ('placeholder','Service Provider',1,'2021-11-23 05:19:25.501111',NULL,556,NULL,false,26,17),
	 ('placeholder','Service',1,'2021-11-23 06:13:18.484345',NULL,556,NULL,false,29,20),
	 ('placeholder','Purpose',1,'2021-11-23 06:13:18.477986',NULL,556,NULL,false,28,19),
	 ('placeholder','Service Type',1,'2021-11-23 05:36:25.223205',NULL,556,NULL,false,27,18),
	 ('placeholder','Researcher',1,'2021-12-09 04:21:51.935279',NULL,556,NULL,false,30,22),
	 ('styles','customStyles',1,'2021-12-09 04:24:33.325918',NULL,556,NULL,false,31,22);
INSERT INTO ebsform.check_prop ("key",value,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,component_id) VALUES
	 ('variant','inline',1,'2021-12-09 04:26:32.453086',NULL,556,NULL,false,32,21),
	 ('label','Completed By',1,'2021-12-09 04:26:32.457384',NULL,556,NULL,false,33,21),
	 ('format','MM/dd/yyyy',1,'2021-12-09 04:26:32.46089',NULL,556,NULL,false,34,21),
	 ('size','small',1,'2021-12-09 04:26:32.466119',NULL,556,NULL,false,36,21),
	 ('placeholder','Admin Contact',1,'2021-10-14 07:09:26.579831',NULL,556,NULL,false,23,1);


truncate table ebsform.helper cascade;

INSERT INTO ebsform.helper (title,placement,arrow,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,component_id) VALUES
	 ('The name of the admin person to contact about the request.','right',true,1,'2021-10-14 04:49:10.882541',NULL,556,NULL,false,1,1),
	 ('Account to charge the cost of the service.','right',true,1,'2021-10-14 04:58:20.360079',NULL,556,NULL,false,2,2),
	 ('Target date when the request should be completed','right',true,1,'2021-10-14 05:16:06.97972',NULL,556,NULL,false,3,5),
	 ('OCS Number','right',true,1,'2021-10-14 05:34:54.340085',NULL,556,NULL,false,4,6),
	 ('Internal service unit that triage, and/or process clients'' service requests.','right',true,1,'2021-11-23 06:19:17.157924',NULL,556,NULL,false,5,17),
	 ('Type of services, such as Genotyping (G), Phytosanitary (P), seed quality (G), pathology including MLN screening','right',true,1,'2021-11-23 06:19:17.16752',NULL,556,NULL,false,6,18),
	 ('High level buiness needs that the service to address. For genotyping, values include MAS, QC, GS/GWAS, germplasm characterization','right',true,1,'2021-11-23 06:19:17.171784',NULL,556,NULL,false,7,19),
	 ('Service or product name, corresponding to a specific vendor catalog product or service identifier','right',true,1,'2021-11-23 06:19:17.175892',NULL,556,NULL,false,8,20);

UPDATE customform.form SET "name"= 'CIMMYT Create Request Right Basic Tab', description = 'Create Request Basic Tab Enabled Fields', tenant_id  = 1, form_type_id= 2 where id =1;
UPDATE customform.form SET "name"= 'IRRI Create Request Right Basic Tab', description = 'Create Request Basic Tab Enabled Fields', tenant_id  = 2, form_type_id= 2 where id =2; 

INSERT INTO customform.form ("name",description,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,form_type_id) VALUES
	 ('Create Request Service Provider Tab','Create Request Service Tab',1,'2021-12-09 04:03:24.93087',NULL,1,NULL,false,4,2),
	 ('Create Request Left Basic Tab','Create Request Basic Tab Autopopulated Fields',1,'2021-12-09 04:03:24.936039',NULL,1,NULL,false,5,2);



truncate table customform.field cascade ;

INSERT INTO customform.field ("name","label",tooltip,default_value,is_required,"order",is_base,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,component_id,data_type_id,fields_scale_value_id,validation_regex_id,form_id,group_id,tab_id) VALUES
	 ('idProgram','Program','Program',NULL,true,1,true,1,'2021-08-03 15:43:59.286256',NULL,1,NULL,false,1,12,4,NULL,NULL,5,1,NULL),
	 ('idCrop','Crop','Crop',NULL,true,2,true,1,'2021-08-03 15:45:11.500342',NULL,1,NULL,false,2,13,4,NULL,NULL,5,1,NULL),
	 ('idRequester','Requester','Requester',NULL,true,3,true,1,'2021-08-03 15:46:20.313201',NULL,1,NULL,false,3,15,4,NULL,NULL,5,1,NULL),
	 ('idRequesterEmail','Requester Email','Requester Email',NULL,true,4,true,1,'2021-08-03 15:48:15.353439',NULL,1,NULL,false,4,16,4,NULL,NULL,5,1,NULL),
	 ('idTissueType','Tissue Type','Tissue Type',NULL,true,5,true,1,'2021-08-03 15:49:25.22128',NULL,1,NULL,false,5,14,4,NULL,NULL,5,1,NULL),
	 ('idAdminContact','Admin Contact','Admin Contact',NULL,true,1,true,1,'2021-08-03 15:52:36.950963',NULL,1,NULL,false,7,1,4,NULL,NULL,1,2,NULL),
	 ('idICC','ICCs','ICC',NULL,true,2,false,1,'2021-08-03 15:53:40.523821',NULL,1,NULL,false,8,2,4,NULL,NULL,1,2,NULL),
	 ('idSubDate','CIMMYT Submission Date','Submission Date',NULL,true,3,false,1,'2021-08-03 15:55:22.650115',NULL,1,NULL,false,9,3,3,NULL,3,1,2,NULL),
	 ('idCompleBy','Completed By','Completed By',NULL,true,4,false,1,'2021-08-03 15:56:33.978854',NULL,1,NULL,false,10,5,3,NULL,1,2,2,NULL),
	 ('idOcsNumber','OCS Number','OCS Number',NULL,true,2,false,2,'2021-09-30 05:45:50.887383',NULL,1,NULL,false,16,6,1,NULL,NULL,2,2,NULL);
INSERT INTO customform.field ("name","label",tooltip,default_value,is_required,"order",is_base,tenant_id,creation_timestamp,modification_timestamp,creator_id,modifier_id,is_void,id,component_id,data_type_id,fields_scale_value_id,validation_regex_id,form_id,group_id,tab_id) VALUES
	 ('idContactPerson','Contact Person','ContactPerson',NULL,true,3,false,2,'2021-09-30 05:45:50.887383',NULL,1,NULL,false,19,7,1,NULL,NULL,2,2,NULL),
	 ('idNotes','Notes','Notes',NULL,false,5,false,1,'2021-08-03 15:57:37.332117',NULL,1,NULL,false,20,4,1,NULL,4,1,2,NULL),
	 ('idServiceProvider','Service Provicer','Service Provider',NULL,true,1,true,1,'2021-11-23 05:18:14.698132',NULL,1,NULL,false,22,17,4,NULL,NULL,4,2,NULL),
	 ('idServiceType','Service Type','Service Type',NULL,true,2,true,1,'2021-11-23 05:33:47.970727',NULL,1,NULL,false,23,18,4,NULL,NULL,4,2,NULL),
	 ('idPurpose','Purpose','Purpose',NULL,true,3,true,1,'2021-11-23 06:13:32.113726',NULL,1,NULL,false,24,19,4,NULL,NULL,4,2,NULL),
	 ('idService','Service','Service',NULL,true,4,true,1,'2021-11-23 06:14:24.239849',NULL,1,NULL,false,25,20,4,NULL,NULL,4,2,NULL),
	 ('idCompletedBy','Completed By','Completed By',NULL,true,4,false,2,'2021-12-09 03:18:27.596675',NULL,1,NULL,false,26,21,3,NULL,3,1,2,NULL),
	 ('idAdminContact','Researcher','Researcher',NULL,true,1,true,2,'2021-12-09 03:48:21.371993',NULL,1,NULL,false,27,22,NULL,NULL,NULL,2,2,NULL);
