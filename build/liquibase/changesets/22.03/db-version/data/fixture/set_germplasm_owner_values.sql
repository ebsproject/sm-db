--liquibase formatted sql

--changeset postgres:set_germplasm_owner_values context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: DB-1069 Set values to germplasm_owner column


UPDATE sample.request
SET germplasm_owner_id = 1;

--Revert Chnages
--rollback UPDATE sample.request SET germplasm_owner_id = NULL;