--liquibase formatted sql

--changeset postgres:set_germplasm_owner_values context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM-670, 672 Set values to germplasm_owner column


--comment: ebsform.component rules
UPDATE ebsform.component
SET "name"='adminContact', title='Team Work Contact', "size"='12, 12, 12, 12, 12', "label"='Team Work Contact', default_value=NULL, on_change=NULL, "rule"='Team Work Contact is required. Please select one.', tenant_id=1, creation_timestamp='2021-09-30 05:37:57.731', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, group_id=2, tab_id=NULL, component_type_id=4, ebs_form_id=1
WHERE id=1;

UPDATE ebsform.component
SET "name"='idICC', title='Account', "size"='12, 12, 12, 12, 12', "label"='ICC', default_value=NULL, on_change=NULL, "rule"='Please enter a value for Account', tenant_id=1, creation_timestamp='2021-09-30 05:41:40.409', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, group_id=2, tab_id=NULL, component_type_id=1, ebs_form_id=1
WHERE id=2;

UPDATE ebsform.component
SET "name"='estimatedBy', title='Estimated By', "size"='12, 12, 12, 12, 12', "label"='Estimated by', default_value=NULL, on_change=NULL, "rule"='Please enter estimated completion date', tenant_id=2, creation_timestamp='2021-10-14 05:09:13.161', modification_timestamp=NULL, creator_id=556, modifier_id=NULL, is_void=false, group_id=NULL, tab_id=NULL, component_type_id=2, ebs_form_id=1
WHERE id=5;

UPDATE ebsform.component
SET "name"='estimatedBy', title='Estimated By', "size"='12, 12, 12, 12, 12', "label"='Estimated by', default_value=NULL, on_change=NULL, "rule"='Please enter estimated completion date', tenant_id=2, creation_timestamp='2021-12-09 03:39:04.919', modification_timestamp=NULL, creator_id=556, modifier_id=NULL, is_void=false, group_id=1, tab_id=NULL, component_type_id=2, ebs_form_id=2
WHERE id=21;



--comment: ebsForm inputProp(checkProp) labels

UPDATE ebsform.check_prop
SET "key"='label', value='Account', tenant_id=1, creation_timestamp='2021-10-14 06:35:27.685', modification_timestamp=NULL, creator_id=556, modifier_id=NULL, is_void=false, component_id=2
WHERE id=4;

UPDATE ebsform.check_prop
SET "key"='label', value='Estimated By', tenant_id=1, creation_timestamp='2021-10-14 06:35:27.721', modification_timestamp=NULL, creator_id=556, modifier_id=NULL, is_void=false, component_id=5
WHERE id=14;

UPDATE ebsform.check_prop
SET "key"='placeholder', value='Team Work Contact', tenant_id=1, creation_timestamp='2021-10-14 07:09:26.579', modification_timestamp=NULL, creator_id=556, modifier_id=NULL, is_void=false, component_id=1
WHERE id=23;

UPDATE ebsform.check_prop
SET "key"='label', value='Estimated By', tenant_id=1, creation_timestamp='2021-12-09 04:26:32.457', modification_timestamp=NULL, creator_id=556, modifier_id=NULL, is_void=false, component_id=21
WHERE id=33;


--change tabe sequence index

UPDATE ebsform.tab
SET "index"=4, "name"='REVIEW', tenant_id=1, creation_timestamp='2021-08-03 15:29:54.908', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, ebs_form_id=1
WHERE id=1;
UPDATE ebsform.tab
SET "index"=1, "name"='ENTRY LIST', tenant_id=1, creation_timestamp='2021-08-03 15:28:15.477', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, ebs_form_id=1
WHERE id=2;
UPDATE ebsform.tab
SET "index"=2, "name"='BASIC', tenant_id=1, creation_timestamp='2021-08-03 15:28:42.454', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, ebs_form_id=1
WHERE id=3;
UPDATE ebsform.tab
SET "index"=3, "name"='SERVICES', tenant_id=1, creation_timestamp='2021-08-03 15:29:08.797', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, ebs_form_id=1
WHERE id=4;

--comment: clear request table data

delete from sample.request_list_member;
delete from sample.request_status;
delete from customform."value";
delete from sample.request;
