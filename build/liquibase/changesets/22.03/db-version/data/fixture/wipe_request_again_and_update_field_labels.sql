--liquibase formatted sql

--changeset postgres:clear_request_table_data context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM-670, 672 Update values to estimated by


--comment: customform.field rules
UPDATE customform.field
SET name='idEstimatedBy', "label"='Estimated By', tooltip='Estimated By', default_value=NULL, is_required=true, "order"=4, is_base=false, tenant_id=2, creation_timestamp='2021-12-09 03:18:27.596', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=21, data_type_id=3, fields_scale_value_id=NULL, validation_regex_id=3, form_id=2, group_id=2, tab_id=NULL
WHERE id=26;
UPDATE customform.field
SET name='idEstimatedBy', "label"='Estimated By', tooltip='Estimated By', default_value=NULL, is_required=true, "order"=4, is_base=false, tenant_id=1, creation_timestamp='2021-08-03 15:56:33.978', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=5, data_type_id=3, fields_scale_value_id=NULL, validation_regex_id=1, form_id=1, group_id=2, tab_id=NULL
WHERE id=10;
UPDATE customform.field
SET "name"='idSubDate', "label"='Submission Date', tooltip='Submission Date', default_value=NULL, is_required=true, "order"=3, is_base=false, tenant_id=1, creation_timestamp='2021-08-03 15:55:22.650', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=3, data_type_id=3, fields_scale_value_id=NULL, validation_regex_id=3, form_id=2, group_id=2, tab_id=NULL
WHERE id=9;


--comment: clear request table data

delete from sample.request_list_member;
delete from sample.request_status;
delete from customform."value";
delete from sample.request;
