--liquibase formatted sql

--changeset postgres:add_new_column_in_request context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1069 Add new column in SM Request table



ALTER TABLE sample.request ADD COLUMN germplasm_owner_id integer NULL;


--Revert Chnages
--rollback ALTER TABLE sample.request DROP COLUMN germplasm_owner_id;