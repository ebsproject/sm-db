--liquibase formatted sql

--changeset postgres:add_program_id_in_service_provider context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-842 Add new column program_id in service_provider table


ALTER TABLE services.service_provider ADD COLUMN program_id integer NULL;


--Revert Changes
--rollback ALTER TABLE services.service_provider DROP COLUMN program_id;