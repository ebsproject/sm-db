--liquibase formatted sql

--changeset postgres:remove_program_entities_from_smdb context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-842 Remove program tables from smdb


--Batch
ALTER TABLE sample.batch DROP CONSTRAINT "FK_batch_crop";

--Request
ALTER TABLE sample.request DROP CONSTRAINT "FK_request_crop";
ALTER TABLE sample.request DROP CONSTRAINT "FK_request_program";

--Sample
ALTER TABLE sample.sample DROP CONSTRAINT "FK_sample_crop";
ALTER TABLE sample.sample DROP CONSTRAINT "FK_sample_season";

--Sample_detail
ALTER TABLE sample.sample_detail DROP CONSTRAINT "FK_sample_detail_crop";
ALTER TABLE sample.sample_detail DROP CONSTRAINT "FK_sample_detail_season";

--Service_provider
ALTER TABLE services.service_provider DROP CONSTRAINT "FK_service_provider_crop";

-- "program".crop_program foreign keys
ALTER TABLE "program".crop_program DROP CONSTRAINT "FK_crop_program_crop";
ALTER TABLE "program".crop_program DROP CONSTRAINT "FK_crop_program_program";

DROP TABLE "program".crop;
DROP TABLE "program"."program";
DROP TABLE "program".season;
DROP TABLE "program".crop_program;

DROP SEQUENCE "program".crop_id_seq;
DROP SEQUENCE "program".program_id_seq;
DROP SEQUENCE "program".season_id_seq;


--Schema Program
DROP SCHEMA "program";



--Revert Changes
--rollback CREATE SCHEMA "program" AUTHORIZATION postgres;

--rollback CREATE TABLE "program".crop (
--rollback 	code varchar(50) NULL,
--rollback 	"name" varchar(50) NULL,
--rollback 	description varchar(50) NULL,
--rollback 	notes varchar(50) NULL,
--rollback 	creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback 	modification_timestamp timestamp NULL,
--rollback 	creator_id int4 NOT NULL,
--rollback 	modifier_id int4 NULL,
--rollback 	is_void bool NOT NULL DEFAULT false,
--rollback 	id serial NOT NULL,
--rollback 	CONSTRAINT "PK_crop" PRIMARY KEY (id)
--rollback );

--rollback CREATE TABLE "program"."program" (
--rollback 	code varchar(50) NULL,
--rollback 	"name" varchar(50) NULL,
--rollback 	"type" varchar(50) NULL,
--rollback 	status varchar(50) NULL,
--rollback 	description varchar(50) NULL,
--rollback 	notes varchar(50) NULL,
--rollback 	creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback 	modification_timestamp timestamp NULL,
--rollback 	creator_id int4 NOT NULL,
--rollback 	modifier_id int4 NULL,
--rollback 	is_void bool NOT NULL DEFAULT false,
--rollback 	id serial NOT NULL,
--rollback 	CONSTRAINT "PK_program" PRIMARY KEY (id)
--rollback );

--rollback CREATE TABLE "program".season (
--rollback 	code varchar(50) NULL,
--rollback 	"name" varchar(50) NULL,
--rollback 	description varchar(50) NULL,
--rollback 	notes varchar(50) NULL,
--rollback 	tenant_id int4 NOT NULL,
--rollback 	creation_timestamp timestamp NOT NULL DEFAULT now(),
--rollback 	modification_timestamp timestamp NULL,
--rollback 	creator_id int4 NOT NULL,
--rollback 	modifier_id int4 NULL,
--rollback 	is_void bool NOT NULL DEFAULT false,
--rollback 	id serial NOT NULL,
--rollback 	CONSTRAINT "PK_season" PRIMARY KEY (id)
--rollback );

--rollback CREATE TABLE "program".crop_program (
--rollback 	program_id int4 NOT NULL,
--rollback 	crop_id int4 NOT NULL,
--rollback 	CONSTRAINT "PK_crop_program" PRIMARY KEY (program_id, crop_id)
--rollback );

--rollback ALTER TABLE "program".crop_program ADD CONSTRAINT "FK_crop_program_crop" FOREIGN KEY (crop_id) REFERENCES program.crop(id);
--rollback ALTER TABLE "program".crop_program ADD CONSTRAINT "FK_crop_program_program" FOREIGN KEY (program_id) REFERENCES program.program(id);

--rollback ALTER TABLE sample.request ADD CONSTRAINT "FK_request_crop" FOREIGN KEY (crop_id) REFERENCES program.crop(id);
--rollback ALTER TABLE sample.request ADD CONSTRAINT "FK_request_program" FOREIGN KEY (program_id) REFERENCES program.program(id);

--rollback ALTER TABLE sample.sample ADD CONSTRAINT "FK_sample_crop" FOREIGN KEY (crop_id) REFERENCES program.crop(id);
--rollback ALTER TABLE sample.sample ADD CONSTRAINT "FK_sample_season" FOREIGN KEY (season_id) REFERENCES program.season(id);

--rollback ALTER TABLE sample.sample_detail ADD CONSTRAINT "FK_sample_detail_crop" FOREIGN KEY (crop_id) REFERENCES program.crop(id);
--rollback ALTER TABLE sample.sample_detail ADD CONSTRAINT "FK_sample_detail_season" FOREIGN KEY (season_id) REFERENCES program.season(id);

--rollback ALTER TABLE services.service_provider ADD CONSTRAINT "FK_service_provider_crop" FOREIGN KEY (crop_id) REFERENCES program.crop(id);
--rollback ALTER TABLE sample.batch ADD CONSTRAINT "FK_batch_crop" FOREIGN KEY (crop_id) REFERENCES program.crop(id);