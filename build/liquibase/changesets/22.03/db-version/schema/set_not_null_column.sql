--liquibase formatted sql

--changeset postgres:set_not_null_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1069 Add new column in SM Request table


ALTER TABLE sample.request
 ALTER COLUMN germplasm_owner_id SET NOT NULL;


 --Revert Changes
 --rollback ALTER TABLE sample.request ALTER COLUMN germplasm_owner_id DROP NOT NULL;
