--liquibase formatted sql

--changeset postgres:add_team_contact_email_and_update context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: disable estimated date,

--comment: SM-697 disable estimated date
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('disabled', 'true', 1, '2022-04-18 01:15:13.673', NULL, 556, NULL, false, 37, 21);

INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('format', 'MM/dd/yyyy', 1, '2022-04-18 02:14:14.866', NULL, 556, NULL, false, 38, 3);

INSERT INTO ebsform.helper
(title, placement, arrow, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('Estimated date of when the request should be completed (15 business days)', 'right', true, 1, '2022-04-18 01:55:59.348', NULL, 556, NULL, false, 9, 21);

INSERT INTO ebsform.helper
(title, placement, arrow, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('The date today', 'right', true, 1, '2022-04-18 02:00:22.486', NULL, 556, NULL, false, 10, 3);


--comment: SM-698 Add a new row for the team work contact email

INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES('adminContactEmail', 'Team Work Contact Email', '12, 12, 12, 12, 12', 'Team Work Contact Email', NULL, NULL, NULL, 1, '2022-04-18 02:51:32.305', NULL, 556, NULL, false, 23, NULL, NULL, 1, 1);

INSERT INTO ebsform.helper
(title, placement, arrow, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('Email of the selected Team Work Contact', 'right', true, 1, '2022-04-18 02:54:46.627', NULL, 556, NULL, false, 11, 23);

INSERT INTO customform.field
("name", "label", tooltip, default_value, is_required, "order", is_base, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES('idTeamWorkContactEmail', 'Team Work Contact Email', 'Team Work Contact Email', NULL, true, 2, false, 2, '2022-04-18 02:58:45.213', NULL, 1, NULL, false, 28, 23, 1, NULL, NULL, 1, 2, NULL);

INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('size', 'small', 1, '2022-04-18 03:04:36.698', NULL, 556, NULL, false, 39, 23);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('color', 'primary', 1, '2022-04-18 03:04:36.703', NULL, 556, NULL, false, 40, 23);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('label', 'Team Work Contact Email', 1, '2022-04-18 03:04:36.705', NULL, 556, NULL, false, 41, 23);
INSERT INTO ebsform.check_prop
("key", value, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, component_id)
VALUES('disabled', 'true', 1, '2022-04-18 03:06:49.888', NULL, 556, NULL, false, 42, 23);


--comment: SM-698 Update form's order

UPDATE customform.field
SET "name"='idAdminContact', "label"='Admin Contact', tooltip='Admin Contact', default_value=NULL, is_required=true, "order"=1, is_base=true, tenant_id=1, creation_timestamp='2021-08-03 15:52:36.950', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=1, data_type_id=4, fields_scale_value_id=NULL, validation_regex_id=NULL, form_id=1, group_id=2, tab_id=NULL
WHERE id=7;
UPDATE customform.field
SET "name"='idICC', "label"='ICCs', tooltip='ICC', default_value=NULL, is_required=true, "order"=3, is_base=false, tenant_id=1, creation_timestamp='2021-08-03 15:53:40.523', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=2, data_type_id=4, fields_scale_value_id=NULL, validation_regex_id=NULL, form_id=1, group_id=2, tab_id=NULL
WHERE id=8;
UPDATE customform.field
SET "name"='idSubDate', "label"='CIMMYT Submission Date', tooltip='Submission Date', default_value=NULL, is_required=true, "order"=4, is_base=false, tenant_id=1, creation_timestamp='2021-08-03 15:55:22.650', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=3, data_type_id=3, fields_scale_value_id=NULL, validation_regex_id=3, form_id=1, group_id=2, tab_id=NULL
WHERE id=9;
UPDATE customform.field
SET "name"='idNotes', "label"='Notes', tooltip='Notes', default_value=NULL, is_required=false, "order"=6, is_base=false, tenant_id=1, creation_timestamp='2021-08-03 15:57:37.332', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=4, data_type_id=1, fields_scale_value_id=NULL, validation_regex_id=4, form_id=1, group_id=2, tab_id=NULL
WHERE id=20;

