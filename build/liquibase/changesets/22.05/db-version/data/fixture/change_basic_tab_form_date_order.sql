--liquibase formatted sql

--changeset postgres:change_basic_tab_form_order context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM-778 Change Basic Tab Form Order


UPDATE customform.field
SET "name"='idEstimatedBy', "order"=5, "label"='Estimated By', tooltip='Estimated By', default_value=NULL, is_required=true, is_base=false, tenant_id=1, creation_timestamp='2021-08-03 15:56:33.978', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=5, data_type_id=3, fields_scale_value_id=NULL, validation_regex_id=1, form_id=1, group_id=2, tab_id=NULL
WHERE id=10;

UPDATE customform.field
SET "name"='idSubDate', "order"=4, "label"='Submission Date', tooltip='Submission Date', default_value=NULL, is_required=true, is_base=false, tenant_id=1, creation_timestamp='2021-08-03 15:55:22.650', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=3, data_type_id=3, fields_scale_value_id=NULL, validation_regex_id=3, form_id=1, group_id=2, tab_id=NULL
WHERE id=9;
