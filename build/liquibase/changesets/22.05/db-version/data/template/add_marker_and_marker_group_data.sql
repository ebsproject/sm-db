--liquibase formatted sql

--changeset postgres:add_marker_and_marker_group_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1218 Add data to marker and marker group table



WITH marker_group as (
    INSERT INTO services.marker_group
        ("name", assay_class_id, tenant_id, creator_id, is_void)
    VALUES
        ('50 SNP_Panel_v2', 1, 1, 1, false)
    RETURNING id
)

INSERT INTO services.marker
    ("name", description, tenant_id, creator_id, is_void, marker_group_id)
VALUES
    ('snpZM00079', 'snpZM00079', 1, 1, false, (select id from marker_group)),
    ('snpZM00080', 'snpZM00080', 1, 1, false, (select id from marker_group))
;

INSERT INTO services.technology_platform_assay_class
(technology_platform_id, assay_class_id)
VALUES(1, 1);

