--liquibase formatted sql

--changeset postgres:add_attributes_batch_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1217 Add attributes in batch table and drop not null constraint




ALTER TABLE services.marker ALTER COLUMN assay_gene_id DROP NOT NULL;

ALTER TABLE sample.batch 
 ADD COLUMN num_empty_well integer NOT NULL   DEFAULT 0;

ALTER TABLE sample.batch 
 ADD COLUMN num_plates integer NOT NULL   DEFAULT 0;

ALTER TABLE sample.batch 
 ADD COLUMN num_samples integer NOT NULL   DEFAULT 0;

ALTER TABLE sample.request_list_member 
 ADD COLUMN total_entities integer NOT NULL   DEFAULT 1;

ALTER TABLE services.technology_platform_assay_class
 DROP COLUMN IF EXISTS assayclass_id;
