--liquibase formatted sql

--changeset postgres:sm_assay_changes context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1195 Apply sm changes



ALTER TABLE services.assay_class 
  DROP  CONSTRAINT "FK_assay_class_service";

DROP TABLE IF EXISTS services.vendor_marker;

ALTER TABLE services.assay_class 
 DROP COLUMN IF EXISTS service_id;

ALTER TABLE sample.batch 
 ADD COLUMN assay_class_id integer NULL;

ALTER TABLE sample.batch 
 ADD COLUMN marker_group_id integer NULL;

ALTER TABLE sample.batch 
 ADD COLUMN technology_platform_id integer NULL;

ALTER TABLE sample.batch 
 ADD COLUMN vendor_id integer NULL;

ALTER TABLE services.marker 
 ADD COLUMN marker_group_id integer NULL;

CREATE TABLE services.marker_group
(
	id integer NOT NULL   DEFAULT NEXTVAL(('services."marker_group_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	name varchar(150) NULL,
	assay_class_id integer NULL,
	tenant_id integer NOT NULL,	-- Id reference to the Tenant table. Indicates the selected Tenant in the system.
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

CREATE SEQUENCE services.marker_group_id_seq INCREMENT 1 START 1
;

ALTER TABLE services.marker_group ADD CONSTRAINT "PK_marker_group"
	PRIMARY KEY (id)
;

ALTER TABLE sample.batch ADD CONSTRAINT "FK_batch_assay_class"
	FOREIGN KEY (assay_class_id) REFERENCES services.assay_class (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE sample.batch ADD CONSTRAINT "FK_batch_marker_group"
	FOREIGN KEY (marker_group_id) REFERENCES services.marker_group (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE sample.batch ADD CONSTRAINT "FK_batch_technology_platform"
	FOREIGN KEY (technology_platform_id) REFERENCES services.technology_platform (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE sample.batch ADD CONSTRAINT "FK_batch_vendor"
	FOREIGN KEY (vendor_id) REFERENCES services.vendor (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE services.marker ADD CONSTRAINT "FK_marker_marker_group"
	FOREIGN KEY (marker_group_id) REFERENCES services.marker_group (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE services.marker_group ADD CONSTRAINT "FK_marker_group_assay_class"
	FOREIGN KEY (assay_class_id) REFERENCES services.assay_class (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN sample.batch.assay_class_id
	IS 'Id reference to the Assay Class table'
;

COMMENT ON COLUMN sample.batch.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN sample.batch.marker_group_id
	IS 'Id reference to the marker_group tale'
;

COMMENT ON COLUMN sample.batch.technology_platform_id
	IS 'Id reference to the technology_platform'
;

COMMENT ON COLUMN sample.batch.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN sample.batch.vendor_id
	IS 'Id reference to the vendor'
;

COMMENT ON COLUMN services.assay_class.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN services.assay_class.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN services.marker.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN services.marker.marker_group_id
	IS 'Id reference to the marker_group'
;

COMMENT ON COLUMN services.marker.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;

COMMENT ON COLUMN services.marker_group.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN services.marker_group.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN services.marker_group.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN services.marker_group.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN services.marker_group.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN services.marker_group.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN services.marker_group.tenant_id
	IS 'Id reference to the Tenant table. Indicates the selected Tenant in the system.'
;