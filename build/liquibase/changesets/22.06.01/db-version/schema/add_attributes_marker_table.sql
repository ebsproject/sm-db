--liquibase formatted sql

--changeset postgres:add_attributes_marker_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1217 Add attributes in marker table


ALTER TABLE services.marker 
 ADD COLUMN source_platform_type Varchar(50)  NOT NULL   DEFAULT '';

ALTER TABLE services.marker 
 ADD COLUMN chromosome Varchar(50)  NOT NULL   DEFAULT '';

ALTER TABLE services.marker 
 ADD COLUMN sequence Varchar(750)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN sequence_startosition_cs_vs1 Varchar(50)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN sequence_endPosition_cd_vs1 Varchar(50)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN similar_sequence_chr_96 Varchar(50)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN hybrid_evaluation Varchar(50)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN fam_allele Varchar(50)  NOT NULL   DEFAULT ''; 
 
ALTER TABLE services.marker 
 ADD COLUMN hex_allele Varchar(50)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN primer_gc_allele_x Varchar(50)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN primer_gc_Allele_y Varchar(50)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN primer_gc_Common Varchar(50)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN blast_score Varchar(50)  NOT NULL   DEFAULT ''; 

ALTER TABLE services.marker 
 ADD COLUMN blast_expect Varchar(50)  NOT NULL   DEFAULT ''; 


