--liquibase formatted sql

--changeset postgres:update_tooltip_and_remove_estimated_by_in_basic_tab context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM-838 Create Request Basic Tab Bugfix

--comment: update team contact email tooltip
UPDATE ebsform.helper
SET title='The name of the Breeder, or any other Breeding team member to contact about the request.', placement='right', arrow=true, tenant_id=1, creation_timestamp='2021-10-14 04:49:10.882', modification_timestamp=NULL, creator_id=556, modifier_id=NULL, is_void=false, component_id=1
WHERE id=1;


--comment: update estimated by name and move out of basic tab (changed tenant id)

UPDATE customform.field
SET "name"='idEstimatedBy', "label"='Data Estimated By', tooltip='Data Estimated By', default_value=NULL, is_required=true, "order"=4, is_base=false, tenant_id=3, creation_timestamp='2021-08-03 15:56:33.978', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=5, data_type_id=3, fields_scale_value_id=NULL, validation_regex_id=1, form_id=2, group_id=2, tab_id=NULL
WHERE id=10;

UPDATE ebsform.component
SET "name"='estimatedBy', title='Data Estimated By', "size"='12, 12, 12, 12, 12', "label"='Data Estimated by', default_value=NULL, on_change=NULL, "rule"='Please enter estimated completion date', tenant_id=2, creation_timestamp='2021-10-14 05:09:13.161', modification_timestamp=NULL, creator_id=556, modifier_id=NULL, is_void=false, group_id=NULL, tab_id=NULL, component_type_id=2, ebs_form_id=1
WHERE id=5;

UPDATE ebsform.check_prop
SET "key"='label', value='Data Estimated By', tenant_id=1, creation_timestamp='2021-10-14 06:35:27.721', modification_timestamp=NULL, creator_id=556, modifier_id=NULL, is_void=false, component_id=5
WHERE id=14;
