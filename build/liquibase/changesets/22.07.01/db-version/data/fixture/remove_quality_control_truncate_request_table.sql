--liquibase formatted sql

--changeset postgres:remove_quality_control_truncate_request_table context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM2-130 Truncate request and reindex table then delete quality control record with id 8 from services schema.

TRUNCATE sample.request RESTART IDENTITY CASCADE;

SELECT SETVAL('sample.request_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM sample.request;

DELETE FROM services.service where purpose_id = 8;
DELETE FROM services.purpose WHERE id = 8 and name = 'Quality Control';


