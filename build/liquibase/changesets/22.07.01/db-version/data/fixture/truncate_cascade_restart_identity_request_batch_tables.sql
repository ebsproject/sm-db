--liquibase formatted sql

--changeset postgres:truncate_cascade_restart_identity_request_batch_tables context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: DB-1318 Truncate request and batch tables then restart index

TRUNCATE sample.batch RESTART IDENTITY CASCADE;
TRUNCATE sample.request RESTART IDENTITY CASCADE;

SELECT SETVAL('sample.batch_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM sample.batch;
SELECT SETVAL('sample.request_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM sample.request;
