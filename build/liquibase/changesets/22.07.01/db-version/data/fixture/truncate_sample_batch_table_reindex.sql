--liquibase formatted sql

--changeset postgres:truncate_sample_batch_table_reindex context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM2-132 Clean the batch table  then restart index

TRUNCATE sample.batch RESTART IDENTITY CASCADE;

SELECT SETVAL('sample.batch_id_seq', COALESCE(MAX(id) + 1, 1), FALSE) FROM sample.batch;