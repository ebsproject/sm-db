    --liquibase formatted sql

    --changeset postgres:update_batch_code context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
    --comment: SM-131 Update the batch code table columns last_batch_number, last_plate_number last_sample_number to zero and service_provider_id to 1

    UPDATE sample.batch_code SET last_batch_number = 0;
    UPDATE sample.batch_code SET last_plate_number = 0;
    UPDATE sample.batch_code SET last_sample_number = 0;
    UPDATE sample.batch_code SET service_provider_id = 1;
