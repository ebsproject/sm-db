--liquibase formatted sql

--changeset postgres:update_services_purpose_isvoid_true context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: DB-1300 Hide the Quality Control: F1 Verfication, Ped Ver, Purity, Line Finishing_Fixation in the selection

UPDATE services.purpose SET is_void = FALSE WHERE id = 4;

--rollback UPDATE services.purpose SET is_void = TRUE WHERE id = 4;
