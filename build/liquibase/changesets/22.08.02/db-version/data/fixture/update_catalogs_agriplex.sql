--liquibase formatted sql

--changeset postgres:update_catalogs_agriplex context:fixture labels:develop splitStatements:false rollbackSplitStatements:false
--comment: SM2-212 Update Catalog to add Agriplex

-- add agriplex service
INSERT INTO services.service(
	name, description, code, tenant_id,  creator_id, is_void, purpose_id )
	VALUES ('Agriplex', 'Agriplex', 'Agriplex', 1, 1, false, 4);

-- add agriplex in vendor

INSERT INTO services.vendor(
	code, reference, status, tenant_id, creator_id,  is_void, data_format_id)
	VALUES ('Agriplex', 'Agriplex', 'Active', 1, 1, false, 1);

-- add agriplex to technology platform
do $$

declare max_vendor_id int := (SELECT MAX(id) from services.vendor);
Begin
INSERT INTO services.technology_platform(
	name, description, tenant_id,  creator_id, is_void, vendor_id)
	VALUES ('mid density', 'mid density', 1, 1, false,  max_vendor_id);
	
end $$;

-- add agriplex assay_class
do $$

-- declare max_service_id int := (SELECT MAX(id) from services.service);
Begin
INSERT INTO services.assay_class(
	name, description, tenant_id, creator_id,  is_void)
	VALUES ('Fixed', 'Fixed', 1, 1, false);
    	
end $$;

-- add technology platform tech id to vendor control

do $$

declare max_technology_platform_id int := (SELECT MAX(id) from services.technology_platform);
Begin
INSERT INTO services.vendor_control(
	"position", tenant_id, creator_id, is_void, technology_platform_id)
	VALUES ('G12, H12', 1, 1, false, max_technology_platform_id);
    	
end $$;
