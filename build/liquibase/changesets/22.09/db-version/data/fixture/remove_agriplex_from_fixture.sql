--liquibase formatted sql

--changeset postgres:remove_agriplex_from_fixture context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1410 Update Template version of SMDB


do $$
declare _service_id integer;
declare _vendor_id integer;
declare _tech integer;

begin
SELECT id FROM services.service WHERE name = 'Agriplex' INTO _service_id;
SELECT id FROM services.vendor WHERE code ='Agriplex' INTO _vendor_id;
SELECT id FROM services.technology_platform WHERE name = 'mid density' and vendor_id = _vendor_id INTO _tech;

DELETE FROM services.vendor_control WHERE "position" = 'G12, H12' and  technology_platform_id = _tech;
DELETE FROM services.technology_platform WHERE id = _tech;

DELETE FROM services.service WHERE name = 'Agriplex';
DELETE FROM services.vendor WHERE code ='Agriplex';

end $$;