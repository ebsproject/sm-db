--liquibase formatted sql

--changeset postgres:remove_fixture_data_assay context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1410 Update Template version of SMDB


INSERT INTO services.marker_group
(id,"name", assay_class_id, tenant_id, creator_id)
VALUES(3, '55 SNP1', 1, 1, 1);

UPDATE services.marker
SET marker_group_id = 3
WHERE marker_group_id = 2;

DELETE FROM services.marker_group WHERE "name" = '55 SNP';

UPDATE services.marker_group
SET assay_class_id=1
WHERE assay_class_id = 3;

DELETE FROM services.technology_platform_assay_class
WHERE technology_platform_id = 1 and assay_class_id = 2;

DELETE FROM services.technology_platform_assay_class
WHERE technology_platform_id = 4 and assay_class_id = 3;

DELETE FROM services.technology_platform_assay_class
WHERE technology_platform_id = 4 and assay_class_id = 4;

DELETE FROM services.technology_platform_assay_class
WHERE technology_platform_id = 5 and assay_class_id = 5;


DELETE FROM services.assay_class WHERE "name" = 'User Defined';
DELETE FROM services.assay_class WHERE "name" = 'Not-Target';
DELETE FROM services.assay_class WHERE "name" = 'Fixed' and id = 3;


SELECT setval('services.marker_group_id_seq', (SELECT MAX(id) FROM services.marker_group)); 