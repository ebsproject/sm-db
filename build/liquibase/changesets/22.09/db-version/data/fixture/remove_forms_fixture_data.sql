--liquibase formatted sql

--changeset postgres:remove_forms_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1410 Update Template version of SMDB



TRUNCATE table sample.mixture_method CASCADE;
TRUNCATE table sample.load_type CASCADE;
TRUNCATE table sample.collection_layout cascade;
TRUNCATE table sample.collection_container_type cascade;
TRUNCATE table ebsform.helper;
TRUNCATE table ebsform.check_prop;
TRUNCATE table customform.field cascade;
TRUNCATE table customform.validation_regex cascade;
TRUNCATE table customform.form cascade;
TRUNCATE table ebsform.component cascade;
TRUNCATE table ebsform.component_type CASCADE;
TRUNCATE table ebsform.tab CASCADE;
TRUNCATE table ebsform.group CASCADE;
TRUNCATE table ebsform.ebs_form CASCADE;
TRUNCATE table ebsform.form_type CASCADE;
TRUNCATE table customform.data_type CASCADE;
