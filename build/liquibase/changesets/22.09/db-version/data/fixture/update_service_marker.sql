--liquibase formatted sql

--changeset postgres:update_service_marker context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1410 Update Template version of SMDB


UPDATE services.marker
SET marker_group_id = 2
WHERE marker_group_id = 3;

DELETE FROM services.marker_group
WHERE id  = 3;