--liquibase formatted sql

--changeset postgres:update_catalogs_agriplex context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1410 Update Template version of SMDB



-- add agriplex in vendor
WITH vendor as (
INSERT INTO services.vendor(
	code, reference, status, tenant_id, creator_id,  is_void, data_format_id)
	VALUES 
    ('Agriplex', 'Agriplex', 'Active', 1, 1, false, 1)
RETURNING id
), tech as (
INSERT INTO services.technology_platform(
	name, description, tenant_id,  creator_id, is_void, vendor_id)
	VALUES ('mid density', 'mid density', 1, 1, false,  (select id from vendor))
RETURNING id	
)
-- add technology platform tech id to vendor contro
INSERT INTO services.vendor_control(
	"position", tenant_id, creator_id, is_void, technology_platform_id)
	VALUES ('G12, H12', 1, 1, false, (select id from tech));


do $$
declare tech_id integer; 

begin

SELECT id FROM services.technology_platform WHERE name = 'mid density' and vendor_id = (select  id from services.vendor where code = 'Agriplex') INTO tech_id;
-- add agriplex assay_class
WITH assay as (
INSERT INTO services.assay_class(
	name, description, tenant_id, creator_id,  is_void)
	VALUES 
    ('Fixed', 'Fixed', 1, 1, false)
RETURNING id
)   	

INSERT INTO services.technology_platform_assay_class
(technology_platform_id, assay_class_id)
VALUES(tech_id, (select id from assay));

end $$