--liquibase formatted sql

--changeset postgres:add_forms_sample_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1410 Update Template version of SMDB



INSERT INTO customform.data_type
("name", description, tenant_id, creator_id, id)
VALUES
('String', 'String', 1, 1, 1),
('Integer', 'Integer', 1, 1, 2),
('Date', 'Date', 1, 1, 3),
('API', 'API', 1, 1, 4);

INSERT INTO ebsform.form_type
("name", tenant_id, creator_id, id)
VALUES
('Tab', 1, 1, 1),
('Group', 1, 1, 2),
('Component', 1, 1, 3);

INSERT INTO ebsform.ebs_form
("name", tenant_id, creator_id, id, form_type_id)
VALUES
('CIMMYT Request Genotyping Service', 1, 1, 1, 2),
('IRRI Request Genotyping Service', 2, 1, 2, 2),
('CIMMYT Seed Health Analysis', 1, 1, 5, 1),
('CIMMYT Molecular Lab', 1, 1, 4, 1),
('IRRI Seed Health Analysis', 2, 1, 3, 1);

INSERT INTO ebsform."group"
("name", title, tenant_id, creator_id, id, ebs_form_id)
VALUES
('basicAutopupulated', 'DISABLED', 1, 556, 2, 1),
('basicForm', 'ENABLED', 1, 556, 1, 1);

INSERT INTO ebsform.tab
("index", "name", tenant_id, creator_id, id, ebs_form_id)
VALUES
(4, 'REVIEW', 1, 1, 1, 1),
(1, 'ENTRY LIST', 1, 1, 2, 1),
(2, 'BASIC', 1, 1, 3, 1),
(3, 'SERVICES', 1, 1, 4, 1);

INSERT INTO ebsform.component_type
("name", tenant_id, creator_id, id)
VALUES
('Switch', 1, 556, 3),
('TextField', 1, 556, 1),
('DatePicker', 1, 556, 2),
('Select', 1, 556, 4);


INSERT INTO ebsform.component
("name", title, "size", "label", default_value, on_change, "rule", tenant_id, creator_id, id, group_id, tab_id, component_type_id, ebs_form_id)
VALUES
('purpose', 'Purpose', '12, 12, 12, 12, 12', 'Purpose', NULL, NULL, 'Purpose is required. Please select one.', 1, 556, 10, NULL, NULL, 4, 3),
('contactPerson', 'Contact Person', '12, 12, 12, 12, 12', 'Contact Person', NULL, NULL, 'Contact Person is required. Please select one.', 1, 1, 7, 2, NULL, 1, 2),
('Crop', 'Crop', '12, 12, 12, 12, 12', 'Crop', NULL, NULL, 'Crop is required. Please select one.', 1, 556, 13, NULL, NULL, 4, 5),
('TissueType', 'Tissue Type', '12, 12, 12, 12, 12', 'Tissue Type', NULL, NULL, 'Tissue Type is required. Please select one.', 1, 556, 14, NULL, NULL, 4, 5),
('requester', 'Requester', '12, 12, 12, 12, 12', 'Requester', NULL, NULL, 'Please enter a value for Requester', 1, 1, 15, 2, NULL, 1, 5),
('serviceProvider', 'Service Provider', '12, 12, 12, 12, 12', 'Service Provider', NULL, NULL, 'Service Provider is required. Please select one.', 1, 556, 8, NULL, NULL, 4, 3),
('Service', 'Service', '12, 12, 12, 12, 12', 'Service', NULL, NULL, 'Service is required. Please select one.', 1, 556, 11, NULL, NULL, 4, 3),
('requesterEmail', 'Requester Email', '12, 12, 12, 12, 12', 'Requester Email', NULL, NULL, 'Please enter a value for Email', 1, 1, 16, 2, NULL, 1, 5),
('Program', 'Program', '12, 12, 12, 12, 12', 'Program', NULL, NULL, 'Program is required. Please select one.', 1, 556, 12, NULL, NULL, 4, 3),
('submitionDate', 'Submition Date', '12, 12, 12, 12, 12', 'Submition Date', NULL, NULL, 'Please enter submition date', 1, 556, 3, 1, NULL, 2, 1),
('serviceType', 'Service Type', '12, 12, 12, 12, 12', 'Service Type', NULL, NULL, 'Service Type is required. Please select one.', 1, 556, 9, NULL, NULL, 4, 3),
('ocsNumber', 'OCS Number', '12, 12, 12, 12, 12', 'OCS Number', NULL, NULL, '', 2, 1, 6, 2, NULL, 1, 2),
('serviceprovider', 'Service Provider', '12, 12, 12, 12, 12', 'Service Provider', NULL, NULL, 'Service Provider is required. Please select one.', 1, 1, 17, NULL, NULL, 4, 3),
('serviceType', 'Service Type', '12, 12, 12, 12, 12', 'Service Type', NULL, NULL, 'Service Type is required. Please select one.', 1, 1, 18, NULL, NULL, 4, 3),
('purpose', 'Purpose', '12, 12, 12, 12, 12', 'Purpose', NULL, NULL, 'Purpose is required. Please select one.', 1, 1, 19, NULL, NULL, 4, 3),
('service', 'Service', '12, 12, 12, 12, 12', 'Service', NULL, NULL, 'Service is required. Please select one.', 1, 1, 20, NULL, NULL, 4, 3),
('adminContact', 'Researcher', '12, 12, 12, 12, 12', 'Researcher', NULL, NULL, 'Researcher is required. Please select one.', 2, 1, 22, NULL, NULL, 4, 2),
('description', 'Notes', '12, 12, 12, 12, 12', 'Notes', '', NULL, NULL, 1, 556, 4, NULL, NULL, 1, 1),
('adminContact', 'Team Work Contact', '12, 12, 12, 12, 12', 'Team Work Contact', NULL, NULL, 'Team Work Contact is required. Please select one.', 1, 1, 1, 2, NULL, 4, 1),
('idICC', 'Account', '12, 12, 12, 12, 12', 'ICC', NULL, NULL, 'Please enter a value for Account', 1, 1, 2, 2, NULL, 1, 1),
('estimatedBy', 'Estimated By', '12, 12, 12, 12, 12', 'Estimated by', NULL, NULL, 'Please enter estimated completion date', 2, 556, 21, 1, NULL, 2, 2),
('adminContactEmail', 'Team Work Contact Email', '12, 12, 12, 12, 12', 'Team Work Contact Email', NULL, NULL, NULL, 1, 556, 23, NULL, NULL, 1, 1),
('estimatedBy', 'Data Estimated By', '12, 12, 12, 12, 12', 'Data Estimated by', NULL, NULL, 'Please enter estimated completion date', 2, 556, 5, NULL, NULL, 2, 1);

INSERT INTO customform.form
("name", description, tenant_id, creator_id, id, form_type_id)
VALUES
('CIMMYT Create Request Right Basic Tab', 'Create Request Basic Tab Enabled Fields', 1, 1, 1, 2),
('IRRI Create Request Right Basic Tab', 'Create Request Basic Tab Enabled Fields', 2, 1, 2, 2),
('Create Request Service Provider Tab', 'Create Request Service Tab', 1, 1, 4, 2),
('Create Request Left Basic Tab', 'Create Request Basic Tab Autopopulated Fields', 1, 1, 5, 2);


INSERT INTO customform.validation_regex
(regex, "name", tenant_id, creator_id, id)
VALUES
('^[a-zA-Z ]*$', 'Person Name', 1, 1, 1),
('.+@cgiar.org.+@cimmyt.org.+@cornell.org.+@irri.org', 'Email', 1, 1, 2),
('MM/DD/YYYY', 'Date', 1, 1, 3),
('[\\d]+[A-Za-z0-9\\s,\\.]+?[\\d\\-]+', 'Address', 1, 1, 4);


INSERT INTO customform.field
("name", "label", tooltip, default_value, is_required, "order", is_base, tenant_id, creator_id, id, component_id, data_type_id, fields_scale_value_id, validation_regex_id, form_id, group_id, tab_id)
VALUES
('idProgram', 'Program', 'Program', NULL, true, 1, true, 1, 1, 1, 12, 4, NULL, NULL, 5, 1, NULL),
('idCrop', 'Crop', 'Crop', NULL, true, 2, true, 1, 1, 2, 13, 4, NULL, NULL, 5, 1, NULL),
('idRequester', 'Requester', 'Requester', NULL, true, 3, true, 1, 1, 3, 15, 4, NULL, NULL, 5, 1, NULL),
('idRequesterEmail', 'Requester Email', 'Requester Email', NULL, true, 4, true, 1, 1, 4, 16, 4, NULL, NULL, 5, 1, NULL),
('idTissueType', 'Tissue Type', 'Tissue Type', NULL, true, 5, true, 1, 1, 5, 14, 4, NULL, NULL, 5, 1, NULL),
('idOcsNumber', 'OCS Number', 'OCS Number', NULL, true, 2, false, 2, 1, 16, 6, 1, NULL, NULL, 2, 2, NULL),
('idContactPerson', 'Contact Person', 'ContactPerson', NULL, true, 3, false, 2, 1, 19, 7, 1, NULL, NULL, 2, 2, NULL),
('idServiceProvider', 'Service Provicer', 'Service Provider', NULL, true, 1, true, 1, 1, 22, 17, 4, NULL, NULL, 4, 2, NULL),
('idServiceType', 'Service Type', 'Service Type', NULL, true, 2, true, 1, 1, 23, 18, 4, NULL, NULL, 4, 2, NULL),
('idPurpose', 'Purpose', 'Purpose', NULL, true, 3, true, 1, 1, 24, 19, 4, NULL, NULL, 4, 2, NULL),
('idService', 'Service', 'Service', NULL, true, 4, true, 1, 1, 25, 20, 4, NULL, NULL, 4, 2, NULL),
('idAdminContact', 'Researcher', 'Researcher', NULL, true, 1, true, 2, 1, 27, 22, NULL, NULL, NULL, 2, 2, NULL),
('idEstimatedBy', 'Estimated By', 'Estimated By', NULL, true, 4, false, 2, 1,26, 21, 3, NULL, 3, 2, 2, NULL),
('idTeamWorkContactEmail', 'Team Work Contact Email', 'Team Work Contact Email', NULL, true, 2, false, 2, 1, 28, 23, 1, NULL, NULL, 1, 2, NULL),
('idAdminContact', 'Admin Contact', 'Admin Contact', NULL, true, 1, true, 1, 1, 7, 1, 4, NULL, NULL, 1, 2, NULL),
('idICC', 'ICCs', 'ICC', NULL, true, 3, false, 1, 1, 8, 2, 4, NULL, NULL, 1, 2, NULL),
('idNotes', 'Notes', 'Notes', NULL, false, 6, false, 1, 1, 20, 4, 1, NULL, 4, 1, 2, NULL),
('idSubDate', 'Submission Date', 'Submission Date', NULL, true, 4, false, 1, 1, 9, 3, 3, NULL, 3, 1, 2, NULL),
('idEstimatedBy', 'Data Estimated By', 'Data Estimated By', NULL, true, 4, false, 3, 1, 10, 5, 3, NULL, 1, 2, 2, NULL);


INSERT INTO ebsform.check_prop
("key", value, tenant_id, creator_id, id, component_id)
VALUES
('styles', 'customStyles', 1, 556, 1, 1),
('size', 'small', 1, 556, 2, 2),
('color', 'primary', 1, 556, 3, 2),
('size', 'small', 1, 556, 5, 3),
('autoOk', 'true', 1, 556, 6, 3),
('variant', 'inline', 1, 556, 7, 3),
('label', 'Submission Date', 1, 556, 8, 3),
('rows', '4', 1, 556, 11, 4),
('autoOk', 'true', 1, 556, 12, 5),
('variant', 'inline', 1, 556, 13, 5),
('size', 'small', 1, 556, 17, 5),
('size', 'small', 1, 556, 18, 4),
('variant', 'outlined', 1, 556, 19, 4),
('color', 'primary', 1, 556, 20, 4),
('placeholder', 'Notes', 1, 556, 21, 4),
('multiline', 'true', 1, 556, 22, 4),
('format', 'MM/dd/yyyy', 1, 556, 10, 2),
('format', 'MM/dd/yyyy', 1, 556, 15, 5),
('disabled', 'true', 1, 556, 9, 3),
('label', 'OCS Number', 2, 556, 24, 6),
('label', 'Contact Person', 2, 556, 25, 7),
('minDate', 'getValues(''submitiondate'') || new Date()', 1, 556, 16, 5),
('placeholder', 'Service Provider', 1, 556, 26, 17),
('placeholder', 'Service', 1, 556, 29, 20),
('placeholder', 'Purpose', 1, 556, 28, 19),
('placeholder', 'Service Type', 1, 556, 27, 18),
('placeholder', 'Researcher', 1, 556, 30, 22),
('styles', 'customStyles', 1, 556, 31, 22),
('variant', 'inline', 1, 556, 32, 21),
('format', 'MM/dd/yyyy', 1, 556, 34, 21),
('size', 'small', 1, 556, 36, 21),
('label', 'Account', 1, 556, 4, 2),
('placeholder', 'Team Work Contact', 1, 556, 23, 1),
('label', 'Estimated By', 1, 556, 33, 21),
('disabled', 'true', 1, 556, 37, 21),
('format', 'MM/dd/yyyy', 1, 556, 38, 3),
('size', 'small', 1, 556, 39, 23),
('color', 'primary', 1, 556, 40, 23),
('label', 'Team Work Contact Email', 1, 556, 41, 23),
('disabled', 'true', 1, 556, 42, 23),
('label', 'Data Estimated By', 1, 556, 14, 5);


INSERT INTO ebsform.helper
(title, placement, arrow, tenant_id, creator_id, id, component_id)
VALUES
('Account to charge the cost of the service.', 'right', true, 1, 556, 2, 2),
('Target date when the request should be completed', 'right', true, 1, 556, 3, 5),
('OCS Number', 'right', true, 1, 556, 4, 6),
('Internal service unit that triage, and/or process clients'' service requests.', 'right', true, 1, 556, 5, 17),
('Type of services, such as Genotyping (G), Phytosanitary (P), seed quality (G), pathology including MLN screening', 'right', true, 1, 556, 6, 18),
('High level buiness needs that the service to address. For genotyping, values include MAS, QC, GS/GWAS, germplasm characterization', 'right', true, 1, 556, 7, 19),
('Service or product name, corresponding to a specific vendor catalog product or service identifier', 'right', true, 1, 556, 8, 20),
('Estimated date of when the request should be completed (15 business days)', 'right', true, 1, 556, 9, 21),
('The date today', 'right', true, 1, 556, 10, 3),
('Email of the selected Team Work Contact', 'right', true, 1, 556, 11, 23),
('The name of the Breeder, or any other Breeding team member to contact about the request.', 'right', true, 1, 556, 1, 1);


INSERT INTO sample.collection_container_type
("name", description, code, tenant_id, creator_id, id)
VALUES('plate 96', 'plate', 'P', 1, 1, 1);

INSERT INTO sample.load_type
("name", description, tenant_id, creator_id, id)
VALUES
('Columns', 'load by column', 1, 1, 1),
('Rows', 'load by row', 1, 1, 2),
('CIMMYT format', 'load by CIMMYT format', 1, 1, 3);

INSERT INTO sample.mixture_method
("name", code, tenant_id, creator_id, id)
VALUES
('Bulk', 'B', 1, 1, 1),
('Pool', 'P', 1, 1, 2);

INSERT INTO sample.collection_layout
(num_columns, num_rows, num_well, tenant_id, creator_id, id, collection_container_type_id)
VALUES(10, 50, 500, 1, 1, 1, 1);


SELECT setval('customform.data_type_id_seq', (SELECT MAX(id) FROM customform.data_type));
SELECT setval('ebsform.form_type_id_seq', (SELECT MAX(id) FROM ebsform.form_type));
SELECT setval('ebsform.ebs_form_id_seq', (SELECT MAX(id) FROM ebsform.ebs_form));
SELECT setval('ebsform.group_id_seq', (SELECT MAX(id) FROM ebsform."group"));
SELECT setval('ebsform.tab_id_seq', (SELECT MAX(id) FROM ebsform.tab));
SELECT setval('ebsform.component_type_id_seq', (SELECT MAX(id) FROM ebsform.component_type));
SELECT setval('ebsform.component_id_seq', (SELECT MAX(id) FROM ebsform.component));
SELECT setval('customform.form_id_seq', (SELECT MAX(id) FROM customform.form));
SELECT setval('customform.validation_regex_id_seq', (SELECT MAX(id) FROM customform.validation_regex));
SELECT setval('customform.field_id_seq', (SELECT MAX(id) FROM customform.field));
SELECT setval('ebsform.check_prop_id_seq', (SELECT MAX(id) FROM ebsform.check_prop));

SELECT setval('ebsform.helper_id_seq', (SELECT MAX(id) FROM ebsform.helper));
SELECT setval('sample.collection_container_type_id_seq', (SELECT MAX(id) FROM sample.collection_container_type));
SELECT setval('sample.load_type_id_seq', (SELECT MAX(id) FROM sample.load_type));
SELECT setval('sample.mixture_method_id_seq', (SELECT MAX(id) FROM sample.mixture_method));
SELECT setval('sample.collection_layout_id_seq', (SELECT MAX(id) FROM sample.collection_layout));
SELECT setval('services.service_provider_id_seq', (SELECT MAX(id) FROM services.service_provider));
