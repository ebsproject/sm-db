--liquibase formatted sql

--changeset postgres:insert_batch_codes_molecular context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1410 Update Template version of SMDB



do $$
declare temprow record;
begin
for temprow in select id from services.service_provider where name like '%Molecular%' and id <> 1
	loop
		INSERT INTO sample.batch_code
            ("name", code, last_batch_number, last_plate_number, last_sample_number, tenant_id, creator_id, service_provider_id)
        VALUES
            ('BS1004BW-DW-QC200313', 'BS1004BW-DW-QC200313', 0, 0, 0, 1, 1, temprow.id);
	end loop;
        INSERT INTO sample.batch_code
            ("name", code, last_batch_number, last_plate_number, last_sample_number, tenant_id, creator_id, service_provider_id)
        VALUES
            ('BS1004BW-DW-QC200313', 'BS1004BW-DW-QC200313', 0, 0, 0, 1, 1, 13);

end $$


