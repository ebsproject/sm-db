--liquibase formatted sql

--changeset postgres:update_catalogs_agriplex context:template splitStatements:false rollbackSplitStatements:false
--comment: SM2-206 Update Agriplex catalogs



do $$
declare _st integer;

begin
WITH _service_type as (
INSERT INTO services.service_type(
        "name", code, description, tenant_id, creator_id)
    VALUES(
        'Genotyping Analysis IRRI', 'GI', 'Analysis to produce genotypic data from leaves, seeds, or other plant parts or DNAs isolated from plant parts', 1, 1)
RETURNING id
)
SELECT id FROM _service_type INTO _st;


DELETE FROM services.service_provider
WHERE id = 13;

DELETE FROM services.service_provider_service_type
WHERE service_type_id=3 and service_provider_id=10;

DELETE FROM services.service_provider
WHERE id=10;

INSERT INTO services.service_provider
(id, code, "name", tenant_id, creator_id) VALUES
(13, 'GSL', 'IRRI Philippines Genotyping Service Lab', 1, 1);


INSERT INTO services.service_provider_service_type
(service_type_id, service_provider_id)
VALUES(_st, 13);

WITH _purpose as (
    INSERT INTO services.purpose
        ("name", code, description, tenant_id, creator_id, service_type_id)
    VALUES(
        'Marker Assisted Selection - QLT/Major Gene', 'SQ', 'MAS (Marker Assisted selection)- QTL/Major gene', 1, 1, _st)
RETURNING id
)
    INSERT INTO services.service
        ("name", description, code, tenant_id,  creator_id, purpose_id)
    VALUES
        ('Rika V4', 'Rika V4', 'Rika V4', 1, 1, (SELECT id FROM _purpose));


WITH _purpose as (
    INSERT INTO services.purpose
        ("name", code, description, tenant_id, creator_id, service_type_id)
    VALUES(
        'Genomic Selection', 'GS', 'Genomic Selection', 1, 1, _st)
RETURNING id
)
    INSERT INTO services.service
        ("name", description, code, tenant_id,  creator_id, purpose_id)
    VALUES
        ('Rika V4', 'Rika V4', 'Rika V4', 1, 1, (SELECT id FROM _purpose));


WITH _purpose as (
    INSERT INTO services.purpose
        ("name", code, description, tenant_id, creator_id, service_type_id)
    VALUES(
        'Quality Control', 'GC', 'Quality Control', 1, 1, _st)
RETURNING id
)
    INSERT INTO services.service
        ("name", description, code, tenant_id,  creator_id, purpose_id)
    VALUES
        ('Agriplex', 'Agriplex', 'Agriplex', 1, 1, (SELECT id FROM _purpose));

end $$