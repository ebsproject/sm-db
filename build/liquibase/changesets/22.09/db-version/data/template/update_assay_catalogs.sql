--liquibase formatted sql

--changeset postgres:update_assay_catalogs context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1410 Update Template version of SMDB


DELETE FROM services.vendor_control where id = 2;
DELETE FROM services.vendor_control where id = 4;
DELETE FROM services.technology_platform where id = 2;
DELETE FROM services.technology_platform where id = 3;


DELETE FROM services.assay_class where id in (2,3,4,5,6,7,8,9,10, 11);

INSERT INTO services.assay_class
("name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('User Defined', 'User Defined', 1, now(), null, 1, null, false, 2);

INSERT INTO services.assay_class
("name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Fixed', 'Fixed', 1, now(), null, 1, null, false, 3);

INSERT INTO services.assay_class
("name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('User Defined', 'User Defined', 1, now(), null, 1, null, false, 4);

INSERT INTO services.assay_class
("name", description, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id)
VALUES('Not-Target', 'Not-Target', 1, now(), null, 1, null, false, 5);

INSERT INTO services.technology_platform_assay_class
(technology_platform_id, assay_class_id)
VALUES(1, 2);

INSERT INTO services.technology_platform_assay_class
(technology_platform_id, assay_class_id)
VALUES(4, 3);

INSERT INTO services.technology_platform_assay_class
(technology_platform_id, assay_class_id)
VALUES(4, 4);

INSERT INTO services.technology_platform_assay_class
(technology_platform_id, assay_class_id)
VALUES(5, 5);

UPDATE services.marker_group
SET assay_class_id=3
WHERE id=1;

INSERT INTO services.marker_group
(id, "name", assay_class_id, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void)
VALUES(2, '55 SNP', 1, 1, now(), null, 1, null, false);

SELECT setval('services.marker_group_id_seq', (SELECT MAX(id) FROM services.marker_group)); 
SELECT setval('services.assay_class_id_seq', (SELECT MAX(id) FROM services.assay_class)); 
