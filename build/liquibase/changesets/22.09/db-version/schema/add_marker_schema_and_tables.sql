--liquibase formatted sql

--changeset postgres:add_marker_schema_and _tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM2-286 Create SQL script for the logical model


CREATE SCHEMA marker;

DROP SEQUENCE IF EXISTS "marker".assay_id_seq;

DROP TABLE IF EXISTS "marker".assay CASCADE
;

CREATE TABLE "marker".assay
(
	assayconditions varchar NULL,
	assayname varchar NULL,
	comments varchar NULL,
	markerid varchar NULL,
	platform varchar NULL,
	strand varchar NULL,
	creation_timestamp varchar NULL,
	modification_timestamp varchar NULL,
	creator_id varchar NULL,
	modifier_id varchar NULL,
	is_void varchar NULL,
	tenant_id varchar NULL,
	id varchar NOT NULL   DEFAULT NEXTVAL(('"marker"."assay_id_seq"'::text)::regclass),
	marker_id varchar NULL
)
;

CREATE SEQUENCE "marker".assay_id_seq INCREMENT 1 START 1;

ALTER TABLE "marker".assay ADD CONSTRAINT "PK_assay"
	PRIMARY KEY (id)
;


DROP SEQUENCE IF EXISTS "marker".alleledefinition_id_seq;

DROP TABLE IF EXISTS "marker".alleledefinition CASCADE;
;

CREATE TABLE "marker".alleledefinition
(
	alleleid varchar NULL,
	call varchar NULL,
	comments varchar NULL,
	marker varchar NULL,
	markergroupid varchar NULL,
	creation_timestamp varchar NULL,
	modification_timestamp varchar NULL,
	creator_id varchar NULL,
	modifier_id varchar NULL,
	is_void varchar NULL,
	tenant_id varchar NULL,
	id varchar NOT NULL   DEFAULT NEXTVAL(('"marker"."alleledefinition_id_seq"'::text)::regclass),
	allele_id varchar NULL
)
;

CREATE SEQUENCE "marker".alleledefinition_id_seq INCREMENT 1 START 1;

ALTER TABLE "marker".alleledefinition ADD CONSTRAINT "PK_alleledefinition"
	PRIMARY KEY (id)
;


DROP SEQUENCE IF EXISTS "marker".allele_id_seq;

DROP TABLE IF EXISTS "marker".allele CASCADE
;

CREATE TABLE "marker".allele
(
	allelename varchar NULL,
	allelestatus varchar NULL,
	comments varchar NULL,
	markergroupid varchar NULL,
	creation_timestamp varchar NULL,
	modification_timestamp varchar NULL,
	creator_id varchar NULL,
	modifier_id varchar NULL,
	is_void varchar NULL,
	tenant_id varchar NULL,
	id varchar NOT NULL   DEFAULT NEXTVAL(('"marker"."allele_id_seq"'::text)::regclass),
	markergroup_id varchar NULL
)
;

CREATE SEQUENCE "marker".allele_id_seq INCREMENT 1 START 1;

ALTER TABLE "marker".allele ADD CONSTRAINT "PK_allele"
	PRIMARY KEY (id)
;


DROP SEQUENCE IF EXISTS "marker".oligo_id_seq;

DROP TABLE IF EXISTS "marker".oligo CASCADE
;

CREATE TABLE "marker".oligo
(
	assayid varchar NULL,
	comments varchar NULL,
	primername varchar NULL,
	primersequence varchar NULL,
	tm varchar NULL,
	creation_timestamp varchar NULL,
	modification_timestamp varchar NULL,
	creator_id varchar NULL,
	modifier_id varchar NULL,
	is_void varchar NULL,
	tenant_id varchar NULL,
	id varchar NOT NULL   DEFAULT NEXTVAL(('"marker"."oligo_id_seq"'::text)::regclass),
	assay_id varchar NULL
)
;

CREATE SEQUENCE "marker".oligo_id_seq INCREMENT 1 START 1;

ALTER TABLE "marker".oligo ADD CONSTRAINT "PK_oligo"
	PRIMARY KEY (id)
;



DROP SEQUENCE IF EXISTS "marker".markergroup_id_seq;

DROP TABLE IF EXISTS "marker".markergroup CASCADE
;

CREATE TABLE "marker".markergroup
(
	comments varchar NULL,
	derivedallele varchar NULL,
	markergroup varchar NULL,
	traitcategory1 varchar NULL,
	traitcategory2 varchar NULL,
	creation_timestamp varchar NULL,
	modification_timestamp varchar NULL,
	creator_id varchar NULL,
	modifier_id varchar NULL,
	is_void varchar NULL,
	tenant_id varchar NULL,
	id varchar NOT NULL   DEFAULT NEXTVAL(('"marker"."markergroup_id_seq"'::text)::regclass)
)
;

CREATE SEQUENCE "marker".markergroup_id_seq INCREMENT 1 START 1;

ALTER TABLE "marker".markergroup ADD CONSTRAINT "PK_markergroup"
	PRIMARY KEY (id)
;

DROP SEQUENCE IF EXISTS "marker".marker_id_seq;

DROP TABLE IF EXISTS "marker".marker CASCADE
;

CREATE TABLE "marker".marker
(
	allele1 varchar NULL,
	allele2 varchar NULL,
	allelesource varchar NULL,
	chromosome varchar NULL,
	comments varchar NULL,
	gene varchar NULL,
	markergroupid varchar NULL,
	markername varchar NULL,
	markertarget varchar NULL,
	position varchar NULL,
	refgenome varchar NULL,
	creation_timestamp varchar NULL,
	modification_timestamp varchar NULL,
	creator_id varchar NULL,
	modifier_id varchar NULL,
	is_void varchar NULL,
	tenant_id varchar NULL,
	id varchar NOT NULL   DEFAULT NEXTVAL(('"marker"."marker_id_seq"'::text)::regclass),
	markergroup_id varchar NULL
)
;

CREATE SEQUENCE "marker".marker_id_seq INCREMENT 1 START 1;

ALTER TABLE "marker".marker ADD CONSTRAINT "PK_marker"
	PRIMARY KEY (id)
;

ALTER TABLE "marker".marker ADD CONSTRAINT "FK_marker_markergroup"
	FOREIGN KEY (markergroup_id) REFERENCES "marker".markergroup (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "marker".assay ADD CONSTRAINT "FK_assay_marker"
	FOREIGN KEY (marker_id) REFERENCES "marker".marker (id) ON DELETE No Action ON UPDATE No Action
;


ALTER TABLE "marker".oligo ADD CONSTRAINT "FK_oligo_assay"
	FOREIGN KEY (assay_id) REFERENCES "marker".assay (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "marker".allele ADD CONSTRAINT "FK_allele_markergroup"
	FOREIGN KEY (markergroup_id) REFERENCES "marker".markergroup (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "marker".alleledefinition ADD CONSTRAINT "FK_alleledefinition_allele"
	FOREIGN KEY (allele_id) REFERENCES "marker".allele (id) ON DELETE No Action ON UPDATE No Action
;