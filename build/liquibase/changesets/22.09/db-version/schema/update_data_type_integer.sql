--liquibase formatted sql

--changeset postgres:update_data_type_integer context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM2-232 Update data type of varchar to integer

ALTER TABLE "marker".marker 
drop constraint IF EXISTS "FK_marker_markergroup";

ALTER TABLE "marker".assay 
drop constraint IF EXISTS "FK_assay_marker";

ALTER TABLE "marker".oligo
drop constraint IF EXISTS "FK_oligo_assay";

ALTER TABLE "marker".allele
drop constraint IF EXISTS "FK_allele_markergroup";

ALTER TABLE "marker".alleledefinition
drop constraint IF EXISTS "FK_alleledefinition_allele";

ALTER TABLE marker.alleledefinition
ALTER id TYPE INT USING id::integer, 
ALTER alleleid TYPE INT USING alleleid::integer,
ALTER allele_id TYPE INT USING allele_id::integer,
ALTER markergroupid TYPE INT USING markergroupid::integer,
ALTER tenant_id TYPE INT USING tenant_id::integer,
ALTER is_void TYPE BOOL USING is_void::boolean,
ALTER creator_id TYPE INT USING creator_id::integer,
ALTER modifier_id TYPE INT USING modifier_id::integer,
ALTER creation_timestamp TYPE timestamp USING creation_timestamp::timestamp without time zone,
ALTER modification_timestamp TYPE timestamp USING modification_timestamp::timestamp without time zone;
ALTER TABLE marker.alleledefinition
ALTER COLUMN call TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE marker.alleledefinition
ALTER COLUMN comments TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.alleledefinition
ALTER COLUMN marker TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE IF EXISTS marker.alleledefinition
ALTER COLUMN creation_timestamp SET DEFAULT now();
ALTER TABLE IF EXISTS marker.alleledefinition
ALTER COLUMN creation_timestamp SET NOT NULL;
ALTER TABLE IF EXISTS marker.alleledefinition
ALTER COLUMN creator_id SET NOT NULL;
ALTER TABLE IF EXISTS marker.alleledefinition
ALTER COLUMN is_void SET DEFAULT false;
ALTER TABLE IF EXISTS marker.alleledefinition
ALTER COLUMN is_void SET NOT NULL;
ALTER TABLE IF EXISTS marker.alleledefinition
ALTER COLUMN tenant_id SET NOT NULL;

ALTER TABLE marker.allele
ALTER id TYPE INT USING id::integer,
ALTER markergroupid TYPE INT USING markergroupid::integer,
ALTER markergroup_id TYPE INT USING markergroup_id::integer,
ALTER tenant_id TYPE INT USING tenant_id::integer,
ALTER is_void TYPE BOOL USING is_void::boolean,
ALTER creator_id TYPE INT USING creator_id::integer,
ALTER modifier_id TYPE INT USING modifier_id::integer,
ALTER creation_timestamp TYPE timestamp USING creation_timestamp::timestamp without time zone,
ALTER modification_timestamp TYPE timestamp USING modification_timestamp::timestamp without time zone;
ALTER TABLE marker.allele
ALTER COLUMN allelename TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE marker.allele
ALTER COLUMN allelestatus TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE marker.allele
ALTER COLUMN comments TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE IF EXISTS marker.allele
ALTER COLUMN creation_timestamp SET DEFAULT now();
ALTER TABLE IF EXISTS marker.allele
ALTER COLUMN creation_timestamp SET NOT NULL;
ALTER TABLE IF EXISTS marker.allele
ALTER COLUMN creator_id SET NOT NULL;
ALTER TABLE IF EXISTS marker.allele
ALTER COLUMN is_void SET DEFAULT false;
ALTER TABLE IF EXISTS marker.allele
ALTER COLUMN is_void SET NOT NULL;

ALTER TABLE marker.assay
ALTER id TYPE INT USING id::integer, 
ALTER markerid TYPE INT USING markerid::integer,
ALTER marker_id TYPE INT USING marker_id::integer,
ALTER tenant_id TYPE INT USING tenant_id::integer,
ALTER is_void TYPE BOOL USING is_void::boolean,
ALTER creator_id TYPE INT USING creator_id::integer,
ALTER modifier_id TYPE INT USING modifier_id::integer,
ALTER creation_timestamp TYPE timestamp USING creation_timestamp::timestamp without time zone,
ALTER modification_timestamp TYPE timestamp USING modification_timestamp::timestamp without time zone;
ALTER TABLE marker.assay
ALTER COLUMN assayconditions TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.assay
ALTER COLUMN assayname TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE marker.assay
ALTER COLUMN comments TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.assay
ALTER COLUMN platform TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.assay
ALTER COLUMN strand TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE IF EXISTS marker.assay
ALTER COLUMN creation_timestamp SET DEFAULT now();
ALTER TABLE IF EXISTS marker.assay
ALTER COLUMN creation_timestamp SET NOT NULL;
ALTER TABLE IF EXISTS marker.assay
ALTER COLUMN creator_id SET NOT NULL;
ALTER TABLE IF EXISTS marker.assay
ALTER COLUMN is_void SET DEFAULT false;
ALTER TABLE IF EXISTS marker.assay
ALTER COLUMN is_void SET NOT NULL;
ALTER TABLE IF EXISTS marker.assay
ALTER COLUMN tenant_id SET NOT NULL;

ALTER TABLE marker.markergroup
ALTER id TYPE INT USING id::integer,
ALTER tenant_id TYPE INT USING tenant_id::integer,
ALTER is_void TYPE BOOL USING is_void::boolean,
ALTER creator_id TYPE INT USING creator_id::integer,
ALTER modifier_id TYPE INT USING modifier_id::integer,
ALTER creation_timestamp TYPE timestamp USING creation_timestamp::timestamp without time zone,
ALTER modification_timestamp TYPE timestamp USING modification_timestamp::timestamp without time zone;
ALTER TABLE marker.markergroup
ALTER COLUMN comments TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.markergroup
ALTER COLUMN derivedallele TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE marker.markergroup
ALTER COLUMN markergroup TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.markergroup
ALTER COLUMN traitcategory1 TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.markergroup
ALTER COLUMN traitcategory2 TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE IF EXISTS marker.markergroup
ALTER COLUMN creation_timestamp SET DEFAULT now();
ALTER TABLE IF EXISTS marker.markergroup
ALTER COLUMN creation_timestamp SET NOT NULL;
ALTER TABLE IF EXISTS marker.markergroup
ALTER COLUMN creator_id SET NOT NULL;
ALTER TABLE IF EXISTS marker.markergroup
ALTER COLUMN is_void SET DEFAULT false;
ALTER TABLE IF EXISTS marker.markergroup
ALTER COLUMN is_void SET NOT NULL;
ALTER TABLE IF EXISTS marker.markergroup
ALTER COLUMN tenant_id SET NOT NULL;

ALTER TABLE marker.marker
ALTER id TYPE INT USING id::integer,
ALTER markergroupid TYPE INT USING markergroupid::integer,
ALTER markergroup_id TYPE INT USING markergroup_id::integer,
ALTER position TYPE INT USING position::integer,
ALTER tenant_id TYPE INT USING tenant_id::integer,
ALTER is_void TYPE BOOL USING is_void::boolean,
ALTER creator_id TYPE INT USING creator_id::integer,
ALTER modifier_id TYPE INT USING modifier_id::integer,
ALTER creation_timestamp TYPE timestamp USING creation_timestamp::timestamp without time zone,
ALTER modification_timestamp TYPE timestamp USING modification_timestamp::timestamp without time zone;
ALTER TABLE marker.marker
ALTER COLUMN allele1 TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE marker.marker
ALTER COLUMN allele2 TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE marker.marker
ALTER COLUMN allelesource TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE marker.marker
ALTER COLUMN chromosome TYPE character varying(50) COLLATE pg_catalog."default";
ALTER TABLE marker.marker
ALTER COLUMN comments TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.marker
ALTER COLUMN gene TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.marker
ALTER COLUMN markername TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.marker
ALTER COLUMN markertarget TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.marker
ALTER COLUMN refgenome TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE IF EXISTS marker.marker
ALTER COLUMN creation_timestamp SET DEFAULT now();
ALTER TABLE IF EXISTS marker.marker
ALTER COLUMN creation_timestamp SET NOT NULL;
ALTER TABLE IF EXISTS marker.marker
ALTER COLUMN creator_id SET NOT NULL;
ALTER TABLE IF EXISTS marker.marker
ALTER COLUMN is_void SET DEFAULT false;
ALTER TABLE IF EXISTS marker.marker
ALTER COLUMN is_void SET NOT NULL;
ALTER TABLE IF EXISTS marker.marker
ALTER COLUMN tenant_id SET NOT NULL;
ALTER TABLE marker.marker
ALTER COLUMN position TYPE CHARACTER VARYING(50);

ALTER TABLE marker.oligo
ALTER id TYPE INT USING id::integer,
ALTER assayid TYPE INT USING assayid::integer,
ALTER assay_id TYPE INT USING assay_id::integer,
ALTER tm TYPE INT USING tm::integer,
ALTER tenant_id TYPE INT USING tenant_id::integer,
ALTER is_void TYPE BOOL USING is_void::boolean,
ALTER creator_id TYPE INT USING creator_id::integer,
ALTER modifier_id TYPE INT USING modifier_id::integer,
ALTER creation_timestamp TYPE timestamp USING creation_timestamp::timestamp without time zone,
ALTER modification_timestamp TYPE timestamp USING modification_timestamp::timestamp without time zone;
ALTER TABLE marker.oligo
ALTER COLUMN comments TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.oligo
ALTER COLUMN primername TYPE character varying(250) COLLATE pg_catalog."default";
ALTER TABLE marker.oligo
ALTER COLUMN primersequence TYPE character varying(750) COLLATE pg_catalog."default";
ALTER TABLE IF EXISTS marker.oligo
ALTER COLUMN creation_timestamp SET DEFAULT now();
ALTER TABLE IF EXISTS marker.oligo
ALTER COLUMN creation_timestamp SET NOT NULL;
ALTER TABLE IF EXISTS marker.oligo
ALTER COLUMN creator_id SET NOT NULL;
ALTER TABLE IF EXISTS marker.oligo
ALTER COLUMN is_void SET DEFAULT false;
ALTER TABLE IF EXISTS marker.oligo
ALTER COLUMN is_void SET NOT NULL;
ALTER TABLE IF EXISTS marker.oligo
ALTER COLUMN tenant_id SET NOT NULL;

TRUNCATE TABLE "marker".marker CASCADE;
TRUNCATE TABLE "marker".assay CASCADE;
TRUNCATE TABLE "marker".oligo CASCADE;
TRUNCATE TABLE "marker".allele CASCADE;
TRUNCATE TABLE "marker".alleledefinition CASCADE;

ALTER TABLE "marker".marker ADD CONSTRAINT "FK_marker_markergroup"
	FOREIGN KEY (markergroup_id) REFERENCES "marker".markergroup (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "marker".assay ADD CONSTRAINT "FK_assay_marker"
	FOREIGN KEY (marker_id) REFERENCES "marker".marker (id) ON DELETE No Action ON UPDATE No Action
;


ALTER TABLE "marker".oligo ADD CONSTRAINT "FK_oligo_assay"
	FOREIGN KEY (assay_id) REFERENCES "marker".assay (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "marker".allele ADD CONSTRAINT "FK_allele_markergroup"
	FOREIGN KEY (markergroup_id) REFERENCES "marker".markergroup (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE "marker".alleledefinition ADD CONSTRAINT "FK_alleledefinition_allele"
	FOREIGN KEY (allele_id) REFERENCES "marker".allele (id) ON DELETE No Action ON UPDATE No Action
;