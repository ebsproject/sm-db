--liquibase formatted sql

--changeset postgres:add_dartag_as_service context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1504 Populate the catalogs for DartTAG



INSERT INTO services.service
("name", description, code, tenant_id, creator_id, purpose_id)
VALUES('DarTag', 'DarTag', 'DarTag', 1, 1, 7);

INSERT INTO services.marker_group
("name", assay_class_id, tenant_id, creator_id)
VALUES
('DarTag', 3, 1, 1),
('RikaV4', (select MAX(id) from services.assay_class), 1, 1)
;
