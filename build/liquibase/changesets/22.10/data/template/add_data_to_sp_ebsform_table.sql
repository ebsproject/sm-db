--liquibase formatted sql

--changeset postgres:add_data_to_sp_ebsform_table context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-280 Add data to service_provider_ebs_form table



WITH _default AS 
    (INSERT INTO ebsform.ebs_form
        ("name", tenant_id, creator_id)
    VALUES
        ('Default', 1, 1)
RETURNING id
    )


INSERT INTO services.service_provider_ebs_form
(ebs_form_id, service_provider_id)
VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
((select id from _default), 7),
((select id from _default), 8),
((select id from _default), 9),
(3, 11),
((select id from _default), 12),
(2, 13);
