--liquibase formatted sql

--changeset postgres:add_new_control_types context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-908 Update the database for batch controls



INSERT INTO sample.control_type
(name, code, description, tenant_id, creator_id)
VALUES
('Blank', 'B', 'Blank', 1, 1),
('Random', 'R', 'Random', 1, 1)
;
