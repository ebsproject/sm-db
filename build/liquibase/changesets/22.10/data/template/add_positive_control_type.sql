--liquibase formatted sql

--changeset postgres:add_positive_control_type context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1525 Add new control_type positive



INSERT INTO sample.control_type
(name, code, description, tenant_id, creator_id)
VALUES
('Positive', 'P', 'Positive', 1, 1)
;
