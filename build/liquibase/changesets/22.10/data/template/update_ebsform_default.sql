--liquibase formatted sql

--changeset postgres:update_ebsform_default context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1526 Update form_type value for ebsform Default


UPDATE ebsform.ebs_form
SET form_type_id = 1
WHERE "name"='Default';
