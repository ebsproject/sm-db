--liquibase formatted sql

--changeset postgres:update_rica_name context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1528 Update service name Rica


UPDATE services.service
SET "name"='Rica V4'
WHERE "name"='Rika V4';

UPDATE services.marker_group
SET "name"='Rica V4'
WHERE "name"='RikaV4';
