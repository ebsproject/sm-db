--liquibase formatted sql

--changeset postgres:update_rica_name_2 context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1528 Update service name Rica


UPDATE services.service
SET "name"='RiCA V4'
WHERE "name"='Rica V4';

UPDATE services.marker_group
SET "name"='RiCA V4'
WHERE "name"='Rica V4';
