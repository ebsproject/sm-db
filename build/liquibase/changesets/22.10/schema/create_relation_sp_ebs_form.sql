--liquibase formatted sql

--changeset postgres:create_relation_sp_ebs_form context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM2-280 Create relation service_provider_ebs_form


CREATE TABLE services.service_provider_ebs_form
(
	ebs_form_id integer NOT NULL,
	service_provider_id integer NOT NULL
)
;

ALTER TABLE services.service_provider_ebs_form ADD CONSTRAINT "PK_service_provider_ebs_form"
	PRIMARY KEY (ebs_form_id,service_provider_id)
;

ALTER TABLE services.service_provider_ebs_form ADD CONSTRAINT "FK_service_provider_ebs_form_ebs_form"
	FOREIGN KEY (ebs_form_id) REFERENCES ebsform.ebs_form (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE services.service_provider_ebs_form ADD CONSTRAINT "FK_service_provider_ebs_form_service_provider"
	FOREIGN KEY (service_provider_id) REFERENCES services.service_provider (id) ON DELETE No Action ON UPDATE No Action
;

