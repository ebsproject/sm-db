--liquibase formatted sql

--changeset postgres:update_dartag_name_service context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1575 Update Services in request Manager



UPDATE services.service
SET "name"='DArTAG-2', description='DArTAG-2', code='DArTAG-2'
WHERE "name"='DarTag';
