--liquibase formatted sql

--changeset postgres:add_column_result_file_in_batch_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: DB-1587 Add column result_file_id uuid NULL in sample.batch table



ALTER TABLE sample.batch ADD result_file_id uuid NULL;
COMMENT ON COLUMN sample.batch.result_file_id IS 'key of the result file with genotype data from the service provider';