--liquibase formatted sql

--changeset postgres:remove_sequence_from_service_provider context:schema splitStatements:false rollbackSplitStatements:false
--comment: Hotfix - remove sequence from service_provider table

ALTER TABLE services.service_provider ALTER COLUMN id DROP DEFAULT;
