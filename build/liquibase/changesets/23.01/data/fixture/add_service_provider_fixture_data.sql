--liquibase formatted sql

--changeset postgres:add_service_provider_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: DB-1527 Add Service Provider fixture data



INSERT INTO services.marker_group
("name", assay_class_id, tenant_id, creator_id)
VALUES
('50 SNP_Panel_v2', 3, 1, 1),
('55 SNP', 1, 1, 1),
('DarTag', 3, 1, 1),
('RiCA V4', 13, 1, 1);


INSERT INTO services.marker
("name", description, tenant_id, creator_id, marker_group_id, source_platform_type, chromosome, "sequence", sequence_startosition_cs_vs1, sequence_endposition_cd_vs1, similar_sequence_chr_96, hybrid_evaluation, fam_allele, hex_allele, primer_gc_allele_x, primer_gc_allele_y, primer_gc_common, blast_score, blast_expect)
VALUES
('snpZM00079', 'snpZM00079', 1, 1, (select id from services.marker_Group where name = '50 SNP_Panel_v2'), '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('snpZM00080', 'snpZM00080', 1, 1, (select id from services.marker_Group where name = '50 SNP_Panel_v2'), '', '', '', '', '', '', '', '', '', '', '', '', '', '');

INSERT INTO services.purpose
("name", code, description, tenant_id, creator_id, id, service_type_id)
VALUES
('Seed Shipment', 'S', 'Seed health analysis to be carried out to meet compliance requirements in order to move seeds, or other material types across country borders. Materials to be shipped already exist in the database', 1, 1, 1, 1),
('Seed Introduction', 'I', 'Seed health analysis to be carried out to meet compliance requirements to import new materials into the country and these materials are non-exist in the EBS database system', 1, 1, 2, 1),
('Seed Certification', 'C', 'Seed health analysis to be carried out to meet compliance requirements to import new materials into the country and these materials are non-exist in the EBS database system', 1, 1, 3, 1),
('Quality Control: F1 Verfication, Ped Ver, Purity, Line Finishing_Fixation', 'Q', 'Quality Control: F1 Verfication, Ped Ver, Purity, Line Finishing_Fixation', 1, 1, 4, 3),
('Germplasm Characterization', 'G', 'Germplasm Characterization: Finger Printing, Diversity', 1, 1, 5, 3),
('Genome-wide Genotyping', 'W', 'Genome-wide Genotyping: GS, GWAS', 1, 1, 6, 3),
('Marker Assisted Selection', 'S', 'Marker Assisted Selection: MAS, MABC', 1, 1, 7, 3),
('Custom Quality Analysis', 'CUQA', 'Custom Quality Analysis', 1, 1, 9, 2),
('Complete Quality Analysis', 'COQA', 'Complete Quality Analysis', 1, 1, 10, 2),
('Basic Quality Analysis', 'BAQA', 'Basic Quality Analysis', 1, 1, 11, 2),
('Nitrogen or Protein Analysis', 'NPA', 'Nitrogen or Protein Analysis', 1, 1, 12, 2),
('MLN Screening', 'MLN', 'MLN Screening', 1, 1, 13, 5),
('TLB Screening', 'TLB', 'TLB Screening', 1, 1, 14, 5),
('GLS Screening', 'GLS', 'GLS Screening', 1, 1, 15, 5),
('MSV Screening', 'MSV', 'MSV Screening', 1, 1, 16, 5),
('Regional Trial Shipments', 'RTS', 'Regional Trial Shipments', 1, 1, 17, 4),
('Genebank Materials GG', 'GMGG', 'Genebank materials (GRP) specific request from GRIN-Global', 1, 1, 18, 4),
('Breeders materials-ad hoc', 'BMA', 'Breeders’ materials-ad hoc', 1, 1, 19, 4),
('Non-propagation materials', 'NPM', 'Non-propagation materials', 1, 1, 20, 4),
('International nurseries', 'INU', 'International nurseries uniform trial (GWP) Shipment', 1, 1, 21, 4),
('Marker Assisted Selection - QLT/Major Gene', 'SQ', 'MAS (Marker Assisted selection)- QTL/Major gene', 1, 1, 22, 6),
('Genomic Selection', 'GS', 'Genomic Selection', 1, 1, 23, 6),
('Quality Control', 'GC', 'Quality Control', 1, 1, 24, 6);


INSERT INTO services.service
("name", description, code, tenant_id, creator_id, id, purpose_id, form_id)
VALUES
('Seed Shipment', 'Seed health analysis to enable Seed Shipment for maize, wheat, barley, triticale', 'SS', 1, 1, 1, 1, NULL),
('Other Shipment', 'Seed health analysis to enable Shipment for flour, ground, leaves, plants, roots, soil, DNA, etc.', 'OS', 1, 1, 2, 1, NULL),
('Seed Introduction', 'Seed health analysis to enable the seed Introduction for maize, wheat, barley, triticale', 'SI', 1, 1, 3, 2, NULL),
('Other Introduction', 'Seed health analysis to enable the introduction for beans, fava beans, peas, lentils, forages, potatoes, cabbages, beets, spinach, flour, ground grains, leaves, plants, roots, soil, DNA, etc.', 'OS', 1, 1, 4, 2, NULL),
('Seed Certification', 'Seed health analysis to enable seed certification for maize, wheat, barley, triticale', 'SC', 1, 1, 5, 3, NULL),
('Other Certification', 'Seed health analysis to enable the Certification for beans, fava beans, peas, lentils, forages, potatoes, cabbages, beets, spinach, flour, ground grains, leaves, plants, roots, soil, DNA, etc.', 'OC', 1, 1, 6, 3, NULL),
('Maize Custom Quality Analyses', 'User-defined set of quality analyses from the maize quality lab', 'MQU', 1, 1, 7, 9, NULL),
('Wheat Complete Quality Analysis', 'Wheat complete analysis which includes 11 separate assays', 'WQC', 1, 1, 8, 10, NULL),
('Wheat Basic Quality Analysis', 'Wheat basic quality analysis which includes 5 separate assays', 'WQB', 1, 1, 9, 11, NULL),
('Wheat Nitrogen or Protein Analysis', 'Wheat nitrogen or protein analysis using one of three possible methods', 'WQN', 1, 1, 10, 12, NULL),
('45 SNP_Diversity_Panel V1', '45 SNP_Diversity_Panel V1', '45SNP', 1, 1, 13, 5, NULL),
('50 SNP_Diversity_Panel V1', '50 SNP_Diversity_Panel V1', '50SNP1', 1, 1, 14, 5, NULL),
('55 SNP_Panel 1 V1', '55 SNP_Panel 1 V1', '55SNP', 1, 1, 16, 4, NULL),
('DArT_Seq', 'DArT_Seq', 'DArT', 1, 1, 17, 6, NULL),
('Diversity_Panel V2', 'Diversity_Panel V2', 'Divers', 1, 1, 18, 5, NULL),
('rAmpSeq', 'rAmpSeq', 'rAmpSeq', 1, 1, 19, 6, NULL),
('rhAmpSeq', 'rhAmpSeq', 'rhAmpSeq', 1, 1, 20, 6, NULL),
('Sequencing', 'Sequencing', 'Sequencing', 1, 1, 21, 6, NULL),
('User_selected_markers', 'User_selected_markers', 'User', 1, 1, 22, 7, NULL),
('MLN_Indexing', 'MLN_Indexing', 'MLNI', 1, 1, 23, 13, NULL),
('MLN_Screening', 'MLN_Screening', 'MLNS', 1, 1, 24, 13, NULL),
('MCMV', 'MCMV', 'MCMV', 1, 1, 25, 13, NULL),
('SCMV', 'SCMV', 'SCMV', 1, 1, 26, 13, NULL),
('TLB', 'TLB', 'TLB', 1, 1, 27, 14, NULL),
('GLS', 'GLS', 'GLS', 1, 1, 28, 15, NULL),
('MSV', 'MSV', 'MSV', 1, 1, 29, 16, NULL),
('Regional Trial Shipments', 'Regional Trial Shipments', 'RTS', 1, 1, 30, 17, NULL),
('Genebank Materials GG', 'Genebank Materials GG', 'GMGG', 1, 1, 31, 18, NULL),
('Breeders Materials Requests', 'Breeders’ Materials Requests', 'BMR', 1, 1, 32, 19, NULL),
('Non-propagation materials', 'Non-propagation materials', 'NPM', 1, 1, 33, 20, NULL),
('International nurseries', 'International nurseries', 'INU', 1, 1, 34, 21, NULL),
('Agriplex', 'Agriplex', 'Agriplex', 1, 1, 38, 24, NULL),
('DArTAG-2', 'DArTAG-2', 'DArTAG-2', 1, 1, 40, 7, NULL),
('RiCA V4', 'Rika V4', 'Rika V4', 1, 1, 36, 22, NULL),
('RiCA V4', 'Rika V4', 'Rika V4', 1, 1, 37, 23, NULL);

INSERT INTO services.service_provider
(code, "name", tenant_id, creator_id, id, crop_id, program_id)
VALUES
('WMBL', 'CIMMYT Mexico Wheat Molecular Laboratory', 1, 1, 1, NULL, NULL),
('ASM', 'CIMMYT India Maize Molecular Laboratory', 1, 1, 2, NULL, NULL),
('AFM', 'CIMMYT Kenya Maize Molecular Laboratory', 1, 1, 3, NULL, NULL),
('LAM', 'CIMMYT Mexico Maize Molecular Laboratory', 1, 1, 4, NULL, NULL),
('SHL', 'CIMMYT Mexico Maize Seed Health Laboratory', 1, 1, 5, NULL, NULL),
('SHL', 'CIMMYT Mexico Wheat Seed Health Laboratory', 1, 1, 6, NULL, NULL),
('MZQ', 'CIMMYT Mexico Maize Quality Laboratory', 1, 1, 7, NULL, NULL),
('WQL', 'CIMMYT Mexico Wheat Quality Laboratory', 1, 1, 8, NULL, NULL),
('SDU', 'CIMMYT Mexico Seed Distribution Unit', 1, 1, 9, NULL, NULL),
('SHU', 'IRRI Philippines Seed Health Unit', 1, 1, 11, NULL, NULL),
('GQNSL', 'IRRI Philippines Grain Quality Nutrition Service Lab', 1, 1, 12, NULL, NULL),
('GSL', 'IRRI Philippines Genotyping Service Lab', 1, 1, 13, NULL, NULL);


INSERT INTO services.service_provider_ebs_form
(ebs_form_id, service_provider_id)
VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(6, 7),
(6, 8),
(6, 9),
(3, 11),
(6, 12),
(2, 13);


INSERT INTO services.service_provider_service_type
(service_type_id, service_provider_id)
VALUES
(1, 5),
(1, 6),
(1, 11),
(2, 7),
(2, 8),
(2, 12),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(4, 9),
(6, 13);


INSERT INTO sample.batch_code
("name", code, last_batch_number, last_plate_number, last_sample_number, tenant_id, creation_timestamp, modification_timestamp, creator_id, modifier_id, is_void, id, service_provider_id)
VALUES
('BS1004BW-DW-QC200313', 'BS1004BW-DW-QC200313', 0, 0, 0, 1, '2021-01-11 14:54:51.936', NULL, 1, NULL, false, 1, 1),
('BS1004BW-DW-QC200313', 'BS1004BW-DW-QC200313', 0, 0, 0, 1, '2022-10-11 11:38:43.928', NULL, 1, NULL, false, 2, 2),
('BS1004BW-DW-QC200313', 'BS1004BW-DW-QC200313', 0, 0, 0, 1, '2022-10-11 11:38:43.928', NULL, 1, NULL, false, 3, 3),
('BS1004BW-DW-QC200313', 'BS1004BW-DW-QC200313', 0, 0, 0, 1, '2022-10-11 11:38:43.928', NULL, 1, NULL, false, 4, 4),
('BS1004BW-DW-QC200313', 'BS1004BW-DW-QC200313', 0, 0, 0, 1, '2022-10-11 11:38:43.928', NULL, 1, NULL, false, 5, 13);


SELECT setval('services.purpose_id_seq', (SELECT MAX(id) FROM services.marker_group));
SELECT setval('services.service_id_seq', (SELECT MAX(id) FROM services.marker));
SELECT setval('services.service_provider_id_seq', (SELECT MAX(id) FROM services.marker_group));
SELECT setval('sample.batch_code_id_seq', (SELECT MAX(id) FROM services.marker));



