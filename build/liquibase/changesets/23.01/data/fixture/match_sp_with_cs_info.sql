--liquibase formatted sql

--changeset postgres:match_sp_with_cs_info context:fixture splitStatements:false rollbackSplitStatements:false
--comment: SM-1001 Match Service Providers with CS data



INSERT INTO services.service_provider
(id, code, name, tenant_id, creator_id)
VALUES
(77, 'WMBL', 'CIMMYT Mexico Wheat Molecular Laboratory', 1, 1), --1
(78, 'ASM', 'CIMMYT India Maize Molecular Laboratory', 1, 1), --2
(79, 'AFM', 'CIMMYT Kenya Maize Molecular Laboratory', 1, 1), --3
(80, 'LAM', 'CIMMYT Mexico Maize Molecular Laboratory', 1, 1), --4
(81, 'SHLM', 'CIMMYT Mexico Maize Seed Health Laboratory', 1, 1), --5
(82, 'SHLW', 'CIMMYT Mexico Wheat Seed Health Laboratory', 1, 1), --6
(83, 'MZQ', 'CIMMYT Mexico Maize Quality Laboratory', 1, 1), --7
(84, 'WQL', 'CIMMYT Mexico Wheat Quality Laboratory', 1, 1), --8
(85, 'SDU', 'CIMMYT Mexico Seed Distribution Unit', 1, 1), --9
(87, 'SHU', 'IRRI Philippines Seed Health Unit', 1, 1), --11
(88, 'GQNSL', 'IRRI Philippines Grain Quality Nutrition Service Lab', 1, 1), --12
(86, 'GSL', 'IRRI Philippines Genotyping Service Lab', 1, 1); --13



UPDATE sample.batch
SET service_provider_id =   CASE 
                                WHEN service_provider_id = 1 THEN 77
                                WHEN service_provider_id = 2 THEN 78
                                WHEN service_provider_id = 3 THEN 79
                                WHEN service_provider_id = 4 THEN 80
                                WHEN service_provider_id = 5 THEN 81
                                WHEN service_provider_id = 6 THEN 82
                                WHEN service_provider_id = 7 THEN 83
                                WHEN service_provider_id = 8 THEN 84
                                WHEN service_provider_id = 9 THEN 85
                                WHEN service_provider_id = 11 THEN 87
                                WHEN service_provider_id = 12 THEN 88
                                WHEN service_provider_id = 13 THEN 86
                            END;



UPDATE sample.batch_code
SET service_provider_id = CASE 
                                WHEN service_provider_id = 1 THEN 77
                                WHEN service_provider_id = 2 THEN 78
                                WHEN service_provider_id = 3 THEN 79
                                WHEN service_provider_id = 4 THEN 80
                                WHEN service_provider_id = 5 THEN 81
                                WHEN service_provider_id = 6 THEN 82
                                WHEN service_provider_id = 7 THEN 83
                                WHEN service_provider_id = 8 THEN 84
                                WHEN service_provider_id = 9 THEN 85
                                WHEN service_provider_id = 11 THEN 87
                                WHEN service_provider_id = 12 THEN 88
                                WHEN service_provider_id = 13 THEN 86
                            END;


UPDATE services.service_provider_ebs_form
SET service_provider_id = CASE 
                                WHEN service_provider_id = 1 THEN 77
                                WHEN service_provider_id = 2 THEN 78
                                WHEN service_provider_id = 3 THEN 79
                                WHEN service_provider_id = 4 THEN 80
                                WHEN service_provider_id = 5 THEN 81
                                WHEN service_provider_id = 6 THEN 82
                                WHEN service_provider_id = 7 THEN 83
                                WHEN service_provider_id = 8 THEN 84
                                WHEN service_provider_id = 9 THEN 85
                                WHEN service_provider_id = 11 THEN 87
                                WHEN service_provider_id = 12 THEN 88
                                WHEN service_provider_id = 13 THEN 86
                            END;

UPDATE services.service_provider_service_type
SET service_provider_id = CASE 
                                WHEN service_provider_id = 1 THEN 77
                                WHEN service_provider_id = 2 THEN 78
                                WHEN service_provider_id = 3 THEN 79
                                WHEN service_provider_id = 4 THEN 80
                                WHEN service_provider_id = 5 THEN 81
                                WHEN service_provider_id = 6 THEN 82
                                WHEN service_provider_id = 7 THEN 83
                                WHEN service_provider_id = 8 THEN 84
                                WHEN service_provider_id = 9 THEN 85
                                WHEN service_provider_id = 11 THEN 87
                                WHEN service_provider_id = 12 THEN 88
                                WHEN service_provider_id = 13 THEN 86
                            END;

UPDATE sample.request
SET service_provider_id = CASE 
                                WHEN service_provider_id = 1 THEN 77
                                WHEN service_provider_id = 2 THEN 78
                                WHEN service_provider_id = 3 THEN 79
                                WHEN service_provider_id = 4 THEN 80
                                WHEN service_provider_id = 5 THEN 81
                                WHEN service_provider_id = 6 THEN 82
                                WHEN service_provider_id = 7 THEN 83
                                WHEN service_provider_id = 8 THEN 84
                                WHEN service_provider_id = 9 THEN 85
                                WHEN service_provider_id = 11 THEN 87
                                WHEN service_provider_id = 12 THEN 88
                                WHEN service_provider_id = 13 THEN 86
                            END;


SELECT setval('services.service_provider_id_seq', (SELECT MAX(id) FROM services.service_provider));