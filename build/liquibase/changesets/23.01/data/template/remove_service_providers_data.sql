--liquibase formatted sql

--changeset postgres:remove_service_providers_data context:template splitStatements:false rollbackSplitStatements:false
--comment: DB-1527 Remove service providers and all related data from SM template version



DELETE FROM sample.batch_code;
DELETE FROM services.service_provider_service_type;
DELETE FROM services.service_provider_ebs_form spef;
DELETE FROM services.service_provider;
DELETE FROM services.service;
DELETE FROM services.purpose;
DELETE FROM services.marker;
DELETE FROM services.marker_group;

ALTER SEQUENCE sample.batch_code_id_seq RESTART WITH 1;
ALTER SEQUENCE services.service_provider_id_seq RESTART WITH 1;
ALTER SEQUENCE services.service_id_seq RESTART WITH 1;
ALTER SEQUENCE services.purpose_id_seq RESTART WITH 1;
ALTER SEQUENCE services.marker_id_seq RESTART WITH 1;
ALTER SEQUENCE services.marker_group_id_seq RESTART WITH 1;

