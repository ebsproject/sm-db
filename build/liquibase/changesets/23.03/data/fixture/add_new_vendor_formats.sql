--liquibase formatted sql

--changeset postgres:add_new_vendor_formats context:fixture splitStatements:false rollbackSplitStatements:false
--comment: SM-1256 Add new vendor formats



INSERT INTO services.technology_platform
("name", description, tenant_id, creator_id, vendor_id)
VALUES
('low density GSL', 'low density GSL', 1, 1, (select id from services.vendor where code = 'Intertek')),
('mid density GSL', 'mid density GSL', 1, 1, (select id from services.vendor where code = 'Intertek'));

INSERT INTO services.assay_class
("name", description, tenant_id, creator_id)
VALUES
('Fixed LDSG', 'Fixed LDSG', 1, 1),
('Fixed MDSG', 'Fixed MDSG', 1, 1);

INSERT INTO services.technology_platform_assay_class
(technology_platform_id, assay_class_id)
VALUES
((SELECT id FROM services.technology_platform WHERE "name" = 'low density GSL'), (SELECT id FROM services.assay_class WHERE "name" = 'Fixed LDSG')),
((SELECT id FROM services.technology_platform WHERE "name" = 'mid density GSL'), (SELECT id FROM services.assay_class WHERE "name" = 'Fixed MDSG'));

INSERT INTO services.vendor_control
("position", tenant_id, creator_id, technology_platform_id)
VALUES
('H11,H12', 1, 1, (SELECT id FROM services.technology_platform WHERE "name" = 'low density GSL')),
('G12,H12', 1, 1, (SELECT id FROM services.technology_platform WHERE "name" = 'mid density GSL'));
