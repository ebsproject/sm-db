--liquibase formatted sql

--changeset postgres:update_control_id context:fixture splitStatements:false rollbackSplitStatements:false
--comment: SM-1256 Update control id for new vendors



UPDATE services.vendor_control
SET id = 7
WHERE "position"='H11,H12' and technology_platform_id= (select id from services.technology_platform where "name" = 'low density GSL');

UPDATE services.vendor_control
SET id = 8 , tenant_id=1
WHERE "position"='G12,H12' and technology_platform_id= (select id from services.technology_platform where "name" = 'mid density GSL');;

SELECT setval('services.vendor_control_id_seq', (SELECT MAX(id) FROM services.vendor_control));