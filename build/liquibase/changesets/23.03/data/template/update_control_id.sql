--liquibase formatted sql

--changeset postgres:update_control_id context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1256 Update control id for vendors



UPDATE services.vendor_control
SET  id = 6
WHERE "position"='G12, H12' and technology_platform_id=6;

SELECT setval('services.vendor_control_id_seq', (SELECT MAX(id) FROM services.vendor_control));
