--liquibase formatted sql

--changeset postgres:increase_column_lenght context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1226 increase lenght of description column in status table


ALTER TABLE sample.status ALTER COLUMN description TYPE varchar(150);