--liquibase formatted sql

--changeset postgres:populate_marker_group_purpose context:fixture splitStatements:false rollbackSplitStatements:false
--comment: SM-1317 Populate marker_group_purpose table




do $$
declare _purpose integer;

begin
SELECT id 
FROM services.purpose 
WHERE "name" = 'Quality Control' AND service_type_id = (SELECT id FROM services.service_type WHERE "name" =  'Genotyping Analysis IRRI')
INTO _purpose;

INSERT INTO services.marker_group_purpose
(marker_group_id, purpose_id, creator_id)
VALUES
(1, _purpose, 1),
(2, _purpose, 1),
(3, _purpose, 1),
(4, _purpose, 1),
(5, _purpose, 1),
(6, _purpose, 1)
;

end $$