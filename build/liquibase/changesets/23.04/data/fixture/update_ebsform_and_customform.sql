--liquibase formatted sql

--changeset postgres:update_ebsform_and_customform context:fixture splitStatements:false rollbackSplitStatements:false
--comment: SM-1209 Change team work contact to NOT required and  SM-1262 update contact person validation message


--comment: EBSFORM.COMPONENT  Change team work contact to NOT required
UPDATE ebsform.component
SET name='contactPerson', title='Contact Person', "size"='12, 12, 12, 12, 12', "label"='Contact Person', default_value=NULL, on_change=NULL, "rule"='Please provide the name of person to contact.', tenant_id=1, creation_timestamp='2022-12-06 19:31:15.657', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, group_id=2, tab_id=NULL, component_type_id=1, ebs_form_id=2
WHERE id=7;

--comment: CUSTOMFORM.FIELD  Change team work contact to NOT required and Change Data Estimated By Form ID to remove them from current dynamic forms
UPDATE customform.field
SET "name"='idAdminContact', "label"='Admin Contact', tooltip='Admin Contact', default_value=NULL, is_required=false, "order"=1, is_base=true, tenant_id=1, creation_timestamp='2022-12-06 19:31:15.657', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=1, data_type_id=4, fields_scale_value_id=NULL, validation_regex_id=NULL, form_id=1, group_id=2, tab_id=NULL
WHERE id=7;

UPDATE customform.field
SET "name"='idTeamWorkContactEmail', "label"='Team Work Contact Email', tooltip='Team Work Contact Email', default_value=NULL, is_required=false, "order"=2, is_base=false, tenant_id=2, creation_timestamp='2022-12-06 19:31:15.657', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=23, data_type_id=1, fields_scale_value_id=NULL, validation_regex_id=NULL, form_id=1, group_id=2, tab_id=NULL
WHERE id=28;

UPDATE customform.field
SET "name"='idEstimatedBy', "label"='Estimated By', tooltip='Data Estimated By', default_value=NULL, is_required=true, "order"=4, is_base=false, tenant_id=3, creation_timestamp='2022-12-06 19:31:15.657', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=5, data_type_id=3, fields_scale_value_id=NULL, validation_regex_id=1, form_id=5, group_id=2, tab_id=NULL
WHERE id=10;

UPDATE customform.field
SET "name"='idEstimatedBy', "label"='Estimated By', tooltip='Estimated By', default_value=NULL, is_required=true, "order"=4, is_base=false, tenant_id=2, creation_timestamp='2022-12-06 19:31:15.657', modification_timestamp=NULL, creator_id=1, modifier_id=NULL, is_void=false, component_id=21, data_type_id=3, fields_scale_value_id=NULL, validation_regex_id=3, form_id=5, group_id=2, tab_id=NULL
WHERE id=26;

