--liquibase formatted sql

--changeset postgres:remove_space_in_position_column context:template splitStatements:false rollbackSplitStatements:false
--comment: Remove space in vendor_control.position



UPDATE services.vendor_control
SET "position"='G12,H12'
WHERE "position"='G12, H12';
