--liquibase formatted sql

--changeset postgres:update_create_desing_label context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1211 Update Create Design label



UPDATE sample.status
SET description='Design Created', "name"='Design Created'
WHERE "name"='Create Design';
