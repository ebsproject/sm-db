--liquibase formatted sql

--changeset postgres:add_marker_purpose_relationship context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1317 Create relationship marker group - purpose - request



CREATE TABLE services.marker_group_purpose
(
    id integer NOT NULL   DEFAULT NEXTVAL(('services."marker_purpose_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	marker_group_id integer NOT NULL,
    purpose_id integer NOT NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)

)
;

CREATE SEQUENCE services.marker_group_purpose_id_seq INCREMENT 1 START 1;

ALTER TABLE services.marker_group_purpose ADD CONSTRAINT "PK_marker_group_purpose"
	PRIMARY KEY (id)
;

ALTER TABLE services.marker_group_purpose ADD CONSTRAINT "FK_marker_group_purpose_purpose"
	FOREIGN KEY (purpose_id) REFERENCES services.purpose (id) ON DELETE No Action ON UPDATE No Action
;

CREATE TABLE sample.request_marker_group
(
    id integer NOT NULL   DEFAULT NEXTVAL(('sample."request_marker_group_id_seq"'::text)::regclass),	-- Unique identifier of the record within the table.
	request_id integer NOT NULL,
	marker_group_id integer NOT NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
	
)
;

CREATE SEQUENCE sample.request_marker_group_id_seq INCREMENT 1 START 1;

ALTER TABLE sample.request_marker_group ADD CONSTRAINT "PK_request_marker_group"
	PRIMARY KEY (id)
;

ALTER TABLE sample.request_marker_group ADD CONSTRAINT "FK_request_marker_group_request"
	FOREIGN KEY (request_id) REFERENCES sample.request (id) ON DELETE No Action ON UPDATE No Action
;

COMMENT ON COLUMN services.marker_group_purpose.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN services.marker_group_purpose.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN services.marker_group_purpose.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN services.marker_group_purpose.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN services.marker_group_purpose.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN services.marker_group_purpose.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN sample.request_marker_group.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN sample.request_marker_group.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN sample.request_marker_group.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN sample.request_marker_group.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN sample.request_marker_group.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN sample.request_marker_group.modifier_id
	IS 'ID of the user who last modified the record'
;

