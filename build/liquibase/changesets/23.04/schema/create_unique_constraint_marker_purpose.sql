--liquibase formatted sql

--changeset postgres:create_unique_constraint_marker_purpose context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1317 Update marker_group_purpose PK



ALTER TABLE services.marker_group_purpose ADD CONSTRAINT purpose_marker UNIQUE (purpose_id,marker_group_id);