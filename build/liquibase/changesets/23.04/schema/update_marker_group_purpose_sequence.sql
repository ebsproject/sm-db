--liquibase formatted sql

--changeset postgres:update_marker_group_purpose_sequence_name context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1317 Create relationship marker group - purpose



ALTER TABLE services.marker_group_purpose ALTER COLUMN id SET DEFAULT NEXTVAL(('services."marker_group_purpose_id_seq"'::text)::regclass);

