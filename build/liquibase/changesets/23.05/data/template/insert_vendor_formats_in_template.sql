--liquibase formatted sql

--changeset postgres:insert_vendor_formats_in_template context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1256 Add new vendor formats



SELECT setval('services.vendor_control_id_seq', (SELECT MAX(id) FROM services.vendor_control));

INSERT INTO services.technology_platform 
    ("name", description, tenant_id, creator_id, vendor_id)
        SELECT 'low density GSL', 'low density GSL', 1, 1, (select id from services.vendor where code = 'Intertek')
        WHERE
        NOT EXISTS (
            SELECT id FROM services.technology_platform  WHERE "name" = 'low density GSL'
    );

INSERT INTO services.technology_platform 
    ("name", description, tenant_id, creator_id, vendor_id)
        SELECT 'mid density GSL', 'mid density GSL', 1, 1, (select id from services.vendor where code = 'Intertek')
        WHERE
        NOT EXISTS (
            SELECT id FROM services.technology_platform  WHERE "name" = 'mid density GSL'
    );

INSERT INTO services.assay_class
    ("name", description, tenant_id, creator_id)
        SELECT 'Fixed LDSG', 'Fixed LDSG', 1, 1
        WHERE
        NOT EXISTS (
            SELECT id FROM services.assay_class  WHERE "name" = 'Fixed LDSG'
    );

INSERT INTO services.assay_class
    ("name", description, tenant_id, creator_id)
        SELECT 'Fixed MDSG', 'Fixed MDSG', 1, 1
        WHERE
        NOT EXISTS (
            SELECT id FROM services.assay_class  WHERE "name" = 'Fixed MDSG'
    );


INSERT INTO services.technology_platform_assay_class
    (technology_platform_id, assay_class_id)
        SELECT (SELECT id FROM services.technology_platform WHERE "name" = 'low density GSL'), (SELECT id FROM services.assay_class WHERE "name" = 'Fixed LDSG')
        WHERE
        NOT EXISTS (
            SELECT technology_platform_id, assay_class_id FROM services.technology_platform_assay_class
            WHERE technology_platform_id = (SELECT technology_platform_id FROM services.technology_platform WHERE "name" = 'low density GSL')
            AND  assay_class_id = (SELECT id FROM services.assay_class WHERE "name" = 'Fixed LDSG')
    );

INSERT INTO services.technology_platform_assay_class
    (technology_platform_id, assay_class_id)
        SELECT (SELECT id FROM services.technology_platform WHERE "name" = 'mid density GSL'), (SELECT id FROM services.assay_class WHERE "name" = 'Fixed MDSG')
        WHERE
        NOT EXISTS (
            SELECT technology_platform_id, assay_class_id FROM services.technology_platform_assay_class
            WHERE technology_platform_id = (SELECT id FROM services.technology_platform WHERE "name" = 'mid density GSL')
            AND assay_class_id = (SELECT id FROM services.assay_class WHERE "name" = 'Fixed MDSG')
    );


INSERT INTO services.vendor_control
    ("position", tenant_id, creator_id, technology_platform_id)
        SELECT 'H11,H12', 1, 1, (SELECT id FROM services.technology_platform WHERE "name" = 'low density GSL')
        WHERE
        NOT EXISTS (
            SELECT id FROM services.vendor_control 
            WHERE "position" = 'H11,H12' 
            AND technology_platform_id = (SELECT id FROM services.technology_platform WHERE "name" = 'low density GSL')
    );

INSERT INTO services.vendor_control
    ("position", tenant_id, creator_id, technology_platform_id)
        SELECT 'G12,H12', 1, 1, (SELECT id FROM services.technology_platform WHERE "name" = 'mid density GSL')
        WHERE
        NOT EXISTS (
            SELECT id FROM services.vendor_control 
            WHERE "position" = 'G12,H12' 
            AND technology_platform_id = (SELECT id FROM services.technology_platform WHERE "name" = 'mid density GSL')
    );   


UPDATE services.vendor_control
SET id = 7
WHERE "position"='H11,H12' and technology_platform_id= (select id from services.technology_platform where "name" = 'low density GSL');

UPDATE services.vendor_control
SET id = 8 , tenant_id=1
WHERE "position"='G12,H12' and technology_platform_id= (select id from services.technology_platform where "name" = 'mid density GSL');

SELECT setval('services.vendor_control_id_seq', (SELECT MAX(id) FROM services.vendor_control));



--changeset postgres:insert_dart_vendor_control context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1435 Add vendor controls for Dart



INSERT INTO services.vendor_control
(id, "position", tenant_id, technology_platform_id, creator_id)
VALUES(9, 'G12,H12', 1, (SELECT id FROM services.technology_platform where "name" = 'high density'), 1);

SELECT setval('services.vendor_control_id_seq', (SELECT MAX(id) FROM services.vendor_control));