--liquibase formatted sql

--changeset postgres:update_vendor_id context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1435 Update Agriplex id



UPDATE services.vendor
SET id=4
WHERE code='Agriplex';

SELECT setval('services.vendor_id_seq', (SELECT MAX(id) FROM services.vendor));