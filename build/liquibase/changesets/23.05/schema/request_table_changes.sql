--liquibase formatted sql

--changeset postgres:add_samples_per_item_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1417 Include number of samples per item in request table



ALTER TABLE sample.request 
 ADD COLUMN samples_per_item integer NULL;


--changeset postgres:update_column_data_type context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1434 Update data type for comments column in request_status table


ALTER TABLE sample.request_status 
 ALTER COLUMN comments TYPE varchar(500);
