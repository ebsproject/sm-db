--liquibase formatted sql

--changeset postgres:update_vendor_constraint context:schema splitStatements:false rollbackSplitStatements:false
--preconditions onFail:CONTINUE onError:HALT
--precondition-sql-check expectedResult:0 SELECT CASE NOT EXISTS(SELECT * FROM information_schema.referential_constraints WHERE constraint_name SIMILAR TO '%FK_technology_platform_vendor%|%FK_service_vendor_vendor%|%FK_vendor_shipment_vendor%') WHEN TRUE THEN 1 ELSE 0 END; 
--comment: SM-1435 Update Vendor constraint



ALTER TABLE services.technology_platform DROP CONSTRAINT "FK_technology_platform_vendor";
ALTER TABLE services.service_vendor DROP CONSTRAINT "FK_service_vendor_vendor";
ALTER TABLE services.vendor_shipment DROP CONSTRAINT "FK_vendor_shipment_vendor";


ALTER TABLE services.technology_platform ADD CONSTRAINT "FK_technology_platform_vendor" FOREIGN KEY (vendor_id) REFERENCES services.vendor(id) ON UPDATE CASCADE;
ALTER TABLE services.service_vendor ADD CONSTRAINT "FK_service_vendor_vendor" FOREIGN KEY (vendor_id) REFERENCES services.vendor(id) ON UPDATE CASCADE;
ALTER TABLE services.vendor_shipment ADD CONSTRAINT "FK_vendor_shipment_vendor" FOREIGN KEY (vendor_id) REFERENCES services.vendor(id) ON UPDATE CASCADE;