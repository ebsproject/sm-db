--liquibase formatted sql

--changeset postgres:populate_marker_group_purpose context:fixture splitStatements:false rollbackSplitStatements:false
--comment: SM-1507 Populate trait_category_purpose table



INSERT INTO services.trait_category_purpose
(trait_category_id, purpose_id, creator_id)
VALUES
(1, (SELECT id FROM services.purpose WHERE name = 'SQ'), 1),
(2, (SELECT id FROM services.purpose WHERE code = 'Marker Assisted Selection'), 1),
(3, (SELECT id FROM services.purpose WHERE code = 'SQ'), 1),
(4, (SELECT id FROM services.purpose WHERE code = 'SQ'), 1),
(5, (SELECT id FROM services.purpose WHERE code = 'SQ'), 1),
(6, (SELECT id FROM services.purpose WHERE name = 'Marker Assisted Selection'), 1)
;

