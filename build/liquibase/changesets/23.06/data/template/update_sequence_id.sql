--liquibase formatted sql

--changeset postgres:update_sequence_id context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1537 Update sequence id



SELECT setval('services.purpose_id_seq', (SELECT MAX(id) FROM services.purpose));

SELECT setval('services.service_id_seq', (SELECT MAX(id) FROM services.service));