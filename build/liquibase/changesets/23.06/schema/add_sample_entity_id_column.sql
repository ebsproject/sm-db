--liquibase formatted sql

--changeset postgres:add_sample_entity_id_column context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1466 Add sample_entity_id column



ALTER TABLE sample.sample_detail 
 ADD COLUMN sample_entity_id integer NULL;