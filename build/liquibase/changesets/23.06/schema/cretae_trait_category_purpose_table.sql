--liquibase formatted sql

--changeset postgres:cretae_trait_category_purpose_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1507 Review Trait category and purpose relationship



CREATE TABLE sample.request_trait
(
	id serial NOT NULL,	-- Unique identifier of the record within the table.
    trait_id integer NULL,
    request_id integer NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
)
;

ALTER TABLE sample.request_trait ADD CONSTRAINT "PK_request_trait"
	PRIMARY KEY (id)
;

ALTER TABLE sample.request_trait ADD CONSTRAINT "FK_request_trait_request"
	FOREIGN KEY (request_id) REFERENCES sample.request (id) ON DELETE No Action ON UPDATE No Action
;

CREATE TABLE services.trait_category_purpose
(
	id serial NOT NULL,	-- Unique identifier of the record within the table.
    trait_category_id integer NULL,
	purpose_id integer NULL,
	creation_timestamp timestamp without time zone NOT NULL   DEFAULT now(),	-- Timestamp when the record was added to the table
	modification_timestamp timestamp without time zone NULL,	-- Timestamp when the record was last modified
	creator_id integer NOT NULL,	-- ID of the user who added the record to the table
	modifier_id integer NULL,	-- ID of the user who last modified the record
	is_void boolean NOT NULL   DEFAULT false	-- Indicator whether the record is deleted (true) or not (false)
	)
;

ALTER TABLE services.trait_category_purpose ADD CONSTRAINT "PK_trait_category_purpose"
	PRIMARY KEY (id)
;

COMMENT ON COLUMN services.trait_category_purpose.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN services.trait_category_purpose.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN services.trait_category_purpose.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN services.trait_category_purpose.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN services.trait_category_purpose.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN services.trait_category_purpose.modifier_id
	IS 'ID of the user who last modified the record'
;

COMMENT ON COLUMN sample.request_trait.creation_timestamp
	IS 'Timestamp when the record was added to the table'
;

COMMENT ON COLUMN sample.request_trait.creator_id
	IS 'ID of the user who added the record to the table'
;

COMMENT ON COLUMN sample.request_trait.id
	IS 'Unique identifier of the record within the table.'
;

COMMENT ON COLUMN sample.request_trait.is_void
	IS 'Indicator whether the record is deleted (true) or not (false)'
;

COMMENT ON COLUMN sample.request_trait.modification_timestamp
	IS 'Timestamp when the record was last modified'
;

COMMENT ON COLUMN sample.request_trait.modifier_id
	IS 'ID of the user who last modified the record'
;
