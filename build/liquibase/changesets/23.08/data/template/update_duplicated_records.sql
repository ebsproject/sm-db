--liquibase formatted sql

--changeset postgres:update_duplicated_records context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1757 Update duplicated reords



UPDATE services.purpose
SET code='MAS'
WHERE "name"='Marker Assisted Selection';

DELETE FROM services.service_provider WHERE id <=13;
