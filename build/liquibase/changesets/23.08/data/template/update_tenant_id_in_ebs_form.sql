--liquibase formatted sql

--changeset postgres:update_tenant_id_in_ebs_form context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1756 Update tenant_id in ebs_form



UPDATE ebsform.ebs_form
SET tenant_id=1;

