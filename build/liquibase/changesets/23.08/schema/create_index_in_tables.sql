--liquibase formatted sql

--changeset postgres:create_index_in_tables context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1759 Create index in SM Tables



CREATE UNIQUE INDEX request_list_member_id_idx ON sample.request_list_member USING btree (id);

