--liquibase formatted sql

--changeset postgres:create_unique_constraints context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1757 Create Unique constraints



ALTER TABLE services.service_provider 
  ADD CONSTRAINT "UQ_sp_code" UNIQUE (code);

ALTER TABLE services.service_provider 
  ADD CONSTRAINT "UQ_sp_name" UNIQUE (name);

ALTER TABLE services.purpose
  ADD CONSTRAINT "UQ_purpose_code" UNIQUE (code);

ALTER TABLE services.purpose
  ADD CONSTRAINT "UQ_purpose_name" UNIQUE (name);

ALTER TABLE services.marker_group
  ADD CONSTRAINT "UQ_marker_group_name" UNIQUE (name);

ALTER TABLE services.service_type 
  ADD CONSTRAINT "UQ_service_type_code" UNIQUE (code);

ALTER TABLE services.service_type
  ADD CONSTRAINT "UQ_service_type_name" UNIQUE (name);
