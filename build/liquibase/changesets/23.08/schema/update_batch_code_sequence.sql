--liquibase formatted sql

--changeset postgres:update_batch_code_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1750 Update Batch Code sequence



SELECT setval('sample.batch_code_id_seq', (SELECT MAX(id) FROM sample.batch_code));