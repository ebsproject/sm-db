--liquibase formatted sql

--changeset postgres:update_service_provider_sequence context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1750 Update Service Provider sequence



ALTER TABLE services.service_provider ALTER COLUMN id SET DEFAULT NEXTVAL(('services."service_provider_id_seq"'::text)::regclass);

