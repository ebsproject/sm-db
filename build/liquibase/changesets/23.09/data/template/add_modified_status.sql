--liquibase formatted sql

--changeset postgres:add_modified_status context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1810 Add Modified status in smdb


INSERT INTO sample.status
    (description, "name", tenant_id, creator_id)
VALUES
    ('Modified', 'Modified', 1, 1);
    
UPDATE sample.status
SET color= '#2276D2'
WHERE description='New';

UPDATE sample.status
SET color= '#709E62'
WHERE description='Approved';

UPDATE sample.status
SET color='#8A2F51'
WHERE description='Rejected';

UPDATE sample.status
SET color= '#EF6C00'
WHERE description='Batch';

UPDATE sample.status
SET color= '#EF6C00'
WHERE description='Design Created';

UPDATE sample.status
SET color= '#9da89e'
WHERE description='Modified';
