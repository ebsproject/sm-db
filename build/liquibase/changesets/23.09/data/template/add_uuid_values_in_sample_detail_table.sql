--liquibase formatted sql

--changeset postgres:add_uuid_values_in_sample_detail_table context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1813 Add uuid values in sample_detail table



UPDATE sample.sample_detail 
SET uuid = uuid_generate_v4()
WHERE uuid IS NULL;