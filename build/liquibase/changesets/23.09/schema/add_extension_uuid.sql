--liquibase formatted sql

--changeset postgres:add_extension_uuid context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1813 Add uuid values in sample_detail table



CREATE EXTENSION IF NOT EXISTS "uuid-ossp";