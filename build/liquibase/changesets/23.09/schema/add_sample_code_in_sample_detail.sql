--liquibase formatted sql

--changeset postgres:add_extension_uuid context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1799 Add sample_code column in sample_detail table



ALTER TABLE sample.sample_detail 
 ADD COLUMN sample_code varchar(250) NULL;