--liquibase formatted sql

--changeset postgres:add_column_color_in_sample_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1811 Add column color in smdb.sample.status table



ALTER TABLE sample.status 
 ADD COLUMN color varchar(16) NULL;