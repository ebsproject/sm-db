--liquibase formatted sql

--changeset postgres:add_new_status_for_sm_request context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1904 Add New status for SM Request



INSERT INTO sample.status 
    (description,"name",tenant_id,creator_id)	
VALUES 
    ('File Uploaded','File Uploaded',1,1),
    ('File Processing Ongoing','File Processing Ongoing',1,1),
    ('File Processed','File Processed',1,1),
    ('File Processing Failed','File Processing Failed',1,1);