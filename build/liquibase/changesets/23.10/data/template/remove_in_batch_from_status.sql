--liquibase formatted sql

--changeset postgres:remove_in_batch_from_status context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1815 Remove "In Batch" option from the Status



UPDATE 
    sample.status
SET 
    is_void =  true
WHERE 
    "name" = 'In Batch';