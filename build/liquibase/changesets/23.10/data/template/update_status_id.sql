--liquibase formatted sql

--changeset postgres:update_status_id context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-1622 Update status id and color




UPDATE sample.status
SET id=5
WHERE "name" = 'Design Created';

UPDATE sample.status
SET id=7, color = '#709E62'
WHERE "name" = 'File Uploaded';

UPDATE sample.status
SET id=8, color = '#709E62'
WHERE "name" = 'File Processing Ongoing';

UPDATE sample.status
SET id=9, color = '#709E62'
WHERE "name" = 'File Processed';

UPDATE sample.status
SET id=10, color = '#8A2F51'
WHERE "name" = 'File Processing Failed';


SELECT setval('sample.status_id_seq', (SELECT MAX(id) FROM sample.status));
