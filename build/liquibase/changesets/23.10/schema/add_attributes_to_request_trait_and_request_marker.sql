--liquibase formatted sql

--changeset postgres:add_attributes_to_request_trait_and_request_marker context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1905 Add new attributes in request_marker_group and request_trait



ALTER TABLE sample.request_trait 
 ADD COLUMN marker_group_id integer NULL;

ALTER TABLE sample.request_trait 
 ADD COLUMN technology_service_provider_id  integer NULL;

ALTER TABLE sample.request_trait 
 ADD COLUMN service_provider_id integer NULL;

ALTER TABLE sample.request_marker_group
 ADD COLUMN technology_service_provider_id integer NULL;

ALTER TABLE sample.request_marker_group
 ADD COLUMN service_provider_id integer NULL;



COMMENT ON COLUMN sample.request_trait.marker_group_id
	IS 'Reference to the marker_group_id related to that partcular request (This id comes from markerdb)';

COMMENT ON COLUMN sample.request_trait.technology_service_provider_id
	IS 'Reference to the technology_service_provider_id related to that partcular request (This id comes from markerdb)';

COMMENT ON COLUMN sample.request_trait.service_provider_id
	IS 'Reference to the service_provider_id selected for a partcular request (This id comes from markerdb)';

COMMENT ON COLUMN sample.request_marker_group.technology_service_provider_id
	IS 'Reference to the technology_service_provider_id related to that partcular request (This id comes from markerdb)';

COMMENT ON COLUMN sample.request_marker_group.service_provider_id
	IS 'Reference to the service_provider_id selected for a partcular request (This id comes from markerdb)';