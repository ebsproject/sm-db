--liquibase formatted sql

--changeset postgres:add_plate_id_to_sample_detail_table context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1868 Add new column plate_id in sample_detail table



ALTER TABLE sample.sample_detail 
 ADD COLUMN plate_id integer NULL;