--liquibase formatted sql

--changeset postgres:update_sample_status context:template splitStatements:false rollbackSplitStatements:false
--comment:  SM-1988 generate sequence segment able to keep multiple sequences based on arbitrary segment values

UPDATE sample.status SET "name"='Results Ready',description='Results Ready'	WHERE id=9;