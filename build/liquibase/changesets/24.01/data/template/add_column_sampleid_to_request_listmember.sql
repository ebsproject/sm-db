--liquibase formatted sql

--changeset postgres:add_column_sampleid_to_request_listmember context:template splitStatements:false rollbackSplitStatements:false
--comment:  SM-2048 including a new column in the table request list member to handle the multigerplams

ALTER TABLE sample.request_list_member ADD sample_detail_id int4 NULL;
