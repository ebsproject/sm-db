--liquibase formatted sql

--changeset postgres:update_id_icc_not_required context:template splitStatements:false rollbackSplitStatements:false
--comment:  SM-1858 update on ebs form custom the id icc to is_required = false

UPDATE customform.field SET is_required=false WHERE name='idICC';
