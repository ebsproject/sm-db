--liquibase formatted sql

--changeset postgres:cleaning_tables context:template splitStatements:false rollbackSplitStatements:false
--comment: SM-2115 cleaning operationals tables from SM


delete from sample.request_list_member;
delete from sample.request_status;
delete from sample.request_marker_group;
delete from sample.request_trait;
delete from sample.sample_detail;
delete from customform.value;
delete from sample.request ;
delete from sample.batch;


ALTER SEQUENCE sample.request_list_member_id_seq RESTART WITH 1;
ALTER SEQUENCE sample.request_status_id_seq RESTART WITH 1;
ALTER SEQUENCE sample.request_marker_group_id_seq RESTART WITH 1;
ALTER SEQUENCE sample.request_trait_id_seq RESTART WITH 1;
ALTER SEQUENCE sample.sample_detail_id_seq RESTART WITH 1;
ALTER SEQUENCE customform.value_id_seq RESTART WITH 1;
ALTER SEQUENCE sample.request_id_seq RESTART WITH 1;
ALTER SEQUENCE sample.batch_id_seq RESTART WITH 1;