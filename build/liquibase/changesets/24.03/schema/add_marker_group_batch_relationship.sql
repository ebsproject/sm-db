--liquibase formatted sql

--changeset postgres:add_marker_group_batch_relationship context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-2115 Create relationship marker group - batch and samples



CREATE TABLE sample.batch_marker_group (
	id integer NOT NULL   DEFAULT NEXTVAL(('sample."batch_marker_group_integration_id_seq"'::text)::regclass),
	batch_id int4 NOT NULL,
	marker_group_id int4 NOT NULL,
	technology_service_provider_id int4 NULL,
	is_fixed bool NOT NULL DEFAULT true,	
	
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	modification_timestamp timestamp NULL,
	creator_id int4 NOT NULL,
	modifier_id int4 NULL,
	is_void bool NOT NULL DEFAULT false
	
);


CREATE SEQUENCE sample.batch_marker_group_integration_id_seq INCREMENT 1 START 1;


ALTER TABLE sample.batch_marker_group ADD CONSTRAINT "PK_batch_marker_group"
	PRIMARY KEY (id);


ALTER TABLE sample.batch_marker_group ADD CONSTRAINT "FK_batch_marker_group_batch"
	FOREIGN KEY (batch_id) REFERENCES sample.batch (id) ON DELETE No Action ON UPDATE No action;


CREATE TABLE sample.batch_marker_group_custom (
	id integer NOT NULL   DEFAULT NEXTVAL(('sample."batch_marker_group_custom_id_seq"'::text)::regclass),
	batch_marker_group_id int4 NOT NULL,
	marker_id int4 NOT NULL,
	assay_id int4 NULL,
	
	creation_timestamp timestamp NOT NULL DEFAULT now(),
	modification_timestamp timestamp NULL,
	creator_id int4 NOT NULL,
	modifier_id int4 NULL,
	is_void bool NOT NULL DEFAULT false	
	
);

CREATE SEQUENCE sample.batch_marker_group_custom_id_seq INCREMENT 1 START 1;

ALTER TABLE sample.batch_marker_group_custom  ADD CONSTRAINT "PK_batch_marker_group_custom"
	PRIMARY KEY (id);

ALTER TABLE sample.batch_marker_group_custom  ADD CONSTRAINT "PK_batch_marker_group"
	FOREIGN KEY (batch_marker_group_id) REFERENCES sample.batch_marker_group (id) ON DELETE No Action ON UPDATE No action;
