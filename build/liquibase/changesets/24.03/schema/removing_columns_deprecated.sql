--liquibase formatted sql

--changeset postgres:add_marker_group_batch_relationship context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-2115 Removing deprecated columns

alter table sample.batch
drop column batch_code_id ;

alter table sample.batch
drop column assay_class_id ;

alter table sample.batch
drop column marker_group_id ;

alter table sample.sample_detail 
drop column vendor_control_id ;

alter table sample.sample_detail 
add is_vendor_control bool NOT NULL DEFAULT false;

alter table sample.batch 
add report_id int4 NOT NULL;