--liquibase formatted sql

--changeset postgres:removing_tables_from_markerDB context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-2115 removing deprecated object to be handle by marker db domain


alter table sample.batch
drop constraint  IF EXISTS "FK_batch_assay_class";

alter table sample.batch
drop constraint  IF EXISTS  "FK_batch_marker_group";

alter table sample.batch
drop constraint  IF EXISTS  "FK_batch_vendor";

alter table sample.batch
drop constraint  IF EXISTS  "FK_batch_technology_platform";

alter table sample.sample_detail 
drop constraint if exists "FK_sample_detail_vendor_control";

drop table sample.batch_marker; 

drop table sample.plant_entry_list;

DROP table services.vendor_control_batch;
drop table services.vendor_control ;
DROP table services.vendor_shipment;

DROP table services.marker_synonym;
DROP table services.marker;
DROP table services.marker_group;

drop table services.assay_gene;
drop table sample.batch_trait;
DROP table services.trait;
DROP table services.trait_class;

DROP table services.service_vendor;
drop table services.technology_platform_assay_class ;
drop table services.assay_class ;
drop table services.technology_platform;
drop table services.vendor;