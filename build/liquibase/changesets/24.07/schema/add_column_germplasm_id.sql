--liquibase formatted sql

--changeset postgres:add_column_germplasm_id context:schema splitStatements:false rollbackSplitStatements:false
--comment: BDS add_column_germplasm_id 


ALTER TABLE sample.request_list_member 
 ADD COLUMN germplasm_id integer NULL;
 
ALTER TABLE sample.sample_detail 
 ADD COLUMN germplasm_id integer NULL;
