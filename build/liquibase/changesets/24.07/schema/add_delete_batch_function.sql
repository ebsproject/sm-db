--liquibase formatted sql

--changeset postgres:add_delete_batch_function context:schema splitStatements:false rollbackSplitStatements:false
--comment: SM-1680 Create rollback function to remove a batch 



CREATE OR REPLACE FUNCTION sample.delete_batch(_batch_id int)
returns boolean
language plpgsql
as
$$
declare
   temprow record;

begin

   FOR temprow IN 
      SELECT DISTINCT request_id FROM sample.request_list_member WHERE batch_id = _batch_id
   LOOP
      UPDATE 
         sample.request_status 
      SET 
         completed = now()
      WHERE 
         request_id = temprow.request_id;
   
      INSERT INTO sample.request_status
         (tenant_id, creator_id,  request_id, status_id)
      VALUES
         (1, 1, temprow.request_id, (SELECT id FROM sample.status WHERE "name" = 'Approved'));

      UPDATE 
         sample.request
      SET  
         status_id = (SELECT id FROM sample.status WHERE "name" = 'Approved') WHERE id =temprow.request_id;

   END LOOP;

   UPDATE sample.sample_detail SET is_void=true WHERE batch_id = _batch_id;
   UPDATE sample.request_list_member SET batch_id = null, sample_detail_id = null WHERE batch_id = _batch_id;
   UPDATE sample.batch set is_void=true WHERE id = _batch_id;
   
return true;

end;
$$