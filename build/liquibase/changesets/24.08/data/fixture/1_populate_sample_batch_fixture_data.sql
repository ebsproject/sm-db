--liquibase formatted sql

--changeset postgres:1_populate_sample_batch_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3057 SM-DB: Populate sample_batch with fixture data



INSERT INTO sample.batch
(name,objective,num_containers,num_members,in_design,tenant_id,creator_id,is_void,service_provider_id,collection_layout_id,crop_id,load_type_id,technology_platform_id,vendor_id,num_empty_well,num_plates,num_samples,result_file_id,report_id)
 VALUES 
('BIRSEAHT_33','BIRSEAHT_33',139,13000,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'fce84061-eb9f-48fa-9469-fdf2f4482e01',1),
('BIRSEAMFB_16','BIRSEAMFB_16',5,470,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),2,4,0,0,0,'0d240e60-d76c-4547-bf73-b4b71d046895',4),
('BIRSEAPPT_18','BIRSEAPPT_18',25,2350,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,NULL,1),
('BIRSEAF_20','BIRSEAF_20',6,500,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),2,4,0,0,0,'89f4dc4f-23a6-4200-969c-2dd9c5e3d530',4),
('BIRSEAPPT_15','BIRSEAPPT_15',2,180,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'1a4323eb-cbc0-41dc-80da-adb065d2fa13',1),
('BBWPPT_21','BBWPPT_21',5,470,False,1,1,True,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),2,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,NULL,1),
('BIRSEAGS_22','BIRSEAGS_22',5,470,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'521f8996-9174-44fb-9027-ff82c83a6433',1),
('BIRSEAGS_24','BIRSEAGS_24',5,470,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'4cdbcadc-0f8e-46f4-8bf5-fac2b1cb5c86',1),
('BIRSEAMFB_26','BIRSEAMFB_26',2,100,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'fb6b6054-69db-40ad-b9a2-fd22dbc9aecf',1),
('BIRSEAGS_27','BIRSEAGS_27',2,100,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),2,4,0,0,0,'a24336e0-022f-412b-99a1-ab44ca0af336',4),
('BIRSEAGS_28','BIRSEAGS_28',5,470,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'d029baec-416c-48bd-aef4-23212c9346b0',1),
('BIRSEAHT_29','BIRSEAHT_29',5,470,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'4c5013b8-0032-4532-97d5-539387c1e595',1),
('BIRSEAHT_30','BIRSEAHT_30',31,2850,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'9163b338-a2ba-4b09-8785-dc65f8bcf178',1),
('BIRSEATFS_31','BIRSEATFS_31',70,6580,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'cb921912-38d1-45c5-ab2b-ee48701a03b4',1),
('BIRSEAGS_32','BIRSEAGS_32',4,300,False,1,1,False,(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.collection_layout WHERE num_columns = 10 AND num_rows = 50 AND num_well = 500 LIMIT 1),1,(SELECT id FROM sample.load_type WHERE name ='Columns' LIMIT 1),1,1,0,0,0,'dec75df2-bd94-45dc-ae83-5a21e11be387',1);



--rollback DELETE FROM sample.batch
--rollback WHERE name in (
--rollback 'BIRSEAHT_33',
--rollback 'BIRSEAMFB_16',
--rollback 'BIRSEAPPT_18',
--rollback 'BIRSEAF_20',
--rollback 'BIRSEAPPT_15',
--rollback 'BBWPPT_21',
--rollback 'BIRSEAGS_22',
--rollback 'BIRSEAGS_24',
--rollback 'BIRSEAMFB_26',
--rollback 'BIRSEAGS_27',
--rollback 'BIRSEAGS_28',
--rollback 'BIRSEAHT_29',
--rollback 'BIRSEAHT_30',
--rollback 'BIRSEATFS_31',
--rollback 'BIRSEAGS_32'
--rollback );