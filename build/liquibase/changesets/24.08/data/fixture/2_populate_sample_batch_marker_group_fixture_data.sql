--liquibase formatted sql

--changeset postgres:2_populate_sample_batch_marker_group_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3057 SM-DB: Populate sample_batch_marker_group with fixture data



INSERT INTO sample.batch_marker_group
(batch_id,marker_group_id,technology_service_provider_id,is_fixed,creator_id,is_void)
 VALUES 
((SELECT id FROM sample.batch WHERE name ='BIRSEAGS_22' LIMIT 1),131,1,False,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEAGS_24' LIMIT 1),131,1,False,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEAMFB_26' LIMIT 1),129,1,True,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEAGS_27' LIMIT 1),129,7,True,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEAGS_28' LIMIT 1),131,1,False,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEAHT_29' LIMIT 1),131,1,False,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEAHT_30' LIMIT 1),131,1,False,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEATFS_31' LIMIT 1),170,1,True,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEATFS_31' LIMIT 1),195,1,True,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEATFS_31' LIMIT 1),164,1,False,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEATFS_31' LIMIT 1),128,1,False,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEATFS_31' LIMIT 1),132,1,False,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEATFS_31' LIMIT 1),173,1,True,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEATFS_31' LIMIT 1),127,1,False,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEAGS_32' LIMIT 1),129,1,True,1,False),
((SELECT id FROM sample.batch WHERE name ='BIRSEAHT_33' LIMIT 1),222,1,True,1,False);



--rollback DELETE FROM sample.batch_marker_group
--rollback WHERE batch_id IN (SELECT id FROM sample.batch WHERE name IN (
--rollback 'BIRSEAHT_33',
--rollback 'BIRSEAGS_22',
--rollback 'BIRSEAGS_24',
--rollback 'BIRSEAMFB_26',
--rollback 'BIRSEAGS_27',
--rollback 'BIRSEAGS_28',
--rollback 'BIRSEAHT_29',
--rollback 'BIRSEAHT_30',
--rollback 'BIRSEATFS_31',
--rollback 'BIRSEAGS_32'
--rollback ));