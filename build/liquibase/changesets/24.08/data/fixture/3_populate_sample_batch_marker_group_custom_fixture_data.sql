--liquibase formatted sql

--changeset postgres:3_populate_sample_batch_marker_group_custom_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3057 SM-DB: Populate sample_batch_marker_group_custom with fixture data



INSERT INTO sample.batch_marker_group_custom
(batch_marker_group_id,marker_id,assay_id,creator_id,is_void)
 VALUES 
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),1499,2789,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),1500,2790,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),1502,2792,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),1503,2793,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),1505,2795,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),1506,2796,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),1507,2797,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),1511,2801,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),1518,2808,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_22') AND marker_group_id = '131' LIMIT 1),2456,5141,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),1499,2789,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),1500,2790,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),1502,2792,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),1503,2793,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),1505,2795,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),1506,2796,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),1507,2797,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),1511,2801,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),1518,2808,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_24') AND marker_group_id = '131' LIMIT 1),2456,5141,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),1499,2789,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),1500,2790,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),1502,2792,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),1503,2793,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),1505,2795,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),1506,2796,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),1507,2797,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),1511,2801,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),1518,2808,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAGS_28') AND marker_group_id = '131' LIMIT 1),2456,5141,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),1499,2789,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),1500,2790,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),1502,2792,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),1503,2793,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),1505,2795,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),1506,2796,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),1507,2797,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),1511,2801,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),1518,2808,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_29') AND marker_group_id = '131' LIMIT 1),2456,5141,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),1499,2789,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),1500,2790,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),1502,2792,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),1503,2793,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),1505,2795,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),1506,2796,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),1507,2797,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),1511,2801,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),1518,2808,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEAHT_30') AND marker_group_id = '131' LIMIT 1),2456,5141,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEATFS_31') AND marker_group_id = '164' LIMIT 1),1489,2781,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEATFS_31') AND marker_group_id = '128' LIMIT 1),1337,2678,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEATFS_31') AND marker_group_id = '132' LIMIT 1),1424,2734,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEATFS_31') AND marker_group_id = '127' LIMIT 1),1318,2666,1,False),
((SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id from sample.batch WHERE name = 'BIRSEATFS_31') AND marker_group_id = '127' LIMIT 1),1319,2667,1,False);



--rollback DELETE FROM sample.batch_marker_group_custom
--rollback WHERE batch_marker_group_id IN (SELECT id FROM sample.batch_marker_group WHERE batch_id IN (SELECT id FROM sample.batch WHERE name IN (
--rollback 'BIRSEAGS_22',
--rollback 'BIRSEAGS_24',
--rollback 'BIRSEAGS_28',
--rollback 'BIRSEAHT_29',
--rollback 'BIRSEAHT_30',
--rollback 'BIRSEATFS_31'
--rollback )));