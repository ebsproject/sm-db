--liquibase formatted sql

--changeset postgres:5_populate_sample_request_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3057 SM-DB: Populate sample_request with fixture data



INSERT INTO sample.request
(request_code,list_id,total_entities,tenant_id,creator_id,is_void,crop_id,program_id,purpose_id,service_provider_id,status_id,service_type_id,admin_contact_id,form_id,requester_owner_id,tissue_type_id,germplasm_owner_id,samples_per_item)
 VALUES 
('RLB-20240712-61',1907,300,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='GS' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),20,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),69,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),525,3),
('RLB-20240710-36',2053,180,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='PPT' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='File Processing Failed' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),101,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),101,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),591,5),
('RLB-20240712-57',2066,500,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='HT' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),101,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),101,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),591,5),
('RLB-20240711-54',1907,100,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='GS' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='File Processing Failed' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),11,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),69,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),525,1),
('RLB-20240711-56',2065,470,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='HT' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),46,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),46,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),461,1),
('RLB-20240711-53',1907,100,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='MFB' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),20,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),69,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),525,1),
('RLB-20240712-58',2065,2350,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='HT' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),46,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),46,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),461,5),
('RLB-20240710-39',2062,2350,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='PPT' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Design Created' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),101,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),101,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),591,5),
('RLB-20240711-55',2063,470,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='GS' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),46,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),46,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),461,1),
('RLB-20240711-51',2063,470,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='GS' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),46,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),46,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),461,1),
('RLB-20240711-50',2063,470,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='GS' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),46,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),46,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),461,1),
('RLB-20240711-47',2063,470,1,1,False,2,93,(SELECT id FROM services.purpose WHERE code ='PPT' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Design Created' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),46,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),46,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),461,1),
('RLB-20240712-62',1907,13000,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='HT' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),11,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),69,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),525,130),
('RLB-20240710-37',2061,470,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='MFB' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='File Processing Ongoing' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),101,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),101,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),591,1),
('RLB-20240712-60',2067,6580,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='TFS' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='Results Ready' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),101,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),101,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),591,94),
('RLB-20240710-40',1907,500,1,1,False,1,92,(SELECT id FROM services.purpose WHERE code ='F' LIMIT 1),(SELECT id FROM services.service_provider WHERE code ='GSL' LIMIT 1),(SELECT id FROM sample.status WHERE "name" ='File Processing Failed' LIMIT 1),(SELECT id FROM services.service_type WHERE code ='GI' LIMIT 1),20,(SELECT id FROM customform.form WHERE "name" ='CIMMYT Create Request Right Basic Tab' LIMIT 1),69,(SELECT id FROM sample.tissue_type WHERE "name" ='Leaf' LIMIT 1),525,5);



--rollback DELETE FROM sample.request 
--rollback WHERE request_code IN (
--rollback 'RLB-20240712-61',
--rollback 'RLB-20240710-36',
--rollback 'RLB-20240712-57',
--rollback 'RLB-20240711-54',
--rollback 'RLB-20240711-56',
--rollback 'RLB-20240711-53',
--rollback 'RLB-20240712-58',
--rollback 'RLB-20240710-39',
--rollback 'RLB-20240711-55',
--rollback 'RLB-20240711-51',
--rollback 'RLB-20240711-50',
--rollback 'RLB-20240711-47',
--rollback 'RLB-20240712-62',
--rollback 'RLB-20240710-37',
--rollback 'RLB-20240712-60',
--rollback 'RLB-20240710-40'
--rollback );