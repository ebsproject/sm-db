--liquibase formatted sql

--changeset postgres:7_populate_sample_request_trait_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3057 SM-DB: Populate sample_request_trait with fixture data



INSERT INTO sample.request_trait
(trait_id,request_id,creator_id,is_void,marker_group_id)
 VALUES 
(67,(SELECT id FROM sample.request WHERE request_code ='RLB-20240710-40' LIMIT 1),1,False,134),
(86,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-50' LIMIT 1),1,False,131),
(86,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-51' LIMIT 1),1,False,131),
(85,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-53' LIMIT 1),1,False,129),
(85,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-54' LIMIT 1),1,False,129),
(86,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-55' LIMIT 1),1,False,131),
(86,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-56' LIMIT 1),1,False,131),
(86,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-57' LIMIT 1),1,False,131),
(86,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-58' LIMIT 1),1,False,131),
(113,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-60' LIMIT 1),1,False,195),
(90,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-60' LIMIT 1),1,False,128),
(91,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-60' LIMIT 1),1,False,164),
(87,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-60' LIMIT 1),1,False,170),
(67,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-60' LIMIT 1),1,False,132),
(109,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-60' LIMIT 1),1,False,127),
(87,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-60' LIMIT 1),1,False,173),
(85,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-61' LIMIT 1),1,False,129),
(84,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-62' LIMIT 1),1,False,222);



--rollback DELETE FROM sample.request_trait
--rollback WHERE request_id in (SELECT id FROM sample.request WHERE request_code IN (
--rollback 'RLB-20240710-40',
--rollback 'RLB-20240711-50',
--rollback 'RLB-20240711-51',
--rollback 'RLB-20240711-53',
--rollback 'RLB-20240711-54',
--rollback 'RLB-20240711-55',
--rollback 'RLB-20240711-56',
--rollback 'RLB-20240712-57',
--rollback 'RLB-20240712-58',
--rollback 'RLB-20240712-60',
--rollback 'RLB-20240712-61',
--rollback 'RLB-20240712-62'
--rollback ));