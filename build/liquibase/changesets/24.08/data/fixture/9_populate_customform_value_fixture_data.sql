--liquibase formatted sql

--changeset postgres:9_populate_customform_value_fixture_data context:fixture splitStatements:false rollbackSplitStatements:false
--comment: BDS-3057 SM-DB: Populate customform_value with fixture data



INSERT INTO customform.value
(value,tenant_id,creator_id,is_void,request_id,field_id)
 VALUES 
('Chuckie',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240710-36' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
(NULL,1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240710-36' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
(NULL,1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240710-37' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('Chuckie',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240710-37' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('Chuckie',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240710-39' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
(NULL,1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240710-39' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('ANJALI BASHYAL',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240710-40' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('200',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240710-40' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
(NULL,1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-47' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('Jr Apolinario',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-47' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('Jr Apolinario',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-50' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
(NULL,1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-50' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
(NULL,1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-51' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('Jr Apolinario',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-51' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('ANJALI BASHYAL',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-53' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('22',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-53' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('22',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-54' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('Test blank',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-54' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('Jr Apolinario',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-55' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('Cash5King-no more',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-55' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('Cash5King',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-56' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('Jr Apolinario',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240711-56' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('Chuckie',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-57' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
(NULL,1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-57' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('Cash5King',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-58' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('Jr Apolinario',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-58' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('Chuckie',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-60' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
(NULL,1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-60' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('asmita',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-61' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1)),
('33',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-61' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('200',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-62' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idOcsNumber' LIMIT 1)),
('MANILA RAUT',1,1,False,(SELECT id FROM sample.request WHERE request_code ='RLB-20240712-62' LIMIT 1),(SELECT id FROM customform.field WHERE name ='idContactPerson' LIMIT 1));



--rollback DELETE FROM customform.value
--rollback WHERE request_id IN (SELECT id FROM sample.request WHERE request_code IN (
--rollback 'RLB-20240710-36',
--rollback 'RLB-20240710-37',
--rollback 'RLB-20240710-39',
--rollback 'RLB-20240710-40',
--rollback 'RLB-20240711-47',
--rollback 'RLB-20240711-50',
--rollback 'RLB-20240711-51',
--rollback 'RLB-20240711-53',
--rollback 'RLB-20240711-54',
--rollback 'RLB-20240711-55',
--rollback 'RLB-20240711-56',
--rollback 'RLB-20240712-57',
--rollback 'RLB-20240712-58',
--rollback 'RLB-20240712-60',
--rollback 'RLB-20240712-61',
--rollback 'RLB-20240712-62')); 