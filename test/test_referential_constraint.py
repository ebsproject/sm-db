import pytest
import numpy as np
from lib.connection_manager import ConnectionManager
from config.config_parser import ConfigParser

config = ConfigParser('config/config.ini')
schema_config = ConfigParser('config/schema.ini')

# Set database connection parameters  
host = config.get_host()
port = config.get_port()
database = config.get_db_name()
user = config.get_username()
password = config.get_password()
list_schema = schema_config.get_schema()

def get_cursor():
    db = ConnectionManager()
    con = db.connect(host=host, port=port, database=database, user=user, password=password)
    return con.cursor()

def get_list_referential_constraints_from_file(path):
    list_ref_constraint = []
    path = path
    file_input = open(path, 'r')
    for i,row in enumerate(file_input):
        ref_constraints = row.strip()
        list_ref_constraint.append(ref_constraints)

    return np.sort(np.array(list_ref_constraint))

def get_list_referential_constraints_from_db(**kwargs):
    cur = kwargs['cursor']
    expected_schemas = kwargs['schemas']
    joined_schemas = str(expected_schemas.split(',')).replace('[','(').replace(']',')').replace(' ','')
    query = "SELECT constraint_schema||'.'||constraint_name FROM information_schema.referential_constraints WHERE constraint_schema in {0};".format(joined_schemas)
    cur.execute(query)
    records = cur.fetchall()
    result_ref_constraints = [ref_constraints[0] for ref_constraints in records]

    return np.sort(np.array(result_ref_constraints))

path = 'reference/expected_referential_constraints.txt'
expected_tables = get_list_referential_constraints_from_file(path=path)
result_tables = get_list_referential_constraints_from_db(cursor=get_cursor(), schemas=list_schema)
missing_values = np.setdiff1d(expected_tables, result_tables)

@pytest.mark.parametrize("a, b, c",[(expected_tables,result_tables,missing_values)])
def test_complete_schema_database_referential_constraint(a,b,c):
    assert np.array_equal(a,b), "There are missing constraints "+str(c)
